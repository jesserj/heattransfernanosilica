/*
  Stepper Motor Control - one revolution

  This program drives a unipolar or bipolar stepper motor.
  The motor is attached to digital pins 8 - 11 of the Arduino.

  Depending on Interupt:
  The motor should revolve a number of steps in one direction,
  or the same number of steps in the other direction.

  Created 11 Mar. 2007
  Modified 30 Nov. 2009
  Modified 20 Dec. 2018
  Modified 20 Feb. 2019
  by Jesse Ross-Jones
*/

#include <Stepper.h>

// change this to fit the number of steps per revolution
// for your motor
const int stepsPerRevolution = 49;  //estimated 2 degrees
//const int stepsPerRevolution = 490;  //estimated 20 degrees

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

bool twoFlag = false;
bool threeFlag = false;

void setupStepper() {
  // set the speed at 60 rpm:
  myStepper.setSpeed(60);
  // initialize the serial port:
  //Serial.begin(9600);
}

void loopStepper() {
  // step one revolution  in one direction:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution);
  delay(500);

  // step one revolution in the other direction:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution);
  delay(500);
}

/*
  Input Pullup Serial

  This example demonstrates the use of pinMode(INPUT_PULLUP). It reads a
  digital input on pin 2 and prints the results to the serial monitor.

  OPEN THE SERIAL MONITOR TO SEE THE OUTPUT FROM THE INPUT PIN >>

  The circuit:
   Momentary switch attached from pin 2 to ground
   Built-in LED on pin 13

  Unlike pinMode(INPUT), there is no pull-down resistor necessary. An internal
  20K-ohm resistor is pulled to 5V. This configuration causes the input to
  read HIGH when the switch is open, and LOW when it is closed.

  created 14 March 2012
  by Scott Fitzgerald

  http://www.arduino.cc/en/Tutorial/InputPullupSerial

  This example code is in the public domain

*/
#include <Servo.h>

int pos = 0;
Servo servo_9;

void setup() {
  //start serial connection
  setupStepper();
  Serial.begin(9600);
  noInterrupts();
  //configure pin2 as an input and enable the internal pull-up resistor
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(13, OUTPUT);
  interrupts();
  //attachInterrupt(digitalPinToInterrupt(2), rtc_pps_handler, FALLING);
  attachInterrupt(digitalPinToInterrupt(2), two, FALLING);
  attachInterrupt(digitalPinToInterrupt(3), three, FALLING);
  delay(100); // Wait for 15 millisecond(s)
  //servo_9.attach(9);
}
void loop() {
  //Serial.println(String(pos));
  //loopStepper();
  if (twoFlag) {
    //noInterrupts();
    Serial.println("Two Pressed");
    //delay(15);
    interuptAction(-1);
    twoFlag = false;
    interrupts();
  }
  if (threeFlag) {
    //noInterrupts();
    Serial.println("Three Pressed");
    //delay(15);
    interuptAction(1);
    threeFlag = false;
    interrupts();
  }
  //delay(100);
}
void loop2() {
  //read the pushbutton value into a variable
  int sensorVal2 = digitalRead(2);
  int sensorVal3 = digitalRead(3);
  //print out the value of the pushbutton
  String stringTwo =  String("2: " + String(sensorVal2) + ", 3: " + String(sensorVal3));
  //Serial.println(stringTwo);
  // Keep in mind the pullup means the pushbutton's
  // logic is inverted. It goes HIGH when it's open,
  // and LOW when it's pressed. Turn on pin 13 when the
  // button's pressed, and off when it's not:
  if (sensorVal2 == HIGH) {
    digitalWrite(13, LOW);
  } else {
    digitalWrite(13, HIGH);
  }
}
void two() {
  digitalWrite(13, HIGH);
  noInterrupts();
  twoFlag = true;
  //Serial.println("Two Pressed");
  //delay(15);
  //interuptAction(-1);
  //interrupts();
}
void three() {
  digitalWrite(13, HIGH);
  noInterrupts();
  threeFlag = true;
  //Serial.println("Three Pressed");
  //delay(15);
  //interuptAction(1);
  //interrupts();
}
void interuptActionServo(int dir) {
  Serial.println("Spin" + String(dir));
  if (dir < 1) {
    for (pos = 0; pos <= 180; pos += 1) {
      // tell servo to go to position in variable 'pos'
      servo_9.write(pos);
      // wait 15 ms for servo to reach the position
      //delay(15); // Wait for 15 millisecond(s)
    }
  }
  else {
    for (pos = 180; pos >= 0; pos -= 1) {
      // tell servo to go to position in variable 'pos'
      servo_9.write(pos);
      // wait 15 ms for servo to reach the position
      //delay(15); // Wait for 15 millisecond(s)
    }
  }
}
void interuptAction(int dir) {
  Serial.println("Spin" + String(dir));
  int modifier = 1;
  int sensorVal5 = digitalRead(5);
  if (sensorVal5 == LOW) {
    Serial.println("modifier enabled");
    modifier = 10;
  } else {
    Serial.println("no modifier");
  }
  if (dir < 1) {
    //digitalWrite(13, HIGH);
    // step one revolution  in one direction:
    Serial.println("clockwise");
    myStepper.step(stepsPerRevolution * modifier);
    delay(1000);
    digitalWrite(13, LOW);
  }
  else {
    //digitalWrite(13, HIGH);
    // step one revolution in the other direction:
    Serial.println("counterclockwise");
    myStepper.step(-stepsPerRevolution * modifier);
    delay(1000);
    digitalWrite(13, LOW);
  }
}