function [] = saveFigures( pathSave , ans1, ans2)
%SAVEFIGURES Summary of this function goes here
%   Detailed explanation goes here


    date = char(ans1(1));
    diam = char(ans1(2));

    anglegap = char(ans2(1));     %step size between 2 angles (�)
    anglestart = char(ans2(2));   %first measured angle (�) 
    angleend = char(ans2(3));     %last measured agle (�)
    laystart = char(ans2(4));     %name of first layer to start with
    layernum = char(ans2(5));     %total number of measured layers

fig1 = figure(1);
fig2 = figure(2);
fig3 = figure(3);
text = [pathSave,'\',date,'_',diam,'nm_',anglegap,'_',anglestart,'_',angleend];
saveas(fig1,[text,'_fig1.png']);
savefig(fig1,[text,'_fig1.fig']);

saveas(fig2,[text,'_fig2.png']);
savefig(fig2,[text,'_fig2.fig']);

saveas(fig3,[text,'_fig3.png']);
savefig(fig3,[text,'_fig3.fig']);

end

