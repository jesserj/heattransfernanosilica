function [k,diamy] = particleform_ellip(beta,k,layernum,laystart,psize)
%% input variables
%beta: available cuvette angles; k = index of figure numbers
%laystart: first layer to be evaluated; layernum: total number of layers to be evaluated
%psize: vector containing determined particle size of simulation with highest congruence for each layer

%% output variables
%k = index of figure numbers
%diamy = extrapolated diameter of particle

%% code
    reflayer = 3;   %reference layer for xy diameter of ellipsoid
    beta(laystart+layernum) = 90;  %extrapolated angle

    center = 0;             %center of ellipsoid / circles
    stepang = 0.01;         %stepzize between angle theta (rad)
    theta = 0:stepang:2*pi; %angle for ellipse coordinates (rad)
    
%% plot circles of measured layers
    figure(k) 
    %layernum = total number of layers
    for i = 1 : layernum 
        %create X, Y and Z vectors using converted polar coordinates
        %psize = determined diameter of addressed layer
        %theta = vector of defined angles (radian)
        circ0(1,:) = (psize(i)/2)*cos(theta); %X vector
        circ0(2,:) = (psize(i)/2)*sin(theta); %Y vector
        circ0(3,:) = zeros(1, length(theta)); %Z vector
        %rotang = rotation angle of addressed layer
        %laystart = first addressed layer
        rotang = (beta(laystart+i-1)/360)*2*pi;
        %Xrot = rotation matrix about X axis
        Xrot = [1          0           0       ;...
                0     cos(rotang)  -sin(rotang);...
                0     sin(rotang)  cos(rotang)];

        %create rotated coordinates
        circ = zeros(3,length(circ0));
        %circ0 = coordinates in a two-dimensional space
        %Xrot = rotation matrix, center = center of circles
        for j = 1 : length(circ0)    
            rot = Xrot*circ0(:,j);       %apply x rotation
            circ(1,j) = rot(1)+center;   %rotated x vector
            circ(2,j) = rot(2)+center;   %rotated y vector
            circ(3,j) = rot(3)+center;   %rotated z vector 
        end

        %plot circle      
        plot3(circ(1,:),circ(2,:),circ(3,:),'r'); 
        xlabel('x / nm')
        ylabel('y / nm')
        zlabel('z / nm')
        hold on
    end
    axis equal;
    grid on;
    k = k + 1;
    
    %% plot ellipses of measured layers
    
    diamx = psize(reflayer-laystart+1);   %define x diameter on basis of reference layer
    diamy = psize.*2 - diamx;             %define y diameter based on transpositioned averaging
    ellip(1,:) = (diamx/2)*cos(theta);    %X vector
    ellip(3,:) = zeros(1, length(ellip)); %Z vector
    
    figure(k)
    for i = 1 : layernum+1
        str0 = ['Ebene' int2str(laystart+i-1)];       
        rotang = (beta(laystart+i-1)./360).*2*pi;  %cuvette angles in radian
        
        % Create Y vectors using converted polar coordinates
        ellip(2,:) = (diamy(i)/2)*sin(theta);

        %rotation matrix about X axis (defined by angle of cuvette in
        %current layer)
        Xrot = [1          0           0       ;...
                0     cos(rotang)  -sin(rotang);...
                0     sin(rotang)  cos(rotang)];
            
        %loop through each coordinate of ellipse
        %ellip = coordinates in a two-dimensional space
        %Xrot = rotation matrix, center = center of ellipses
        for j = 1 : length(ellip)    
            rot = Xrot*ellip(:,j);                    %apply X rotation
            coordEllip.(str0)(1,j) = rot(1)+center;   %X vecor
            coordEllip.(str0)(2,j) = rot(2)+center;   %Y vector
            coordEllip.(str0)(3,j) = rot(3)+center;   %Z vector
        end
        
        %plot ellipse        
        plot3(coordEllip.(str0)(1,:),coordEllip.(str0)(2,:),coordEllip.(str0)(3,:),'r'); 
        xlabel('x / nm')
        ylabel('y / nm')
        zlabel('z / nm')
        hold on     
    
        %% fit and plot ellipse for angle 90�
        if i == layernum
            lin = zeros(3,layernum);
            ang90 = round((1/stepang).*((90/360)*2*pi)+1); %angle 90� on ellipse
            %create matrix containing one point of each layer for same angle theta (90�).
            for j = 1 : layernum
                str0 = ['Ebene' int2str(laystart+j-1)];
                lin(:,j) = coordEllip.(str0)(:,ang90);
            end
            
            MSqu(1:2,1)=0;  %mean squared error array
            c = 3;
            
            diamfit = diamx-5; %diamfit = X diameter of elliptical fit
            thetahalf = theta(1:round(length(theta)/2)); %define theta as semicircle 
            %ellip90 = coordinates of fitted semicircle
            %Y vector of semiellipse (= X Vector in previous figure, because axis changed)
            ellip90(:,2) = (diamx/2)*sin(thetahalf)+center;  
            
            %loop while mse gets smaller by expanding y diameter
            %MSqu = mean squared error of addressed semiellipse
            %diamx = defined x diameter (equivalent to diameter of reference layer)
            while MSqu(c-1) <= MSqu(c-2) || MSqu(c-2) == 0
                if diamfit > diamx*5   %maximal form (height/width) = 5 -> break loop
                    disp('particle form exceeds boundaries (fitting has been cancelled)')
                    break
                end
                diamfit = diamfit+5; %expand x diameter of elliptical fit (stepsize 5)
                ellip90(:,1) = (diamfit/2)*cos(thetahalf)+center; %X vector of semiellipse
                fit90 = fit(ellip90(:,1),ellip90(:,2),'cubicinterp');%interpolate ellipse   
                %calculate mse of layer points and ellipse
                MSqu(c) = immse(lin(2,:)',feval(fit90,lin(3,:)'));                 
                c = c + 1;                
            end
            
            diamy(i+1) = diamfit-5; %determine new y diameter
            ellip90(:,1) = (diamy(i+1)/2)*cos(thetahalf)+center; % determine correct X vector
        end
    end
    grid on
    axis equal
    k = k + 1;
        
        
    %% plot fitted curve
    figure(k)
    plot(ellip90(:,1),ellip90(:,2),'b-')    %semielliptical fit
    hold on
    plot(lin(3,:),lin(2,:),'r.')            %data points of measured layers
    hold on
    plot([center-diamy(layernum+1)/2,center+diamy(layernum+1)/2],[center,center],'r*') %extrapolated points
    grid on
    xlabel('z / nm')
    ylabel('y / nm')
    axis equal
    k = k + 1;
        
    %% plot ellipsoid / calculate form factor
    figure(k)
    %[rotX,rotY,rotZ]=cylinder(feval(fit90,center-diamy(layernum+1)/2:1:center+diamy(layernum+1)/2)-center);
    %create ellipsoidal coordinates
    [rotX,rotY,rotZ] = ellipsoid(center,center,center,diamx/2,diamx/2,diamy(layernum+1)/2);
    %plot ellipsoid
    f = surf(rotX,rotY,rotZ);
    xlabel('x / nm')
    ylabel('y / nm')
    zlabel('z / nm')
    set(f,'LineStyle','none')
    axis equal
    k = k + 1;

    a = diamy(layernum+1)/2; b = diamx/2; c = diamx/2;  %radii of ellipsoid
    V = (4/3)*pi*a*b*c;      %ellipsoid volume
    S = 4*pi*(((a*b)^1.6075 + (a*c)^1.6075 + (b*c)^1.6075)/3)^(1/1.6075);   %ellipsoid surface
    psi = ((36*pi)^(1/3))*((V^(2/3))/S);        %sphericity
    form = diamy(layernum+1)/diamx;             %factor: dz/dx of ellipsoid
    disp(['sphericity: ' num2str(psi,2)])       %display sphericity
    disp(['form factor: ' num2str(form,2)])     %display form factor
    disp(['diameter x/y: ' num2str(diamx) ' nm'])             %display xy diameter
    disp(['diameter z: ' num2str(diamy(layernum+1),3) ' nm']) %display z diameter
end

