Requirements
Matlab with Curve Fitting Toolbox, and Image Processing Toolbox

Run Particle_SizeAndForm_PI.m to evaluate and find the closest matching curve from the Mie Light Scattering Database

Adjust the path variable for the measurment data on line 36 if necessary
Adjust the path variable for the Mie Database data on line 37 if necessary

Adjust the path variable for the output location on line 42

Run with parameters described in !Informationen.txt in each measurement folder

date of measurement: 		3052019
particle diameter [nm]: 	500

angle stepsize (°):		2
first angle (°):		20
last angle (°):			160
first layer:			3
total number of layers:		1

When adjusting the Mie Scattering Database make sure the start, stop and step variables match the database parameters
Make adjustments in read_intensityVsScattAngle.m lines 16 to 18