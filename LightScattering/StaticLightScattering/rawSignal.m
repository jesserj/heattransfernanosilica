function [k] = rawSignal(alpha,laystart,layernum,anglegap,anglenum,lbound,datnum,matnum,fluid,k,Data,subdata)
    %time: vector containing recorded time values
    %alpha1: evaluated angles twice consecutively
    time = Data(laystart).rawdat(1).Data.MeasuredData(3).Data(lbound:lbound+datnum-1, 1);
    alpha1 = vertcat(alpha,alpha);
    for E = laystart:laystart+layernum-1
        %str3 defines current scanned layer
        str3 = ['Ebene' int2str(E)];
        %anglenum = number of evaluated angles, matnum = number of measurement series
        %datnum = number of least available values of tdms files
        %rawSig = matrix containing raw signal and associated angle
        for i = 1:anglenum*matnum
            rawSig.(str3)(1:datnum,i*2-1) = alpha1(i);
            rawSig.(str3)(:,i*2) = subdata.(str3)(:,i);
        end
    end

    if matnum ==2
        fluidmat = strcat(strtrim(cellstr({'Particle' 'Water'}')));
    elseif matnum == 1
        fluidmat = strcat(cellstr(fluid));
    end

    p = 1;
    figure(k) %plot signal over time and angle
    for i = laystart:laystart+layernum-1
        str3 = ['Ebene' int2str(i)];
        for j = 1 : matnum
            subplot(layernum,matnum,p+j-1)
            plot3(time,rawSig.(str3)(:,(j-1)*anglenum*2+1:2:anglenum*2*j-1),rawSig.(str3)(:,(j-1)*anglenum*2+2:2:anglenum*2*j))
            title([fluidmat(j) ' measurement layer ' int2str(i)])
            ylabel('angle / degrees')
            zlabel('intensity / counts')
            xlabel('time / ms')
            grid on
        end
        p = p + matnum;
    end
    k = k + 1;

    p = 1;
    figure(k) %boxplot
    for i = laystart:laystart+layernum-1
        str3 = ['Ebene' int2str(i)];
        for j = 1 : matnum
            subplot(layernum,matnum,p+j-1)
            boxplot(subdata.(str3)(:,anglenum*(j-1)+1:anglenum*j),'Labels',num2cell(alpha));
            title([fluidmat(j) ' measurement layer ' int2str(i)])
            xlabel('angle / degrees')
            ylabel('intensity / counts')
            if anglegap < 10
                ax = gca;
                labels = string(ax.XAxis.TickLabels);
                labels(2:2:end) = nan;
                ax.XAxis.TickLabels = labels; 
            end
            grid on
        end
        p = p + matnum;
    end
    k = k + 1;
end

