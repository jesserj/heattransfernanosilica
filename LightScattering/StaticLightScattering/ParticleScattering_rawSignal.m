%shortened program to check if recorded signal is usable

clear
diam = 0;
date = 0;
fluid = 'off';
anglegap = 0;
anglestart = 0;
angleend = 0;
laystart = 0;
layernum = 0;

%% inputboxes for user
%inputbox for particle diameter and date
while diam <= 0 || isnan(diam) || date<=0 || isnan(date)  
    ans1 = inputdlg({'date of measurement:','particle diameter [nm]:'},'Input');
    date = str2double(ans1(1));
    diam = str2double(ans1(2));
end 
%inputbox for measurement execution parameters
while anglegap <=0 || anglestart <=0 || angleend <=0 || laystart <=0 || layernum <=0 || isnan(anglegap) || isnan(anglestart) || isnan(angleend) || isnan(laystart) || isnan(layernum)
    answer = inputdlg({'angle stepsize (�):','first angle (�):','last angle (�):','first layer:','total number of layers:'},'Input');
    anglegap = str2double(answer(1));     %step size between 2 angles (�)
    anglestart = str2double(answer(2));   %first measured angle (�) 
    angleend = str2double(answer(3));     %last measured agle (�)
    laystart = str2double(answer(4));     %name of first layer to start with
    layernum = str2double(answer(5));     %total number of measured layers
end

%% define relevant input variables
path1 = ['C:\Users\EVE\Documents\Work\Research\Mie_DataFitting\software\Wunsch_arbeit\Wunsch_Messdaten\' int2str(date) '_' int2str(diam) 'nm'];  %path to tdms data
lbound = 51;    %first value to include during averaging

anglenum = (angleend-anglestart)/anglegap +1;  %number of measured angles (�)
if round(anglenum) ~= anglenum  %if starting and ending point of angle are selected so that total number of angles is uneven, last angle is excluded
    anglenum = fix(anglenum);   %cut off decimals of anglenum
end

if exist([path1 '\Partikel_' int2str(anglestart) '_E' int2str(laystart) '.tdms']) == 0       %particle data does not exist (only water)
    strfl = '\Wasser_';
    matnum = 1;
    fluid = 'Water';
    p = 2;
elseif exist([path1 '\Wasser_' int2str(anglestart) '_E' int2str(laystart) '.tdms']) == 0     %water data does not exist (only particle)
    strfl = '\Partikel_';
    matnum = 1;
    fluid = 'Particle';
    p = 2;
else                    %2 matrices (particle+water) exist
    matnum = 2;     
    strfl = '\Partikel_';
    p = 5;
end

%define relevant angles (degrees) in the vector alpha
alpha(:,1) = anglestart : anglegap : angleend;
%convert degrees to radian in the vector phi
phi = (alpha(:,1)./360)*2*pi;   

%% preparation of measurement data
for index = laystart : laystart+layernum-1
    str0 = ['Ebene' int2str(index)]; 
    str = strfl;
    ang = anglestart;
    for i = 1 : anglenum*matnum     %convert TDSM data to structure array
           Data(index).rawdat(i) = convertTDMS(1, [path1 str int2str(ang) '_E' int2str(index) '.tdms']); 
           if i == anglenum                  %after completion of particle data:
               str = '\Wasser_';             %string changes to water
               ang = anglestart-anglegap;    %ang is set on first measured angle
           end
           %find out the number of the smallest measurement matrix (sets the total number of values to include during median calculation)        
           if i == 1 && index == laystart   %set starting point for value with first read matrix
               [datnum,columns1] = size(Data(index).rawdat(i).Data.MeasuredData(4).Data);
           end
           [rows2,columns2] = size(Data(index).rawdat(i).Data.MeasuredData(4).Data); %get size of current addressed matrix in loop
           if rows2 < datnum
               datnum = rows2;    %replace value with current value if matrix is smaller 
           end
           ang = ang + anglegap;
    end
end

datnum = datnum - lbound;   %first 50 values are excluded 
if datnum < 100             %warning appears if there are less than 100 value to include during median calculation
    msgbox('WARNING: less than 100 data points are used for each angle','Warning')
end

%laystart = first layer to be evaluated
%layernum = total number of layers to be evaluated

for index = laystart : laystart+layernum-1
    %str0 defines current scanned layer
    str0 = ['Ebene' int2str(index)];
    
    %subdata: shortened matrix with least available number of values (starting at 51st value -> lbound = 51)
    %datnum = least available number of values
    %loop through each angle of both measurement series 
    subdata.(str0) = zeros(datnum,anglenum*matnum);
    for k = 1 : anglenum*matnum      
        subdata.(str0)(:,k) = Data(index).rawdat(k).Data.MeasuredData(4).Data(lbound:lbound+datnum-1, 1); 
    end
    
    %middata0 = generated median values for each angle (signal over time)
    %anglenum = total number of angles to be evaluated
    middata0.(str0) = zeros(matnum,anglenum);
    middata0.(str0)(1,:) = median(subdata.(str0)(:,1:anglenum),1);
    %write second measurement series (water) in new row
    %matnum = number of measurement series
    if matnum == 2
        middata0.(str0)(2,:) = median(subdata.(str0)(:,anglenum+1:end),1); 
    end

    %formats change: previous form -> matrices are arranged side by side
    %in the next step generated form -> rows: particle, water, delta, quotient
                                       %columns: angles 
    
    %first row of new matrix middata is filled with measured angles      
    middata.(str0)(1,:) = alpha';
 
    %calculate moving averages for each measurement series
    for j = 1 : matnum     
        middata.(str0)(j+1,:) = smooth(middata0.(str0)(j,:));
    end
    
    if matnum == 2  
        %calculate delta (particle-water) in the row below 
        middata.(str0)(matnum+2,:) = middata.(str0)(2,:) - middata.(str0)(3,:); 
        %calculate quotient (particle/water) in the row below
        middata.(str0)(matnum+3,:) = middata.(str0)(2,:)./middata.(str0)(3,:);  
    end
end

k = 1;
k0 = 1;

figure(k)
for i = laystart : laystart+layernum-1
    str0 = ['Ebene' int2str(i)];
    counts(:,1) = middata.(str0)(p,:);
    if matnum == 2
        counts(:,2) = middata.(str0)(2,:); %particle
        counts(:,3) = middata.(str0)(3,:); %water
        leg = strcat(strtrim(cellstr({'Quotient' 'Particle' 'Water'}')));
    else
        leg = fluid;
    end
    %create polar plot
    subplot(layernum,2,k0);
    polarplot(phi,counts(:,1));
    legend(leg(1,1));                       %create legend
    title(['Layer ' int2str(i)])
    %create line plot
    subplot(layernum,2,k0+1);
    plot(alpha,counts(:,1:end),alpha,middata0.(str0)(1:end,:),'.');                  
    legend(leg);                                      %create legend
    set(gca, 'YScale', 'log')                           %generate log y scale
    xlabel('angle / degrees')                           %label x axis of line plot
    ylabel('intensity / counts')          %label y axis of line plot
    title(['Layer ' int2str(i)])
    grid on                                             %display gridlines
    %xlswrite('Data.xls', middata.(str0), i, 'B1')       %write data of matrix middata in excel table (each layer in separate excel sheets)
    k0 = k0 + 2;
end
k = k + 1;

%% Evaluation of raw signal

%time: vector containing recorded time values
%alpha1: evaluated angles twice consecutively
time = Data(laystart).rawdat(1).Data.MeasuredData(3).Data(lbound:lbound+datnum-1, 1);
alpha1 = vertcat(alpha,alpha);
for E = laystart:laystart+layernum-1
    %str3 defines current scanned layer
    str3 = ['Ebene' int2str(E)];
    %anglenum = number of evaluated angles, matnum = number of measurement series
    %datnum = number of least available values of tdms files
    %rawSig = matrix containing raw signal and associated angle
    for i = 1:anglenum*matnum
        rawSig.(str3)(1:datnum,i*2-1) = alpha1(i);
        rawSig.(str3)(:,i*2) = subdata.(str3)(:,i);
    end
end

if matnum ==2
    fluidmat = strcat(strtrim(cellstr({'Particle' 'Water'}')));
elseif matnum == 1
    fluidmat = strcat(cellstr(fluid));
end

p = 1;
figure(k) %plot signal over time and angle
for i = laystart:laystart+layernum-1
    str3 = ['Ebene' int2str(i)];
    for j = 1 : matnum
        subplot(layernum,matnum,p+j-1)
        plot3(time,rawSig.(str3)(:,(j-1)*anglenum*2+1:2:anglenum*2*j-1),rawSig.(str3)(:,(j-1)*anglenum*2+2:2:anglenum*2*j))
        title([fluidmat(j) ' measurement layer ' int2str(i)])
        ylabel('angle / degrees')
        zlabel('intensity / counts')
        xlabel('time / ms')
        grid on
    end
    p = p + matnum;
end
k = k + 1;

p = 1;
figure(k) %boxplot
for i = laystart:laystart+layernum-1
    str3 = ['Ebene' int2str(i)];
    for j = 1 : matnum
        subplot(layernum,matnum,p+j-1)
        boxplot(subdata.(str3)(:,anglenum*(j-1)+1:anglenum*j),'Labels',num2cell(alpha));
        %title([fluidmat(j) ' measurement layer ' int2str(i)])
        xlabel('measurement number')
        ylabel('measurement signal')
        if anglegap < 10
            ax = gca;
            labels = string(ax.XAxis.TickLabels);
            labels(2:2:end) = nan;
            ax.XAxis.TickLabels = labels; 
        end
        %set(gca,'YScale','log')
        grid on
    end
    p = p + matnum;
end


