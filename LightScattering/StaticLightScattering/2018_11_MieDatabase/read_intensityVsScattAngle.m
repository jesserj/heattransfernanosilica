function [ data ] = read_intensityVsScattAngle(filename)
%READ_MIE_BATCH Summary of this function goes here
%   Detailed explanation goes here


N=[4,4];  % the number of columns for each section
H=[10,1827];   % the number of header lines (note added in the trailer here

%fid=fopen('rad_0.04-1.1um_step0.01um_0-180deg_intensityVsScattAngle.mut');
fid=fopen(filename);

frewind(fid);
%for i=1:3
i = 1;
start = 0.04; %% MAKE SURE THESE MATCH THE REAL VALUES
stop = 1.1; %% MAKE SURE THESE MATCH THE REAL VALUES
step = 0.01; %% MAKE SURE THESE MATCH THE REAL VALUES

batchSimData = [];
batchSimLabels = [];

while ~feof(fid)
    if (i == 1)
        col = N(1);
        header = H(1);
    else
        col = N(2);
        header = H(2);
    end
     fmt1=repmat('%f',1,col);  % build the format string for the number columns

     a(1,1)=textscan(fid,fmt1,'headerlines',header,'collectoutput',1); % read section
     if(~isempty(a{1,1}(:,1)))
         if (i == 1)
            batchSimData(:,1) = a{1,1}(:,1);
            batchSimLabels{1,1} = 'Scattering Angle';
         end
         temp = a{1,1}(:,4);
         batchSimData(:,i+1) = temp;

         batchSimLabels{1,i+1} = 2*(start+step*(i-1)); %%DIAMETER
         i = i + 1;
     end
   end
 fid=fclose(fid);
 disp('THE PARTICLE SIZE IS DIAMETER')
 
 data = {batchSimData, batchSimLabels};
 
 