function [Simulation,angsim,sim0,simnum,intsize,diamsim] = prep_simulationData(path2,anglestart,angleend)
    %% input variables
    %path2: path to simulation file 
    %anglestart: first evaluated angle; angleend: last evaluated angle
    
    %% output variables
    %Simulation: structure of simulation matrices (raw, shortened, normalized, differentiated)
    %angsim: vector of simulated angles; simnum: total number of simulations
    %sim0: vector of total number of extremes of each simulation
    %intsize: reciprocal stepsize of simulation angles; diamsim: simulated particle diameters
    
    %% code
    %arrange simulation files in structure matrix Simulation.Data and create structure
    Simulation.Data = read_intensityVsScattAngle(path2);

    %intsize = determined stepsize between simulated angles
    intsize = 1./(Simulation.Data{1,1}(2,1)-Simulation.Data{1,1}(1,1)); 
    %define relevant simualation angles(first angle to last angle)
    angsim = Simulation.Data{1,1}(anglestart*intsize+1:angleend*intsize+1,1);
    %subdata = shortened simulation data matrix (compatible with evaluated measured data)
    Simulation.subdata = Simulation.Data{1,1}(anglestart*intsize+1:angleend*intsize+1,:);  
    %determine maximum and minimum values of each column in submatrix
    yusim = max(Simulation.subdata(:,2:end),[],1);  
    ylsim = min(Simulation.subdata(:,2:end),[],1);
    %insert each simulated diameter in a vector (in nanometers)
    diamsim = (cell2mat(Simulation.Data{1,2}(1,2:end))./2).*1000;  
    %determine total number of simulations
    simnum = length(diamsim);

    for j = 1 : simnum
        %str2 = designate matrix as belonging particle diameter
        str2 = ['nm' int2str(diamsim(j))];
        %normdata = normalized subdata
        Simulation.normdata.(str2) = (Simulation.subdata(:,j+1)-ylsim(j))./(yusim(j)-ylsim(j));  
        %Differentiation = differentiated normalized data
        Simulation.Differentiation.(str2) = diff(Simulation.normdata.(str2)(:));
    end

    %sim0 = vector filled with number of extremes of each simulation
    %simnum = total number of simulations
    sim0 = zeros(simnum,1);   
    %count number of extremes of each simulation
    for i = 1 : simnum
        %str2 = identify matrix with addressed particle diameter
        str2 = ['nm' int2str(diamsim(i))];
        %angsim = relevant simulation angles (first to last angle)
        for j = 1 : length(angsim)-2
            if sign(Simulation.Differentiation.(str2)(j)) ~= sign(Simulation.Differentiation.(str2)(j+1)) 
                sim0(i) = sim0(i) + 1;
            end
        end
    end
end

