function [k,middata,Quot,datnum,Data,subdata] = prep_measurementData(k,path1,laystart,layernum,anglenum,anglegap,matnum,anglestart,fluid2,strfile,p,lbound,alpha,phi)
    %% input variables:   
    %k: index of figure numbers; path1: path to tdms files
    %matnum: number of measurement series(particle/water); fluid2: string of water in file name
    %strfile: first specified string in filename (based on determined measurement series in file folder)
    %p: relevant row in matrix middata to use in fit-simulation comparison (based on determined measurement series in file folder)
    %lbound: lower limit of first value in measurement vector to include during median averaging 
    %alpha: vector of specified angles in degrees; phi: vector of specified angles in radian
    %(variables specified by user:)
    %laystart: first layer to be evaluated; layernum: total number of layers to be evaluated
    %anglenum: total number of angles; anglegap: stepsize of angles; anglestart: first angle
    
    %% output variables:
    %k: index of figure numbers; Quot: matrix of quotients of each layer and assigned layer index
    %middata: structure with fields referring to each layer, fields containing matrix of averaged measurement matrices (1st row: angles, 2nd row: particle data, 3rd row: water data, 4th row: delta, 5th row: quotient)
    %datnum: total number of values included during median averaging
    %Data: structure of converted TDMS files; subdata: matrix of unitary reduced measurement vectors by datnum.
    
    %% code

    datnum = 0;
    Quot = zeros(anglenum,layernum*2);
    
    %loop through each layer
    for index = laystart : laystart+layernum-1
        str = strfile;
        ang = anglestart;
        %loop through each angle
        for i = 1 : anglenum*matnum     %convert TDSM data to structure array
               Data(index).rawdat(i) = convertTDMS(1, [path1 '\' str '_' int2str(ang) '_E' int2str(index) '.tdms']); 
               if i == anglenum                  %after completion of particle data:
                   str = fluid2;                 %string changes to water
                   ang = anglestart-anglegap;    %ang is set on first measured angle
               end
               %determine length of measurement vector 
               rows = length(Data(index).rawdat(i).Data.MeasuredData(4).Data);
               %determine minimal number of avialable values of imported files
               if rows < datnum || datnum == 0
                   datnum = rows;    %total number of values to include during averaging
               end
               ang = ang + anglegap;
        end
    end

    datnum = datnum - lbound;
    %if total number of values to include during averaging is smaller than 100 values, warning appears
    if datnum < 100
        msgbox('WARNING: less than 100 data points are used for each angle')
    end

    %laystart = first layer to be evaluated
    %layernum = total number of layers to be evaluated

    for index = laystart : laystart+layernum-1
        %str0 defines current scanned layer
        str0 = ['Ebene' int2str(index)];

        %subdata: shortened matrix with least available number of values 
        %datnum = least available number of values
        %loop through each angle of both measurement series 
        subdata.(str0) = zeros(datnum,anglenum*matnum);
        for i = 1 : anglenum*matnum      
            subdata.(str0)(:,i) = Data(index).rawdat(i).Data.MeasuredData(4).Data(lbound:lbound+datnum-1,1); 
        end

        %middata0 = generated median values for each angle (signal over time)
        %anglenum = total number of angles to be evaluated
        middata0.(str0) = zeros(matnum,anglenum);
        middata0.(str0)(1,:) = median(subdata.(str0)(:,1:anglenum),1);
        %write second measurement series (water) in new row
        %matnum = number of measurement series
        if matnum == 2
            middata0.(str0)(2,:) = median(subdata.(str0)(:,anglenum+1:end),1); 
        end

        %formats change: previous form -> matrices are arranged side by side
        %in the next step generated form -> rows: particle, water, delta, quotient
                                           %columns: angles 

        %first row of new matrix middata is filled with measured angles      
        middata.(str0)(1,:) = alpha';

        %calculate moving averages for each measurement series
        for j = 1 : matnum     
            middata.(str0)(j+1,:) = smooth(middata0.(str0)(j,:));
        end

        if matnum == 2  
            %calculate delta (particle-water) in the row below 
            middata.(str0)(matnum+2,:) = middata.(str0)(2,:) - middata.(str0)(3,:); 
            %calculate quotient (particle/water) in the row below
            middata.(str0)(matnum+3,:) = middata.(str0)(2,:)./middata.(str0)(3,:);  
        end

        %create matrix containing quotient/averaged raw data of each layer and associated layer number
        %p = relevant row of matrix middata depending on number of measurement series
        Quot(1:anglenum,(index-laystart)*2+1) = index;
        Quot(:,(index-laystart)*2+2) = middata.(str0)(p,:);
    end

    k0 = 1; %index of plots in subplot

    figure(k)
    for i = laystart : laystart+layernum-1
        str0 = ['Ebene' int2str(i)];
        counts(:,1) = middata.(str0)(p,:);
        if matnum == 2
            counts(:,2) = middata.(str0)(2,:); %particle
            counts(:,3) = middata.(str0)(3,:); %water
            leg = strcat(strtrim(cellstr({'Quotient' 'Particle' 'Water'}')));
        else
            leg = fluid;
        end
        %create polar plot
        subplot(layernum,2,k0);
        polarplot(phi,counts(:,1));
        legend(leg(1,1));                      
        title(['Layer ' int2str(i)])
        %create line plot
        subplot(layernum,2,k0+1);
        plot(alpha,counts(:,1:end),alpha,middata0.(str0)(1:end,:),'.');                  
        legend(leg,'Location','Best');                                   
        set(gca, 'YScale', 'log')                          
        xlabel('angle / degrees')                          
        ylabel('intensity / counts')         
        %title(['Layer ' int2str(i)])
        grid on                                             
        %xlswrite('Data.xls', middata.(str0), i, 'B1')       %write data of matrix middata in excel table (each layer in separate excel sheets)
        k0 = k0 + 2;
    end
    k = k + 1;

    if layernum > 1
        %create 3d plot to display every layer in one plot
        figure(k);
        plot3(alpha, Quot(:,1:2:end), Quot(:,2:2:end))         
        title('Quotient curve of each layer')
        xlabel('angle / degrees')                                
        ylabel('layer / -')                                      
        zlabel('intensity / -')
        grid on
        k = k + 1;
    end
end

