function [k,Fit,psize,diamselect,a] = comparison_fit_simulation(k,Quot,laystart,layernum,angsim,diam,alpha,intsize,Simulation,simnum,diamsim,sim0)
    %% input variables
    %k: index of figure numbers; Quot: matrix of quotients of each layer and assigned layer index
    %laystart: first layer to be evaluated; layernum: total number of layers to be evaluated
    %angsim: vector of simulated angles; diam: particle size according to manufacture
    %alpha: vector of specified angles in degrees; intsize: reciprocal stepsize of simulation angles
    %Simulation: structure of simulation matrices (raw, shortened, normalized, differentiated)
    %simnum: total number of simulations; diamsim: simulated particle diameters
    %sim0: vector of total number of extremes of each simulation
    
    %% output variables
    %k: index for figure numbers; Fit: structure of fitted measured data(raw, normalized, differentiated)
    %psize: vector containing determined particle size of simulation with highest congruence for each layer
    %diamselect: diameters of selected simulations based on total number of extremes
    %a: structure with field for each layer, fiels containing vector with measure of congruence between fit data and each simulation of diamselect
    
    useALL = true; %will skip extrema check to filter simulations
    %useALL = false; %will use extrema check to filter simulations
    
    %% code
    %collect data relevant to the comparison of each layer 
    QuotSub = Quot(:,2:2:end);  %quotient of each layer    
    fitmod = {'sin1','sin2','sin3','sin4','sin5'};  %fiting models
    Squ = zeros(length(fitmod),1);          %MSE of each fitting model 
    k0 = 1;                                 %index for subplots of first figure
    k1 = 1;                                 %index for subplot of second figure
    psize = zeros(layernum,1);              %determined particle sizes
    fit0 = zeros(layernum,1);               %number of extremes of each layer
    
    tempVal = 0;                            %used to plot fit on same figure

    %loop through each layer
    for int3 = 1 : layernum

        %define y data of addressed layer
        y = QuotSub(:,int3); 

        %designate addressed layer to use in sructure arrays
        fitvar = ['Ebene' int2str(int3+laystart-1)];

        %fitmod = array of chosen fit models
        %alpha = evaluated angles, y = data to be fit
        for i = 1 : length(fitmod)
            obj0.(['model' int2str(i)]) = fit(alpha,y,fitmod{i}); %fit data
            %calculate mse between fit and data
            Squ(i) = immse(y,feval(obj0.(['model' int2str(i)]),alpha));
        end

        %choose appropriate fit function
        %fitindex = vector index of minimal value
        [~,fitindex] = min(Squ);
        %obj = defined fit function of addressed layer based on minimal MSE
        obj.(fitvar) = obj0.(['model' int2str(fitindex)]);             

        %int3 = define column of addressed layer
        %angsim = angles used in simulation between defined angles
        %determine y values of fit
        Fit.Data(:,int3) = feval(obj.(fitvar),angsim);
        yufit = max(Fit.Data(:,int3));            %identify maximal value
        ylfit = min(Fit.Data(:,int3));            %identify minimal value
        %normalize fit curve by referring to minimal and maximal value
        Fit.norm(:,int3) = (Fit.Data(:,int3)-ylfit)./(yufit-ylfit);         
        %differentiate normalized fitted curve
        Fit.diff(:,int3) = diff(Fit.norm(:,int3));   

        %count number of extremes of addressed layer
        %start and end extrema search within limits (+-5�) to prevent counting artificial made extrema of fit function
        fit0(int3) = 0;
        for j = 5*intsize : (length(angsim)-2)-5*intsize     
            if sign(Fit.diff(j,int3)) ~= sign(Fit.diff(j+1,int3))
                fit0(int3) = fit0(int3)+1;
            end
        end

        %simnum= number of simualtions, diamsim = simulated diameters
        %sim0= number of extremes of each simulation
        %fit0= number of extremes of fit of addressed layer (defined by fitvar)
        %[to do: when stepper motor is used, change this]
        int4 = 1;  
        if fit0(int3) < 4
            for i = 1 : simnum
                if sim0(i) == fit0(int3) || abs(sim0(i)-fit0(int3)) == 1 || useALL
                    diamselect.(fitvar)(int4,1) = diamsim(i);
                    int4 = int4 + 1;
                end
            end
        elseif fit0(int3) >= 4
            for i = 1 : simnum
                if sim0(i) >= fit0(int3) || useALL
                    diamselect.(fitvar)(int4,1) = diamsim(i);
                    int4 = int4 + 1;
                end
            end
        end   
        %diamselect= simulated diameters consistent with fit pursuant to    
        %            number of extremes

        %for each angle, check if differentiated values have same sign
        %add reciprocal value of absolute differentiation difference(fitted curve-simulated curve) to variable a
        %repeat operation for each simulation -> 'a' is a column vector (number of rows = number of simulation files)
        ap = zeros(length(diamselect.(fitvar)),1);
        an = zeros(length(diamselect.(fitvar)),1);
        fp = zeros(length(diamselect.(fitvar)),1);
        fn = zeros(length(diamselect.(fitvar)),1);
        a.(fitvar) = zeros(length(diamselect.(fitvar)),1);

        %diamselect= diameters of simulation subset
        for j = 1 : length(diamselect.(fitvar)) 
            %str2= defines addressed simulation by diameter
            str2 = ['nm' int2str(diamselect.(fitvar)(j))];
            %angsim= angles (stepsize according to simulation, first angle to last angle)
            %Fit.diff= differentiated fit, Simulation.Differentiation= differentiated simulations.
            for i = 1 : length(angsim)-1
                if sign(Fit.diff(i,int3)) == sign(Simulation.Differentiation.(str2)(i))  
                    ap(j) = trapz([ap(j); 1./abs(Fit.diff(i,int3)-Simulation.Differentiation.(str2)(i))]);
                    fp(j) = fp(j) + 1;
                else
                    an(j) = trapz([an(j); abs(Fit.diff(i,int3)-Simulation.Differentiation.(str2)(i))]); 
                    fn(j) = fn(j) + 1;
                end      
            end
        end
        %ap= measure of agreement between fit and each simulation
        %an= measure of disagreement between fit and each simulation
        %fp= number of agreement cases, fn= number of disagreement cases

        apnorm = (ap-min(ap))./(max(ap)-min(ap))*9+1;  %normalized ap
        annorm = (an-min(an))./(max(an)-min(an))*9+1;  %normalized an
        fap = (fp./(fp+fn))*10; %occurrence of equal signs
        fan = (fn./(fp+fn))*10; %occurrence of unequal signs
        %a= quotient of leveraged normalized measures
        a.(fitvar) = (apnorm.*fap)./(annorm.*fan);

        %if difference of differentiated fit and simulation = 0 
        %-> identical derivatives, reciprocal value is infinite 
        %a0 = logical vector, indicates infinite values in a
        a0.(fitvar) = isnan(a.(fitvar));
        if sum(a0.(fitvar)) > 0    
            a.(fitvar) = a0.(fitvar);
        end 
        %maximal value in 'a' represents simulation with highest correspondence
        %pos = position of maximal value in 'a' 
        [~,pos] = max(a.(fitvar)); 
        %psize = determined particle size
        %diamselect = selected diameters due to extremes determination
        psize(int3) = diamselect.(fitvar)(pos);       

        figure(k);  %plot of fit curve and fitted data 
        subplot(layernum+tempVal,3,k0);
        plot(obj.(fitvar), 'r-',alpha,y,'b.')         
        title(['fit function of measured data (layer ' int2str(laystart+int3-1) ')'])
        legend('Location','Best')
        xlabel('angle / degrees')
        ylabel('intensity / -')
        grid on  

        %plot simulation of determined particle size
        str2 = ['nm' int2str(psize(int3))];
        subplot(layernum+tempVal,3,k0+1);
        plot(angsim,Simulation.normdata.(str2),angsim,Fit.norm(:,int3))
        set(gca, 'YScale', 'log') 
        title(['Simulation ' int2str(psize(int3)) ' nm & fit function'])
        xlabel('angle / degrees')
        ylabel('intensity (normalized) / -')
        grid on
        
        %%diameter correction
%         if(diam==300) diam=310, end;%314nm
%         if(diam==400) diam=410, end;%407nm
%         if(diam==500) diam=510, end;%507nm
%         if(diam==550) diam=550, end;%546nm
%         if(diam==600) diam=640, end;
%         if(diam==700) diam=680, end;%681nm
%         if(diam==800) diam=810, end;%812nm
%         if(diam==900) diam=900, end;
%         if(diam==1000) diam=990, end;%987nm

        %plot simulation of entered particle size
        subplot(layernum+tempVal,3,k0+2);
        str2 = ['nm' int2str(diam)];
        plot(angsim,Simulation.normdata.(str2),angsim,Fit.norm(:,int3))
        set(gca, 'YScale', 'log')  
        title(['Simulation ' int2str(diam) ' nm & fit function'])
        xlabel('angle / degrees')
        ylabel('intensity (normalized) / -')
        grid on
        
        %%%%%
%         subplot(layernum+tempVal,3,[4,5,6]);
%         plot(diamselect.(fitvar),a.(fitvar)(:,1),'b-',diamselect.(fitvar),a.(fitvar)(:,1),'r.')%diamselect.(fitvar),annorm.*fan,diamselect.(fitvar),apnorm.*fap,diamselect.(fitvar),a.(fitvar)(:,1));
%         xlabel('particle diameter of simulation / nm')
%         ylabel('congruence / -')
%         grid on
        %%%%%%

        k0 = k0 + 3;

        figure(k+1)
        subplot(layernum,1,k1);
        plot(diamselect.(fitvar),a.(fitvar)(:,1),'b-',diamselect.(fitvar),a.(fitvar)(:,1),'r.')%diamselect.(fitvar),annorm.*fan,diamselect.(fitvar),apnorm.*fap,diamselect.(fitvar),a.(fitvar)(:,1));
        xlabel('particle diameter of simulation / nm')
        ylabel('congruence / -')
        grid on
        %set(gca,'YScale','log')
        k1 = k1 + 1;
    end
    k = k + 2;
end

