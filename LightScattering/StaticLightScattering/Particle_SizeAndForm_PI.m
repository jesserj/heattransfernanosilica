%% inputboxes for user

clear
diam = 0;
date = 0;
anglegap = 0;
anglestart = 0;
angleend = 0;
laystart = 0;
layernum = 0;

%inputbox for particle diameter and date
while diam <= 0 || isnan(diam) || date<=0 || isnan(date)
    definput = {'3052019',''};
    ans1 = inputdlg({'date of measurement:','particle diameter [nm]:'},'Input',[1,20],definput);
    date = str2double(ans1(1));
    diam = str2double(ans1(2));
end 
%inputbox for measurement execution parameters
while anglegap <=0 || anglestart <=0 || angleend <=0 || laystart <=0 || layernum <=0 || isnan(anglegap) || isnan(anglestart) || isnan(angleend) || isnan(laystart) || isnan(layernum)
    definput = {'2','20','160','3','1'};
    %definput = {'2','40','140','3','1'};
    %definput = {'2','60','120','3','1'};
    ans2 = inputdlg({'angle stepsize (�):','first angle (�):','last angle (�):','first layer:','total number of layers:'},'Input',[1,20],definput);
    anglegap = str2double(ans2(1));     %step size between 2 angles (�)
    anglestart = str2double(ans2(2));   %first measured angle (�) 
    angleend = str2double(ans2(3));     %last measured agle (�)
    laystart = str2double(ans2(4));     %name of first layer to start with
    layernum = str2double(ans2(5));     %total number of measured layers
end
%% define relevant input variables

%%% Make sure to modify the particles size in
%%% 'read_intenistyVsScattAngle.m' if you change the simulation database

path1 = ['Measurements\'  int2str(date) '_' int2str(diam) 'nm'];  %path to tdms data
path2 = '2018_11_MieDatabase\rad_0.02-0.55um_step0.005um_0-180deg_intensityVsScattAngle.mut'; %path to simulation files

%pathSave = [path1,'\..\resultsParallelPol'];
%pathSave = [path1,'\..\resultsUNPol'];
%pathSave = [path1, '\..\resultsPerpenPol'];
pathSave = [path1, '\..\temp'];

fluid1 = 'Partikel';
fluid2 = 'Wasser';
lbound = 51;     %first value to include during averaging
beta = [-30 -15 0 15];  %available angles of cuvette

anglenum = (angleend-anglestart)/anglegap +1;  %number of measured angles (�)
%round number of angles if stepsize does not fit first and last angle
if round(anglenum) ~= anglenum
    anglenum = fix(anglenum);
    angleend = anglestart+anglenum*anglegap-anglegap;
end

%determine available measurement series 
if exist([path1 '\' fluid1 '_' int2str(anglestart) '_E' int2str(laystart) '.tdms'],'file') == 0       %particle data does not exist (only water)
    strfile = fluid2;
    matnum = 1;
    fluid = 'Water';
    p = 2;              %row in middata to be used in fit
elseif exist([path1 '\' fluid2 '_' int2str(anglestart) '_E' int2str(laystart) '.tdms'],'file') == 0     %water data does not exist (only particle)
    strfile = fluid1;
    matnum = 1;
    fluid = 'Particle';
    p = 2;              %row in middata to be used in fit
else                    %2 matrices (particle+water) exist
    matnum = 2;     
    fluid = 'off';
    strfile = fluid1;
    p = 5;              %row in middata to be used in fit 
end

%define relevant angles (degrees) in vector alpha
alpha(:,1) = anglestart : anglegap : angleend;
%convert degrees to radian in vector phi
phi = (alpha(:,1)./360)*2*pi;   

k = 1; %index for figures

%% preparation of measured data
[k,middata,Quot,datnum,Data,subdata] = prep_measurementData(k,path1,laystart,layernum,anglenum,anglegap,matnum,anglestart,fluid2,strfile,p,lbound,alpha,phi);

%% evaluation of raw signal
%k = rawSignal(alpha,laystart,layernum,anglegap,anglenum,lbound,datnum,matnum,fluid,k,Data,subdata);

%% preparation of simulation data
[Simulation,angsim,sim0,simnum,intsize,diamsim] = prep_simulationData(path2,anglestart,angleend);

%% Comparison of fitted measured data with simulation data
[k,Fit,psize,diamselect,a] = comparison_fit_simulation(k,Quot,laystart,layernum,angsim,diam,alpha,intsize,Simulation,simnum,diamsim,sim0);

%% visualization of particle form

if layernum > 1
    [k,diamy] = particleform_ellip(beta,k,layernum,laystart,psize);
end

%% save images
saveFigures(pathSave, ans1, ans2);