% convert all fig files in a folder to tikz files
% created 26 05 2020 by Jesse Ross-Jones

% navigate to folder in Matlab UI then run this script

fileList = dir('*.fig');

for c=1:length(fileList)
    filename = strtok(fileList(c).name,'.');
    
    fig = openfig([filename,'.fig']);
    
    matlab2tikz([filename,'.tex']);
    
    close(fig);
end