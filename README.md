# HeatTransferNanoSilica

This repository hosts the code for generating nano silica geometry and 
numerical evaluation of heat transfer through nano porous materials. 

**GenGeoYade** contains the YADE DEM code for generating nano particle geometry
for various precipitated silica samples. 

**HTOpenLB** contains the OpenLB code for numerical evaluation of heat transfer 
through nano-porous geometry with relevant example inputs as well as test cases 
for 2D and 3D normal calculations

**LightScattering** contains the code for static light scattering, montecarlo 
light scattering as well as the arduino code for automating light scattering 
measurements

**PoreDistributionAnalysis** contains the code for processing the pore distance 
field to evaluate pore size distributions