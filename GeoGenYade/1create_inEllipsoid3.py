__author__ = "Jesse Ross-Jones"
__license__ = "Public Domain"
__version__ = "1.0"
######################################################################
# A script for creating a set of aggregates
#
# Each aggragate is a dense packing, but macroscopically the packing
# is loose
# 
# the aggregates are clump[0-9].txt saved to /tmp
#
# These files contain the particle location and radii for the sample, 
# format x, y, z, r
######################################################################


## Create ellipsoidal aggregates given 
#	aggregate shape (3 radii)
#	pore radius (1 radius)
#	particle radius (1 radius)
#	number of particles
#	number of pores

#	SAXS
#Sample name	d_p [nm]	D_sf [-]	D_mf [-]
#	GT			24.60		2.05		1.95
#	KS			20.70		1.88		2.07
#	CP			15.58		1.93		1.85
#	MX			7.75		2.27		2.23

#	Disk Centrifuge
#Sample name	avg(d_agg) [nm] 	sigma_rel  [%]
#	GT			120.35				13.51
#	KS			116.2				13.44
#	CP			107.25				22.27
#	MX			133.15				27.51

#	Pore size from Hg and BJH
#Sample name	d_(pore,Hg) [um]	Porosity phi [%]	d_(pore,BJH) [nm]
#	GT			0.0283				91.7			43.6
#	KS			0.0252				86.6			40.4
#	CP			0.0091				87.4			33.1
#	MX			0.0058				85.7			14.0
#	MX			0.3557



##########
from yade import ymport
from yade import pack
from numpy import random as rand
from numpy import linalg as ln
from yade import export

def tryvtk():
	O.engines = [
	   ForceResetter(),
	   InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
	   #InteractionLoop(
		  #[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()], ##triax only
		#  [Ig2_Sphere_Sphere_ScGeom()],
		#  [Ip2_FrictMat_FrictMat_FrictPhys()],
		#  [Law2_ScGeom_FrictPhys_CundallStrack()]
	   #),
	   FlowEngine(dead=1,label="flow"),#introduced as a dead engine for the moment, see 2nd section
	   GlobalStiffnessTimeStepper(active=1,timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8),
	   #PeriTriaxController(dynCell=True,mass=0.2,maxUnbalanced=0.01,relStressTol=0.02,goal=(-1e6,-1e6,-1e6),stressMask=3,globUpdate=5,maxStrainRate=(1e10,1e10,1e10),doneHook='triaxDone()',label='compressor'),
	   #NewtonIntegrator(damping=.6), ##Triaxonly
	   NewtonIntegrator(damping=.2),
	]
	factor = .5
	O.dt = factor * PWaveTimeStep()
	flow.dead=0
	flow.defTolerance=0.3
	flow.meshUpdateInterval=200
	flow.useSolver=3
	flow.permeabilityFactor=1
	flow.viscosity=10
	flow.bndCondIsPressure=[0,0,1,1,0,0]
	flow.bndCondValue=[0,0,1,0,0,0]
	flow.boundaryUseMaxMin=[0,0,0,0,0,0]
	O.dt=0.1e-3
	O.dynDt=False
	flow.emulateAction()
	flow.saveVtk()
	raw_input('FlowEngine saveVtk END')
	
	
	vtkExporter = export.VTKExporter('/tmp/vtkExporterTesting')
	vtkExporter.exportSpheres(what=[('dist','b.state.pos.norm()')])

#def createAggregate(radAgg,fdiAgg,clumpNum):
def createAggregate(radAgg, fdiAgg, radPart, fuzPart, radPore, fuzPore, clumpNum):
	
	txt = 'packing.txt'
	O.reset()

	#radAgg = 120.35e-9 / 2. #range 107.25e-9 - 133.15e-9 
	#fdiAgg = Vector3(1,0.6,0.6) # dimension of ellipsoid
	#fdiAgg = Vector3(1,1,1) # dimension of ellipsoid
	c1 = Vector3(radAgg*fdiAgg[0],radAgg*fdiAgg[1],radAgg*fdiAgg[2]) #three radii of ellipsoid
	


	#radPart = 24.6e-9 / 2. #range 7.75e-9 - 24.6e-9
	#radPart = 15.58e-9 / 2. #range 7.75e-9 - 24.6e-9
	#fuzPart = 0.0 #Uniform distribution radii: rad +- (rad*fuz))

	#radPore = 28.3e-9 / 2. #range 5.8e-9  - 355.7e-9  [TODO] why is pore larger than primary particle
	#radPore = 9.1e-9 / 2. #range 5.8e-9  - 355.7e-9  [TODO] why is pore larger than primary particle
	#fuzPore = 0.0 #Uniform distribution radii: rad +- (rad*fuz)) 

	numPart = 2000 #[TODO] calculate numPart to fit in the volume
	numPores= 500

	dim = 1.5e-6 #starting dimension for makeCloud
	dim = dim * 2

	print('Ellipse dimensions: ', c1)
	


	#raw_input('Creating Packing')
	print('Creating Packing')
	# create initial packing
	pores = SpherePack()
	particles = SpherePack()

	sp = SpherePack()
	minc,maxc = Vector3(0,0,0), dim*Vector3(1,1,1)
	sp.makeCloud(minc,maxc,rMean=radPart,rRelFuzz=fuzPart,num=numPart)
	# change num in both cases to get desired mass ratio
	#for c,r in sp:
	#	print(c,r)
	#print(' ' )

	#particles = sp

	sp.makeCloud(minc,maxc,rMean=radPore,rRelFuzz=fuzPore,num=numPores)

	#print(len(sp), ' ', len(particles), '', len(pores) )

	#pores = list(sp - particles)

	#print(len(sp), ' ', len(particles), '', len(pores) )
	print(len(sp), ' Particles generated')
	#for c,r in sp:
	#	print(c,r)

	O.bodies.append([sphere(c,r) for c,r in sp])


		
	#quit()


	#raw_input('Created Packing')

	#sp.toSimulation()
	with open(txt,'w') as f:
		for b in O.bodies:
			x,y,z = b.state.pos
			r = b.shape.radius
			f.write("{} {} {} {}\n".format(x,y,z,r))

	#raw_input('Triax')
	print('Triax Compression')
	# TriaxialTest
	O.reset()
	test = TriaxialTest(importFilename=txt)
	test.load()
	O.run(1000,True)
	# extract cylinder
	mina,maxa = aabbExtrema() #Return coordinates of box enclosing all spherical bodies
	print('min: ', mina, ' , max: ', maxa)
	#raw_input('Finished Compression ')
	print('Finished Compression ')
	
	print('TwoPhase start')
	#TwoPhase=TwoPhaseFlowEngine()
	#TwoPhase.initialization()
	
	#raw_input('Tesselation end')
	
	#tryvtk()
	#raw_input('Pause')
	#quit()
	#raw_input("End try vtk")
	
	

	d0 = max(mina)
	d1 = min(maxa)
	# WARNING if size smaller than requested aggregate size (restart?) [DONE]
	size = (d1-d0)/2.
	print('size: ', size)
	#size = radAgg
	c = d0+.5*size
	c0 = Vector3(c,c,c) #center of the ellipsoid
	
	print(c0)
	
	#x = (min(O.bodies[0].state.pos[0],O.bodies[1].state.pos[0]) - max(O.bodies[0].state.pos[0],O.bodies[1].state.pos[0]))/2.
	#y = (min(O.bodies[2].state.pos[1],O.bodies[3].state.pos[1]) - max(O.bodies[2].state.pos[1],O.bodies[3].state.pos[1]))/2.
	#z = (min(O.bodies[4].state.pos[2],O.bodies[5].state.pos[2]) - max(O.bodies[4].state.pos[2],O.bodies[5].state.pos[2]))/2.
	
	dim=utils.aabbExtrema()
	dim=utils.aabbExtrema()
	xinf=dim[0][0]
	xsup=dim[1][0]
	xdim = xsup-xinf
	X=xinf+(xdim)/2.
	yinf=dim[0][1]
	ysup=dim[1][1]
	ydim = ysup-yinf
	Y=yinf+(ydim)/2.
	zinf=dim[0][2]
	zsup=dim[1][2]
	zdim = zsup-zinf
	Z=zinf+(zdim)/2.
	
	#raw_input(['Dim', xdim, ':', c1[0],  ', ', ydim, ':', c1[1], ', ', zdim, ':', c1[2]])
	if (xdim < c1[0] * 2) or (ydim < c1[1] * 2) or (zdim < c1[2] * 2):
		raw_input('WARNING, aggregate smaller than compressed spheres')
	
	c0 = Vector3(X,Y,Z) #center of the ellipsoid
	print(['center of aggregate: ' , c0])
	#raw_input('Pause')
	
	#c1 = Vector3(size,size,size) #three radii of ellipsoid
	
	#pred = pack.inCylinder(c0,c1,.4*size)
	pred = pack.inEllipsoid(c0,c1) #center, radii
	
	#[TODO]FIX CENTER the aggregate is being cropped [DONE]
	#O.bodies.append([sphere(center=c0,radius=2e-7)])
	
	#raw_input('generate sphere')
	
	cyl = [b.id for b in O.bodies if isinstance(b.shape,Sphere) and pred(b.state.pos,b.shape.radius)]
	
	volPar = 0
	volPor = 0
	with open('clump' + str(clumpNum) + '.txt','w') as f:
		f.write("#format_x_y_z_r\n")
		for i in cyl:
			if i < numPart:
				b = O.bodies[i]
				x,y,z = b.state.pos
				x = x - c0[0]
				y = y - c0[1]
				z = z - c0[2]
				r = b.shape.radius
				volPar += (4./3.) * 3.14159 * b.shape.radius * b.shape.radius * b.shape.radius
				f.write("{} {} {} {}\n".format(x,y,z,r))
			else:
				volPor += (4./3.) * 3.14159 * b.shape.radius * b.shape.radius * b.shape.radius

	volAgg = (4./3.) * 3.14159 * c1[0] * c1[1] * c1[2]
	print(['Total Volume: ', volAgg, ' , Particles: ', volPar, ' , Pores: ', volPor])
	#print('Dimensions: ', O.cell.size)
	#quit()
	##[TODO] Print Porosity
	
	#raw_input('Load')
	# load
	O.reset()
	sphs = ymport.text('clump' + str(clumpNum) + '.txt')
	O.bodies.append(sphs)
	print('Number of particles: ', len(sphs))
	#raw_input('Loaded')
	
	#vtkExporter = export.VTKExporter('aggregate'+str(clumpNum))
	#vtkExporter.exportSpheres(what=[('dist','b.state.pos.norm()')])
	
	
	listOfClumpMembers = []
	for bod in O.bodies:
		listOfClumpMembers.append(bod.id)
		#print(bod)
		print(bod.state.pos)
	#O.bodies.appendClumped(listOfClumpMembers)
	c1 = O.bodies.clump(listOfClumpMembers)
	#O.bodies.clump(O.bodies)
	#export.text('/tmp/clump' + str(num) + '.txt')
	#export.textExt('/tmp/clump' + str(num) + '.txt')
	
	#for b in O.bodies:
	#	b.state.pos = O.cell.wrapPt(b.state.pos)
	#raw_input('Export')
	export.textClumps('/tmp/clump' + str(clumpNum) + '.txt')
	
	dim=utils.aabbExtrema()
	dim=utils.aabbExtrema()
	xinf=dim[0][0]
	xsup=dim[1][0]
	xdim = xsup-xinf
	X=xinf+(xdim)/2.
	yinf=dim[0][1]
	ysup=dim[1][1]
	ydim = ysup-yinf
	Y=yinf+(ydim)/2.
	zinf=dim[0][2]
	zsup=dim[1][2]
	zdim = zsup-zinf
	Z=zinf+(zdim)/2.
	
	print('Export Dimensions: ', xdim,  ', ', ydim,  ', ', zdim)
	#quit()
	
	##########
	
	#print('TwoPhase start')
	#TwoPhase=TwoPhaseFlowEngine()
	#TwoPhase.initialization()
	
	#raw_input('Before FlowEngine saveVtk')
	
#	O.engines = [
#	   ForceResetter(),
#	   InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
#	   #InteractionLoop(
#		  #[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()], ##triax only
#		#  [Ig2_Sphere_Sphere_ScGeom()],
#		#  [Ip2_FrictMat_FrictMat_FrictPhys()],
#		#  [Law2_ScGeom_FrictPhys_CundallStrack()]
#	   #),
#	   FlowEngine(dead=1,label="flow"),#introduced as a dead engine for the moment, see 2nd section
#	   GlobalStiffnessTimeStepper(active=1,timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8),
#	   #PeriTriaxController(dynCell=True,mass=0.2,maxUnbalanced=0.01,relStressTol=0.02,goal=(-1e6,-1e6,-1e6),stressMask=3,globUpdate=5,maxStrainRate=(1e10,1e10,1e10),doneHook='triaxDone()',label='compressor'),
#	   #NewtonIntegrator(damping=.6), ##Triaxonly
#	   NewtonIntegrator(damping=.2),
#	]
#	factor = .5
#	O.dt = factor * PWaveTimeStep()
#	
#	#B. Activate flow engine and set boundary conditions in order to get permeability
#	#TW=TesselationWrapper()
#	#TW.triangulate()
#	#TW.computeVolumes()
#	flow.dead=0
#	flow.defTolerance=0.3
#	flow.meshUpdateInterval=200
#	flow.useSolver=3
#	flow.permeabilityFactor=1
#	flow.viscosity=10
#	flow.bndCondIsPressure=[0,0,1,1,0,0]
#	flow.bndCondValue=[0,0,1,0,0,0]
#	flow.boundaryUseMaxMin=[0,0,0,0,0,0]
#	O.dt=0.1e-3
#	O.dynDt=False
#	flow.emulateAction()
#	flow.saveVtk()
#	raw_input('FlowEngine saveVtk END')

if __name__ == '__main__':
	#number of aggregates to generate
	numAgg = 10
	
	#relative dimensions of elipsoid
	fdiAgg = Vector3(1,0.6,0.6) 
	fdiAgg = Vector3(1.36,0.82,0.82) 
	fdiAgg = Vector3(1,1,1) 
	
	# scale ellipse such that averge diameter is maintained
	mean = (fdiAgg[0] + fdiAgg[1] + fdiAgg[2])/3
	print(mean)
	#fdiAgg = fdiAgg / mean 
	print(fdiAgg)
	
	
	sample = 'gt3000'
	sample = 'cp513'
	sample = 'ks408'
	sample = 'mx109'
	
	#silica sample to use
	if   sample == 'gt3000': aggRadii = rand.normal(120.35,13.51,numAgg)	# GT3000
	elif sample == 'cp513' : aggRadii = rand.normal(107.25,22.27,numAgg)	# CP513
	elif sample == 'ks408' : aggRadii = rand.normal(116.20,13.44,numAgg)	# KS408
	elif sample == 'mx109' : aggRadii = rand.normal(113.15,27.51,numAgg)	# MX109
	else: 
		print('invalid aggregate size')
		quit()
	
	#convert to nm and radius!
	aggRadii *= 1e-9 / 2.
	print('agg. radii: ',aggRadii)
	
	
	
	#primary particles
	if   sample == 'gt3000': radPart = 24.60e-9 / 2. 	# GT3000
	elif sample == 'cp513' : radPart = 15.58e-9 / 2. 	# CP513
	elif sample == 'ks408' : radPart = 20.70e-9 / 2. 	# KS408
	elif sample == 'mx109' : radPart = 7.750e-9 / 2. 	# MX109
	else: 
		print('invalid primary particle size')
		quit()
	fuzPart = 0.0 #Uniform distribution radii: rad +- (rad*fuz))

	if   sample == 'gt3000': radPore = 28.3e-9 / 2. #/ 2. # GT3000  [TODO] why is pore larger than primary particle
	elif sample == 'cp513' : radPore = 9.10e-9 / 2. #/ 2.# CP513   [TODO] why is pore larger than primary particle
	elif sample == 'ks408' : radPore = 25.2e-9 / 2. # KS408
	elif sample == 'mx109' : radPore = 5.80e-9 / 2. # MX109
	else: 
		print('invalid primary pore size')
		quit()
	fuzPore = 0.0 #Uniform distribution radii: rad +- (rad*fuz)) 
	
	print('Sample: ', sample, ', aggRadii: ', aggRadii, ', PrimaryDiam: ', radPart*2, ', PoreDiam: ', radPore*2)
	#quit()
	
	for idx, radAgg in enumerate(aggRadii):
		print('Generating Aggregate number: ', idx)
		#if idx == 9:
		createAggregate(radAgg, fdiAgg, radPart, fuzPart, radPore, fuzPore, idx)
		raw_input(['i: ', idx, ' , rad: ', radAgg])
	raw_input('End')
	quit()
