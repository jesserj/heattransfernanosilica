__author__ = "Jesse Ross-Jones"
__license__ = "Public Domain"
__version__ = "1.0"
######################################################################
# A script for creating a compressed packing
# 
# the compressed packing is saved to two files
# compressed.txt contains the compressed packing for the sample
# compressed_peri.txt contains the compressed and periodic on all axis
#
# These files contain the particle location and radii for the sample, 
# format x, y, z, r
#
# Will also export two xmls for use in OpenLB
# the first with the periodic particles 
# the second with the defined bounding box
######################################################################


#folderName = 'compressNetwork2'

def tryvtk():
	from yade import export
	
	vtkExporter = export.VTKExporter('compressNetwork2/vtkGeometry')
	vtkExporter.exportSpheres(what=[('dist','b.state.pos.norm()')])
	
	print('Spheres Exported')
	
	print('Starting flow engine and triangulation export')
	
	O.engines = [
	   ForceResetter(),
	   InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
	   #InteractionLoop(
		  #[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()], ##triax only
		#  [Ig2_Sphere_Sphere_ScGeom()],
		#  [Ip2_FrictMat_FrictMat_FrictPhys()],
		#  [Law2_ScGeom_FrictPhys_CundallStrack()]
	   #),
	   FlowEngine(dead=1,label="flow"),#introduced as a dead engine for the moment, see 2nd section
	   GlobalStiffnessTimeStepper(active=1,timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8),
	   #PeriTriaxController(dynCell=True,mass=0.2,maxUnbalanced=0.01,relStressTol=0.02,goal=(-1e6,-1e6,-1e6),stressMask=3,globUpdate=5,maxStrainRate=(1e10,1e10,1e10),doneHook='triaxDone()',label='compressor'),
	   #NewtonIntegrator(damping=.6), ##Triaxonly
	   NewtonIntegrator(damping=.2),
	]
	factor = .5
	O.dt = factor * PWaveTimeStep()
	flow.dead=0
	flow.defTolerance=0.3
	flow.meshUpdateInterval=200
	flow.useSolver=3
	flow.permeabilityFactor=1
	flow.viscosity=10
	flow.bndCondIsPressure=[0,0,1,1,0,0]
	flow.bndCondValue=[0,0,1,0,0,0]
	flow.boundaryUseMaxMin=[0,0,0,0,0,0]
	O.dt=0.1e-4
	O.dynDt=False
	flow.emulateAction()
	flow.saveVtk('compressNetwork2/vtkNetwork')
	raw_input('FlowEngine saveVtk END')
	

def getDims():
	dim=utils.aabbExtrema()
	dim=utils.aabbExtrema()
		
	xinf=dim[0][0]
	xsup=dim[1][0]
	xdim = xsup-xinf
	X=xinf+(xdim)/2.
	yinf=dim[0][1]
	ysup=dim[1][1]
	ydim = ysup-yinf
	Y=yinf+(ydim)/2.
	zinf=dim[0][2]
	zsup=dim[1][2]
	zdim = zsup-zinf
	Z=zinf+(zdim)/2.
	
	testMax = max([xdim,ydim,zdim])
	
	#print(['dim :' , [xdim,ydim,zdim]])
	#print(['max :' , testMax])
	#print(['max2:' , max([testMax, maxDim])])
	
	return [[xinf,yinf,yinf],[xsup,ysup,zsup]]
	
def checkPoreSizes():
	
	spheres = []
	for b in O.bodies:
		if b.isClump == False:
			spheres.append([b.state.pos, b.shape.radius])
	print(['Number of particles: ', len(spheres)])
	O.switchScene(); O.resetThisScene() #####!!!!!!!	
	
	for pos,r in spheres:
		O.bodies.append(sphere(pos,r))
		
	raw_input('Pause after recreating spheres')
	#TwoPhase=TwoPhaseFlowEngine()
	#TwoPhase.initialization()
	#TwoPhase.savePoreNetwork('compressNetwork2')
	tryvtk()
	O.switchScene(); 

def compressGeo(targetPorosity = 0.93):
	#compressionLevel = 3
	
	O.engines = [
	   ForceResetter(),
	   InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
	   InteractionLoop(
		  #[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()], ##triax only
		  [Ig2_Sphere_Sphere_ScGeom()],
		  [Ip2_FrictMat_FrictMat_FrictPhys()],
		  [Law2_ScGeom_FrictPhys_CundallStrack()]
	   ),
	   #TriaxialStressController(
		  #thickness = 0,
		  #stressMask = 7,
		  #internalCompaction = False,
		  #label = 'compressor',
	   #),
	   #~ PeriTriaxController(
		  #~ #thickness = 0,
		  #~ stressMask = 7,
		  #~ #internalCompaction = False,

		  #~ # type of servo-control
		  #~ dynCell=True,
		  #~ #maxStrainRate=(10,10,10),
		  #~ # wait until the unbalanced force goes below this value
		  #~ maxUnbalanced=.1,relStressTol=1e-9,
		  #~ # call this function when goal is reached and the packing is stable
		  #~ doneHook='compactionFinished()',
		  #~ label = 'compressor'
	   #~ ),
	   FlowEngine(dead=1,label="flow"),#introduced as a dead engine for the moment, see 2nd section
	   GlobalStiffnessTimeStepper(active=1,timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8),
	   PeriTriaxController(dynCell=True,mass=0.2,maxUnbalanced=0.01,relStressTol=0.02,goal=(-1e6,-1e6,-1e6),stressMask=3,globUpdate=5,maxStrainRate=(1e10,1e10,1e10),doneHook='triaxDone()',label='compressor'),
	   #NewtonIntegrator(damping=.6), ##Triaxonly
	   NewtonIntegrator(damping=.2),
	]
	factor = .5
	O.dt = factor * PWaveTimeStep()
	#raw_input('Pause before make ghosts real, press any key to continue')
	
	#compressor.allowBiggerThanPeriod=True
	
	
	#print(compressor.height)
	compressor.goal1 = compressor.goal2 = compressor.goal3 = -1e-3
	compressor.goal1 = compressor.goal2 = compressor.goal3 = -1e-5
	
	sigmaIso = -1e-10
	#compressor.goal = (sigmaIso,sigmaIso,sigmaIso)
	
	#raw_input('Pausing before tesselation, press any key to continue')
	#tesselate()
	#raw_input('Pausing before compression, press any key to continue')
	
	#targetPorosity = 0.85
	runNum = 0
	#compFricDegree = 30
	#O.materials.append(FrictMat(young=young,poisson=0.5,frictionAngle=radians(compFricDegree),density=2600,label='spheres'))
	#O.materials.append(FrictMat(young=young,poisson=0.5,frictionAngle=0,density=0,label='walls'))
	
	print('Porosity target: ', targetPorosity)
	while porosity()>targetPorosity:
	## we decrease friction value and apply it to all the bodies and contacts
		#compFricDegree = 0.95*compFricDegree
		#setContactFriction(radians(compFricDegree))
		#print "\r Friction: ",compFricDegree," porosity:",triax.porosity,
		#sys.stdout.flush()
	## while we run steps, triax will tend to grow particles as the packing
	## keeps shrinking as a consequence of decreasing friction. Consequently
	## porosity will decrease
		print('Run: ', runNum, ' , Porosity: ', porosity())
		print('Extend: ', getExtend())
		O.run(5000,1)
		runNum += 1
		
	return
	
	O.run(50000,True)
	for i in range(compressionLevel):
		print('strain: ', compressor.strain)
		O.run(50000,True)
		tesselate()
		#raw_input(['Compress: ' , i])
	
	
def tesselate():
	TW=TesselationWrapper()
	TW.triangulate()
	TW.computeVolumes()
	
	for i in O.bodies:
		if i.isClump == False:
			#volTW+=TW.volume(i.id)
			print('ID: ', i.id, ' Pos: ', i.state.pos, ' Volume: ', TW.volume(i.id))
		else:
			#print("Local ", i.id, " Porosity: CLUMP")
			pass

def prettyPrint(element, level=0):
	'''
	Printing in elementTree requires a little massaging
	Function taken from elementTree site:
	http://effbot.org/zone/element-lib.htm#prettyprint

	'''
	indent = '\n' + level * '  '
	if len(element):
		if not element.text or not element.text.strip():
			element.text = indent + '  '

		if not element.tail or not element.tail.strip():
			element.tail = indent
 
		for element in element:
			prettyPrint(element, level + 1)

		if not element.tail or not element.tail.strip():
			element.tail = indent

	else:
		if level and (not element.tail or not element.tail.strip()):
			element.tail = indent

	return element

def saveXML2(compressionLevel,fileName,fileNamePeri,radiusPlus=0):
	from xml.etree import cElementTree as ET
	display = 0
	rootNode = ET.Element('IndicatorUnion3D')

	#f = open("VTK2periodicSpheres","r")

	row = []
	f = open(fileName,"r")
	for line in f.readlines():
		row.append([line])
		for i in line.split('\t'):
			i = i.strip('\n')
			row[-1].append(i)
	f.close()
	#print row
	#print(row[1])
	#print(row[1][4])
	#print(len(row))
	for i in range(1,len(row)):
		sphere = row[i]
		#print(sphere[2])
		radius = float(sphere[4]) + radiusPlus
		location = [float(sphere[1]),float(sphere[2]),float(sphere[3])]
		
		if display:
			bpy.ops.mesh.primitive_uv_sphere_add(size=radius, view_align=False, enter_editmode=False, location=location, layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
		
		elementNode = ET.Element('IndicatorSphere3D ')
	   
		rootNode.append(elementNode)
	   
		#p1tup = (str(p1.x), " ", str(p1.y), " ", str(p1.z))
		p1tup = (str(float(sphere[1])), " ", str(float(sphere[2])), " ", str(float(sphere[3])))
		p1join = ''.join(p1tup)
		elementNode.attrib['center'] = p1join
	   
		#p2tup = (str(p2.x), " ", str(p2.y), " ", str(p2.z))
		#p2join = ''.join(p2tup)
		#elementNode.attrib['center2'] = p2join
	   
		elementNode.attrib['radius'] = str(radius)
	   
		#f.write("<IndicatorCylinder3D center1="0 0 0" center2="10 0 0" radius="2"/>")
	   
		#worldify = lambda p: om * Vector(p[:])
		#coords = [worldify(p).to_tuple() for p in local_coords]
	   
		#print(coords)
		#bpy.context.object.modifiers.clear()
	   
		#bpy.ops.rigidbody.object_add()
	 
	numParticles = len(row)
########
	row = []
	f = open(fileNamePeri,"r")
	for line in f.readlines():
		row.append([line])
		for i in line.split('\t'):
			i = i.strip('\n')
			row[-1].append(i)
	f.close()
	#print row
	#print(row[1])
	#print(row[1][4])
	#print(len(row))
	for i in range(1,len(row)):
		sphere = row[i]
		#print(sphere[2])
		#print(sphere)
		radius = float(sphere[4]) + radiusPlus
		location = [float(sphere[1]),float(sphere[2]),float(sphere[3])]
		
		if display:
			bpy.ops.mesh.primitive_uv_sphere_add(size=radius, view_align=False, enter_editmode=False, location=location, layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
		
		elementNode = ET.Element('IndicatorSphere3D ')
	   
		rootNode.append(elementNode)
	   
		#p1tup = (str(p1.x), " ", str(p1.y), " ", str(p1.z))
		p1tup = (str(float(sphere[1])), " ", str(float(sphere[2])), " ", str(float(sphere[3])))
		p1join = ''.join(p1tup)
		elementNode.attrib['center'] = p1join
	   
		#p2tup = (str(p2.x), " ", str(p2.y), " ", str(p2.z))
		#p2join = ''.join(p2tup)
		#elementNode.attrib['center2'] = p2join
	   
		elementNode.attrib['radius'] = str(radius)
	   
		#f.write("<IndicatorCylinder3D center1="0 0 0" center2="10 0 0" radius="2"/>")
	   
		#worldify = lambda p: om * Vector(p[:])
		#coords = [worldify(p).to_tuple() for p in local_coords]
	   
		#print(coords)
		#bpy.context.object.modifiers.clear()
	   
		#bpy.ops.rigidbody.object_add()
##############
	numParticles+=len(row)
	# Pretty print the rootNode
	prettyPrint(element=rootNode)
	 
	xmlText = ET.tostring(rootNode)
	 
	# FINAL RESULT, we need to append the HEADER
	#print ( '<?xml version="1.0" ?>\n{xmlText}'.format(xmlText=xmlText) )
	
	#aps = calculatePoreSize()
	 
	f = open('spheresXML_N' + str(numParticles) +'_C' + str(compressionLevel) +'_r' + str(radiusPlus) +'.xml' , 'w')
	f.write('<?xml version="1.0" ?>\n' + xmlText.decode('utf8'))
	f.close()
	print('Num particles inc. periodic', numParticles)
	#for sphere in row:
		#print(len(sphere))
		#print(sphere)
		#radius = sphere[4]
		#location = [sphere[1],sphere[2],sphere[3]]
		#bpy.ops.mesh.primitive_uv_sphere_add(size=radius, view_align=False, enter_editmode=False, location=location, layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))


	#quit()

	#lines = f.readlines()
 
 
def saveXML(numParticles):
	from xml.etree import cElementTree as ET
	display = 0
	rootNode = ET.Element('IndicatorUnion3D')

	#f = open("VTK2periodicSpheres","r")

	row = []
	f = open("/tmp/compressed.txt","r")
	for line in f.readlines():
		row.append([line])
		for i in line.split('\t'):
			i = i.strip('\n')
			row[-1].append(i)
	f.close()
	#print row
	#print(row[1])
	#print(row[1][4])
	#print(len(row))
	for i in range(1,len(row)):
		sphere = row[i]
		#print(sphere[2])
		radius = float(sphere[4])
		location = [float(sphere[1]),float(sphere[2]),float(sphere[3])]
		
		if display:
			bpy.ops.mesh.primitive_uv_sphere_add(size=radius, view_align=False, enter_editmode=False, location=location, layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
		
		elementNode = ET.Element('IndicatorSphere3D ')
	   
		rootNode.append(elementNode)
	   
		#p1tup = (str(p1.x), " ", str(p1.y), " ", str(p1.z))
		p1tup = (str(float(sphere[1])), " ", str(float(sphere[2])), " ", str(float(sphere[3])))
		p1join = ''.join(p1tup)
		elementNode.attrib['center'] = p1join
	   
		#p2tup = (str(p2.x), " ", str(p2.y), " ", str(p2.z))
		#p2join = ''.join(p2tup)
		#elementNode.attrib['center2'] = p2join
	   
		elementNode.attrib['radius'] = str(radius)
	   
		#f.write("<IndicatorCylinder3D center1="0 0 0" center2="10 0 0" radius="2"/>")
	   
		#worldify = lambda p: om * Vector(p[:])
		#coords = [worldify(p).to_tuple() for p in local_coords]
	   
		#print(coords)
		#bpy.context.object.modifiers.clear()
	   
		#bpy.ops.rigidbody.object_add()
	 
	 
	# Pretty print the rootNode
	prettyPrint(element=rootNode)
	 
	xmlText = ET.tostring(rootNode)
	 
	# FINAL RESULT, we need to append the HEADER
	print ( '<?xml version="1.0" ?>\n{xmlText}'.format(xmlText=xmlText) )
	 
	f = open('compressedXML_' + str(len(row)) + '.xml' , 'w')
	f.write('<?xml version="1.0" ?>\n' + xmlText.decode('utf8'))
	f.close()
	#for sphere in row:
		#print(len(sphere))
		#print(sphere)
		#radius = sphere[4]
		#location = [sphere[1],sphere[2],sphere[3]]
		#bpy.ops.mesh.primitive_uv_sphere_add(size=radius, view_align=False, enter_editmode=False, location=location, layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))


	#quit()

	#lines = f.readlines()

def saveCuboidXML(extend,origin,compressionLevel):
	from xml.etree import cElementTree as ET
	display = 0
	rootNode = ET.Element('IndicatorUnion3D')

	#sphere = row[i]
	#print(sphere[2])
	#radius = float(sphere[4])
	#location = [float(sphere[1]),float(sphere[2]),float(sphere[3])]
	
	elementNode = ET.Element('IndicatorCuboid3D ')
   
	rootNode.append(elementNode)
   
	#p1tup = (str(p1.x), " ", str(p1.y), " ", str(p1.z))
	p1tup = (str(float(extend[0])), " ", str(float(extend[1])), " ", str(float(extend[2])))
	p1join = ''.join(p1tup)
	elementNode.attrib['extend'] = p1join
   
	p2tup = (str(float(origin[0])), " ", str(float(origin[1])), " ", str(float(origin[2])))
	p2join = ''.join(p2tup)
	elementNode.attrib['origin'] = p2join
	
	# Pretty print the rootNode
	prettyPrint(element=rootNode)
	 
	xmlText = ET.tostring(rootNode)
	 
	# FINAL RESULT, we need to append the HEADER
	print ( '<?xml version="1.0" ?>\n{xmlText}'.format(xmlText=xmlText) )
	 
	f = open('cuboidXML_C' + str(compressionLevel) + '.xml' , 'w')
	f.write('<?xml version="1.0" ?>\n' + xmlText.decode('utf8'))
	f.close()


def getExtend():
	extend = O.cell.size
	return extend
	
def getOrigin():
	origin = Vector3();
	for b in O.bodies:
		if b.isClump == False:
			xmin,xmax = O.cell.wrap(b.bound.min)[0], O.cell.wrap(b.bound.max)[0] # wrap is important as physically the particles can be anywhere
			ymin,ymax = O.cell.wrap(b.bound.min)[1], O.cell.wrap(b.bound.max)[1] 
			zmin,zmax = O.cell.wrap(b.bound.min)[2], O.cell.wrap(b.bound.max)[2]
			if xmin < origin[0]: origin[0] = xmin
			if ymin < origin[1]: origin[1] = zmin
			if zmin < origin[2]: origin[2] = zmin
	extend=getExtend()
	origin = Vector3(0,0,0) - extend/2.
	origin = Vector3(0,0,0)
	return origin
	
def makeGhostsReal(makeReal):
	#inspiration from https://www.mail-archive.com/yade-users@lists.launchpad.net/msg08994.html
	from yade import pack
	#pack.randomPeriPack(3.5,(45,45,45), rRelFuzz = 0.5).toSimulation()
	O.step() # to initialize bounding boxes
	xImages = [] # list of "edge" particles

	print('Number of spheres in ', len(O.bodies))
	
	if(makeReal):
		print('Making ghosts real')
		for b in O.bodies:
			if b.isClump == False:
				xmin,xmax = O.cell.wrap(b.bound.min)[0], O.cell.wrap(b.bound.max)[0] # wrap is important as physically the particles can be anywhere

				if xmin > xmax: # this means that bounding box crosses periodic cell. You can define various different conditions..
					#print(b.id, ' crosses cell boundary')
					xImages.append(b)

		pr = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in xImages] # list or [pos,radius] of "edge" particles. Now pos is always inside the cell
		#print(xImages)
		
		from copy import deepcopy
		pr_Orig = deepcopy(pr)

		for i,(pos,r) in enumerate(pr):
			shift = Vector3(O.cell.size[0],0,0) * (1 if pos[0]<0.5*O.cell.size[0] else -1) # determine shift to create a periodic image +x... direction if particles is near -x face and vice versa
			#print(i, ' ' , pr[i][0])
			pr[i][0] += shift
			#print(i, ' ' , pr[i][0])
			#print(i, ' ', pr_Orig[i][0])
			
		#print('shift: ' , shift)
		#print(pr)

		
		#raw_input("X finished Press Enter to continue...")

		# saves images into a file
		#f = open("/home/konda/imp/Extra_imp/one_packing/1_1_x.dat","w")
		#f.write("x y z r\n")
		#f.writelines("%g %g %g %g\n"%(pos[0],pos[1],pos[2],r) for pos,r in pr)
		#f.close()

		yImages = []

		for b in O.bodies:
			if b.isClump == False:
				#print(b.id)
				ymin,ymax = O.cell.wrap(b.bound.min)[1], O.cell.wrap(b.bound.max)[1]
				if ymin > ymax:
					yImages.append(b)

		pr1 = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in yImages]
		
		pr1_Orig = deepcopy(pr1)

		for i,(pos,r) in enumerate(pr1):
			shift = Vector3(0,O.cell.size[1],0) * (1 if pos[1]<0.5*O.cell.size[1] else -1)
			#print(i, ' ' , pr1[i][0])
			pr1[i][0] += shift
			#print(i, ' ' , pr1[i][0])


		# saves images into a file
		#f = open("/home/konda/imp/Extra_imp/one_packing/1_1_y.dat","w")
		#f.write("x y z r\n")
		#f.writelines("%g %g %g %g\n"%(pos[0],pos[1],pos[2],r) for pos,r in pr1)
		#f.close()

		zImages = []

		for b in O.bodies:
			if b.isClump == False:
				zmin,zmax = O.cell.wrap(b.bound.min)[2], O.cell.wrap(b.bound.max)[2]
				if zmin > zmax:
					zImages.append(b)

		pr2 = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in zImages]
		
		pr2_Orig = deepcopy(pr2)

		for i,(pos,r) in enumerate(pr2):
			shift = Vector3(0,0,O.cell.size[2]) * (1 if pos[2]<0.5*O.cell.size[2] else -1)
			pr2[i][0] += shift
		
		#raw_input("Finding single plane peri particles finished Press Enter to continue...")
		
		#for i,(pos,r) in enumerate(pr2):
		#	print i
		#	print(pr2[i], pr2_Orig[i])
			
		#raw_input("Finished printing spheres Press Enter to continue...")
		xyImages = []
		for b in O.bodies:
			if b.isClump == False:
				xmin,xmax = O.cell.wrap(b.bound.min)[0],O.cell.wrap(b.bound.max)[0] # x bounds
				ymin,ymax = O.cell.wrap(b.bound.min)[1],O.cell.wrap(b.bound.max)[1] # y bounds
				if xmin > xmax and ymin > ymax: # note condition in two direction
					xyImages.append(b)
		prXY = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in xyImages]
		prXY_Orig = deepcopy(prXY)
		for i,(pos,r) in enumerate(prXY):
			xShift = (1 if pos[0]<0.5*O.cell.size[0] else -1) # positive or negative shift in x direction
			yShift = (1 if pos[1]<0.5*O.cell.size[1] else -1) # positive or	negative shift in y direction
			shift = Vector3(xShift*O.cell.size[0],yShift*O.cell.size[1],0) #actual shift of "ghost"
			prXY[i][0] += shift
		#raw_input("XY")
		#############################
		yzImages = []
		for b in O.bodies:
			if b.isClump == False:
				ymin,ymax = O.cell.wrap(b.bound.min)[1],O.cell.wrap(b.bound.max)[1] # y bounds
				zmin,zmax = O.cell.wrap(b.bound.min)[2],O.cell.wrap(b.bound.max)[2] # z bounds
				if ymin > ymax and zmin > zmax: # note condition in two direction
					yzImages.append(b)
		prYZ = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in yzImages]
		prYZ_Orig = deepcopy(prYZ)
		for i,(pos,r) in enumerate(prYZ):
			yShift = (1 if pos[1]<0.5*O.cell.size[1] else -1) # positive or negative shift in y direction
			zShift = (1 if pos[2]<0.5*O.cell.size[2] else -1) # positive or	negative shift in z direction
			shift = Vector3(0,yShift*O.cell.size[1],zShift*O.cell.size[2]) #actual shift of "ghost"
			prYZ[i][0] += shift
		#raw_input("YZ")
		#############################
		xzImages = []
		for b in O.bodies:
			if b.isClump == False:
				xmin,xmax = O.cell.wrap(b.bound.min)[0],O.cell.wrap(b.bound.max)[0] # x bounds
				zmin,zmax = O.cell.wrap(b.bound.min)[2],O.cell.wrap(b.bound.max)[2] # z bounds
				if xmin > xmax and zmin > zmax: # note condition in two direction
					xzImages.append(b)
		prXZ = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in xzImages]
		prXZ_Orig = deepcopy(prXZ)
		for i,(pos,r) in enumerate(prXZ):
			xShift = (1 if pos[0]<0.5*O.cell.size[0] else -1) # positive or negative shift in x direction
			zShift = (1 if pos[2]<0.5*O.cell.size[2] else -1) # positive or	negative shift in z direction
			shift = Vector3(xShift*O.cell.size[0],0,zShift*O.cell.size[2]) #actual shift of "ghost"
			prXZ[i][0] += shift
		#raw_input("XZ")
		#raw_input("Finding peri particles finished Press Enter to continue...")
		
		##### [TODO] copy xyz particles as well
		xyzImages = []
		for b in O.bodies:
			if b.isClump == False:
				xmin,xmax = O.cell.wrap(b.bound.min)[0],O.cell.wrap(b.bound.max)[0] # x bounds
				ymin,ymax = O.cell.wrap(b.bound.min)[1],O.cell.wrap(b.bound.max)[1] # y bounds
				zmin,zmax = O.cell.wrap(b.bound.min)[2],O.cell.wrap(b.bound.max)[2] # z bounds
				if xmin > xmax and ymin > ymax and zmin > zmax: # note condition in two direction
					xyzImages.append(b)
		prXYZ = [[O.cell.wrap(b.state.pos),b.shape.radius] for b in xyzImages]
		prXYZ_Orig = deepcopy(prXYZ)
		for i,(pos,r) in enumerate(prXYZ):
			xShift = (1 if pos[0]<0.5*O.cell.size[0] else -1) # positive or negative shift in x direction
			yShift = (1 if pos[1]<0.5*O.cell.size[1] else -1) # positive or negative shift in y direction
			zShift = (1 if pos[2]<0.5*O.cell.size[2] else -1) # positive or	negative shift in z direction
			shift = Vector3(xShift*O.cell.size[0],yShift*O.cell.size[1],zShift*O.cell.size[2]) #actual shift of "ghost"
			prXYZ[i][0] += shift
		#raw_input("XYZ")
		########## Add the missing spheres to the simulation	
		O.switchScene(); O.resetThisScene()
		
		for i,(pos,r) in enumerate(pr):
			#print(b)
			#print(i, ' pos: ', pos , ' , radius: ', r)
			#raw_input("Appending Particles Press Enter to continue...")
			#print('1Number of spheres in ', len(O.bodies))
			#O.bodies.append(sphere(pos,r))
			#print(pr_Orig[i][0])
			#print(i, ' pos: ', pr_Orig[i][0] , ' , radius: ', pr_Orig[i][1])
			#raw_input("Appending PeriTicle Press Enter to continue...")
			#print('2Number of spheres in ', len(O.bodies))
			#O.bodies.append(sphere(pr_Orig[i][0],pr_Orig[i][1]))
			#print('Error spheres are added in the wrong place') #[TODO]
			#print('Number of spheres in ', len(O.bodies))
			#raw_input("Press Enter to continue...")
			O.bodies.appendClumped([sphere(pos,r),sphere(pr_Orig[i][0],pr_Orig[i][1])])
		#raw_input('Pause before returning to scene1')
		for i,(pos,r) in enumerate(pr1):
			#O.bodies.append(sphere(pos,r))
			#O.bodies.append(sphere(pr_Orig[i][0],pr_Orig[i][1]))
			O.bodies.appendClumped([sphere(pos,r),sphere(pr1_Orig[i][0],pr1_Orig[i][1])])
		#raw_input('Pause before returning to scene1')
		for i,(pos,r) in enumerate(pr2):
			#O.bodies.append(sphere(pos,r))
			#O.bodies.append(sphere(pr_Orig[i][0],pr_Orig[i][1]))
			O.bodies.appendClumped([sphere(pos,r),sphere(pr2_Orig[i][0],pr2_Orig[i][1])])
		#raw_input('Pause before returning to scene1')
		for i,(pos,r) in enumerate(prXY):
			O.bodies.appendClumped([sphere(pos,r),sphere(prXY_Orig[i][0],prXY_Orig[i][1])])
		#raw_input('Pause before returning to scene1')
		for i,(pos,r) in enumerate(prYZ):
			O.bodies.appendClumped([sphere(pos,r),sphere(prYZ_Orig[i][0],prYZ_Orig[i][1])])
		#raw_input('Pause before returning to scene1')
		for i,(pos,r) in enumerate(prXZ):
			O.bodies.appendClumped([sphere(pos,r),sphere(prXZ_Orig[i][0],prXZ_Orig[i][1])])
		#raw_input('Pause before returning to scene1')
		for i,(pos,r) in enumerate(prXYZ):
			O.bodies.appendClumped([sphere(pos,r),sphere(prXYZ_Orig[i][0],prXYZ_Orig[i][1])])
		#raw_input("ALL")
	print('Exporting compressed_peri.txt')
	from yade import export
	#export.textExt('/tmp/compressed_peri.txt','x_y_z_r_attrs',attrs=['b.agglomerate'])
	export.textExt('/tmp/compressed_peri.txt','x_y_z_r')
	#saveXML(len(O.bodies),'/tmp/compressed_peri.txt')
	O.switchScene();
	
	#~ for i in range(10):
		#~ O.switchScene();
		#~ raw_input('Pause before returning to scene1')
		#~ print('Number of spheres in ', len(O.bodies))
	
	#~ for i,(pos,r) in enumerate(pr):
		#~ O.bodies.appendClumped([sphere(pos,r),sphere(pr_Orig[i][0],pr_Orig[i][1])])
		
	#~ for i,(pos,r) in enumerate(pr1):
		#~ O.bodies.appendClumped([sphere(pos,r),sphere(pr1_Orig[i][0],pr1_Orig[i][1])])

	#~ for i,(pos,r) in enumerate(pr2):
		#~ O.bodies.appendClumped([sphere(pos,r),sphere(pr2_Orig[i][0],pr2_Orig[i][1])])
	#~ print('Number of spheres in ', len(O.bodies))
	
readParamsFromTable(compressionLevel=1,radius=7.79e-09,N=4)
# make rMean, rRelFuzz, maxLoad accessible directly as variables later
from yade.params.table import *

def main():
	
	
	print('Default resolution and radius:', N, radius)
	

	from yade import export,ymport
	import random
	random.seed(1)
	

	#dim = Vector3(2e-6,2e-6,2e-6)
	
	sample = 'gt3000'
	#sample = 'cp513'
	#sample = 'ks408'
	#sample = 'mx109'
	
	if   sample == 'gt3000': targetPorosity = 0.917		# GT3000
	elif sample == 'cp513' : targetPorosity = 0.874		# CP513
	elif sample == 'ks408' : targetPorosity = 0.866		# KS408
	elif sample == 'mx109' : targetPorosity = 0.857		# MX109
	else: 
		print('invalid aggregate size')
		quit()
	
	print('Sample: ' , sample, ', target porosity: ', targetPorosity)

	O.materials.append(FrictMat(density=1e6,frictionAngle=0))
	#O.materials.append(FrictMat(young=young,poisson=1,frictionAngle=radians(compFricDegree),density=2600,label='spheres'))
	#O.materials.append(FrictMat(poisson=0.5,frictionAngle=0,density=0,label='spheres'))
	O.periodic=True
	#O.cell.setBox(1e-6,1e-6,1e-6)
	sizeMultiplier = 2.
	O.cell.setBox(5e-7*sizeMultiplier,5e-7*sizeMultiplier,5e-7*sizeMultiplier)
	
	#compressionLevel = 3
	
	attrs = []
	
	#raw_input("start")
	
	#sp = ymport.textExt('/tmp/clump0.txt',format='x_y_z_r_attrs',attrs=attrs)
	#sp = ymport.textClumps('/tmp/clump0.txt')
	
	sp = ymport.textExt('/tmp/divided.txt',format='x_y_z_r_attrs',attrs=attrs)
	n = max(int(a[0]) for a in attrs)+1
	colors = [randomColor() for _ in xrange(n)]
	agglomerates = [[] for _ in xrange(n)]
	for s,a in zip(sp,attrs):
	   aa = int(a[0])
	   s.agglomerate = aa
	   s.shape.color = colors[aa]
	   agglomerates[aa].append(s)
	for a in agglomerates:
	   O.bodies.appendClumped(a)
	
	
	print(['Number of bodies: ', len(O.bodies)])
	print(['Dimensions', getDims()])
	print('Extends: ', getExtend())
	#quit()
	
	print('Porosity before compression: ', porosity())
	raw_input("Beofre Compression")
	#quit()
	
	#print('Before Compressed Geometry')
	#compressGeo(compressionLevel)
	compressGeo(targetPorosity)
	#quit()
	
	print('Porosity after compression: ', porosity())
	#raw_input("quit")
	#quit()
	
	#export.textExt('/tmp/compressed1.txt','x_y_z_r_attrs',attrs=['b.agglomerate'])
	#raw_input('After Compressed Geometry')
	
	spheres = []
	for b in O.bodies:
		if b.isClump == False:
			spheres.append([b.state.pos, b.shape.radius])
	print(['Number of particles: ', len(spheres)])
	
	sceneDim = getDims()
	#raw_input('Pause0')
	print(sceneDim[0],sceneDim[1])
	pred=pack.inAlignedBox(sceneDim[0],sceneDim[1]) 
	for pos,r in spheres:
		pred -= pack.inSphere(pos,r)
		#print(['Pos: ',pos,' radius: ',r])
	#raw_input('Pause1')
	
	for b in O.bodies:
		b.state.pos = O.cell.wrapPt(b.state.pos)
	
	#export.textExt('/tmp/compressed.txt','x_y_z_r_attrs',attrs=['b.agglomerate'])
	export.textExt('/tmp/compressed.txt','x_y_z_r_attrs',attrs=['b.agglomerate'])
	raw_input('Beofre Ghosts')
	makeGhostsReal(1) #also saves compressed_peri.txt
	#export.textExt('/tmp/compressed_peri.txt','x_y_z_r')
	#saveXML()
	saveXML2(compressionLevel,'/tmp/compressed.txt','/tmp/compressed_peri.txt')
	
	origin = getOrigin()
	extend = getExtend()
	
	saveXML2(compressionLevel,'/tmp/compressed.txt','/tmp/compressed_peri.txt', radius/N)
	
	print('Porosity after periodic: ', porosity())
	#raw_input("quit")
	#quit()
	
	print('Origin: ', origin)
	print('Extends: ', extend)
	saveCuboidXML(extend,origin,compressionLevel)
	
	print('Porosity: ', porosity())
	raw_input("quit")
	quit()
	checkPoreSizes()
	
	raw_input('Switch back')
	
	TwoPhase=TwoPhaseFlowEngine()
	TwoPhase.initialization()
	#TwoPhase.savePoreNetwork('compressNetwork')
	
	
	raw_input('Pause2')
	
	tryvtk()
	quit()
	O.resetThisScene() #####!!!!!!!	
	
	
	## Use predicate to insert new particles
	predSpheres=pack.randomDensePack(pred,spheresInCell=500,radius=1e-6)
	O.bodies.append(predSpheres)
	
	
	quit()
	
	#raw_input('Pause1.1')
	for pos,r in spheres:
		#O.bodies.append(sphere(pos,r))
		pred(b.state.pos,b.shape.radius)
		#print(pos,r)
	#raw_input('Pause1.2')
	#quit()
	
	#TwoPhase=TwoPhaseFlowEngine()
	#TwoPhase.initialization()
	
	#raw_input('Pause2')
	#quit()
	print('')
	tryvtk()
	
	raw_input('Pause3')
	#quit()

	
	
	print('Number of particles = ' , len(O.bodies))
	print('Total particles inc. periodic', len(O.bodies))


	try:
		from yade import qt
		qt.View()
	except:
		pass
	
	

	
if __name__=="__main__":
	main()
