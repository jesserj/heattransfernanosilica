__author__ = "Jesse Ross-Jones"
__license__ = "Public Domain"
__version__ = "1.0"
######################################################################
# A script for creating a loose packing of clumps
#
# the loose packing is divided.txt saved to /tmp
#
# These files contain the particle location and radii for the sample, 
# format x, y, z, r
######################################################################
def getClumpInfo():
	c = []
	for b in O.bodies:
		#print(b.isClump)
		if b.isClump:
			c.append(b) 
			#print(b.shape)
			print('Clump ',b.id,' has following members:')
			keys = b.shape.members.keys()
			for ii in range(0,len(keys)):
				print('- Body ',keys[ii])
	#print(c)
	return c
	
def setClumpInfo(disp=False):
	c = []
	aggNum = -1
	for b in O.bodies:
		#print(b.isClump)
		
		if b.isClump:
			aggNum+=1
			c.append(b) 
			#print(b.shape)
			if disp: print('Clump ',b.id,' has following members:')
			keys = b.shape.members.keys()
			for ii in range(0,len(keys)):
				if disp: print('- Body ',keys[ii])
				#O.bodies[keys[ii]].agglomerate = b.id # tell each particle who is its agglomerate
				O.bodies[keys[ii]].agglomerate = aggNum # tell each particle who is its agglomerate
	print(str(len(c))+ ' ' + str(aggNum))
	return len(c)	#number of clumps	

# if the unbalanced forces goes below .05, the packing
# is considered stabilized, therefore we stop collected
# data history and stop
def checkUnbalanced():
	from yade import pack, plot
	if unbalancedForce()<.05:
		O.pause()
		plot.saveDataTxt('bbb.txt.bz2')
		# plot.saveGnuplot('bbb') is also possible

# collect history of data which will be plotted
def addPlotData():
	from yade import pack, plot
	# each item is given a names, by which it can be the unsed in plot.plots
	# the **O.energy converts dictionary-like O.energy to plot.addData arguments
	plot.addData(i=O.iter,unbalanced=unbalancedForce(),**O.energy)
	
def gravityDeposition():
	# gravity deposition in box, showing how to plot and save history of data,
	# and how to control the simulation while it is running by calling
	# python functions from within the simulation loop

	# import yade modules that we will use below
	from yade import pack, plot
	
	dim=utils.aabbExtrema()
	dim=utils.aabbExtrema()
	xinf=dim[0][0]
	xsup=dim[1][0]
	xdim = xsup-xinf
	X=xinf+(xdim)/2.
	yinf=dim[0][1]
	ysup=dim[1][1]
	ydim = ysup-yinf
	Y=yinf+(ydim)/2.
	zinf=dim[0][2]
	zsup=dim[1][2]
	zdim = zsup-zinf
	Z=zinf+(zdim)/2.

	center = Vector3(X,Y,Z) #center of the packing
	extend = Vector3(xdim,ydim,zdim) #extends of the packing

	# create rectangular box from facets
	O.bodies.append(geom.facetBox(center,extend,wallMask=31))

	O.materials.append(FrictMat(young=1e10, poisson=0.28, frictionAngle=0.2, density=2600.))


	O.engines=[
		ForceResetter(),
		InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Facet_Aabb()]),
		#InsertionSortCollider([Bo1_Sphere_Aabb(), Bo1_Wall_Aabb(), Bo1_Facet_Aabb()],allowBiggerThanPeriod=True),
		InteractionLoop(
			# handle sphere+sphere and facet+sphere collisions
			[Ig2_Sphere_Sphere_ScGeom(),Ig2_Facet_Sphere_ScGeom()],
			#[Ig2_Sphere_Sphere_ScGeom(), Ig2_Facet_Sphere_ScGeom(), Ig2_Wall_Sphere_ScGeom()],
			[Ip2_FrictMat_FrictMat_FrictPhys()],
			[Law2_ScGeom_FrictPhys_CundallStrack()]
		),
		#NewtonIntegrator(gravity=(0,0,-9.81),damping=0.4),
		NewtonIntegrator( gravity=(0,0,-9.81),label='newtonEng'),# NO DAMPING
		# call the checkUnbalanced function (defined below) every 2 seconds
		PyRunner(command='checkUnbalanced()',realPeriod=2),
		# call the addPlotData function every 200 steps
		PyRunner(command='addPlotData()',iterPeriod=100)
	]
	
	#O.dt=.5*PWaveTimeStep()*10000.
	O.dt=10*PWaveTimeStep()
	O.bodies.updateClumpProperties(discretization=10)# correct mass, volume, inertia!!
	
	

	# enable energy tracking; any simulation parts supporting it
	# can create and update arbitrary energy types, which can be
	# accessed as O.energy['energyName'] subsequently
	O.trackEnergy=True
	
	#O.run(500,True)
	
	raw_input('test')

	# define how to plot data: 'i' (step number) on the x-axis, unbalanced force
	# on the left y-axis, all energies on the right y-axis
	# (O.energy.keys is function which will be called to get all defined energies)
	# None separates left and right y-axis
	plot.plots={'i':('unbalanced',None,O.energy.keys)}
	
	O.run(1000,True)
	
	# show the plot on the screen, and update while the simulation runs
	plot.plot()
	
def maxSphereDim(maxDim):
	dim=utils.aabbExtrema()
	dim=utils.aabbExtrema()
		
	xinf=dim[0][0]
	xsup=dim[1][0]
	xdim = xsup-xinf
	X=xinf+(xdim)/2.
	yinf=dim[0][1]
	ysup=dim[1][1]
	ydim = ysup-yinf
	Y=yinf+(ydim)/2.
	zinf=dim[0][2]
	zsup=dim[1][2]
	zdim = zsup-zinf
	Z=zinf+(zdim)/2.
	
	testMax = max([xdim,ydim,zdim])
	
	#print(['dim :' , [xdim,ydim,zdim]])
	#print(['max :' , testMax])
	#print(['max2:' , max([testMax, maxDim])])
	
	return max([testMax, maxDim])

def tryvtk():
	O.engines = [
	   ForceResetter(),
	   InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
	   #InteractionLoop(
		  #[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()], ##triax only
		#  [Ig2_Sphere_Sphere_ScGeom()],
		#  [Ip2_FrictMat_FrictMat_FrictPhys()],
		#  [Law2_ScGeom_FrictPhys_CundallStrack()]
	   #),
	   FlowEngine(dead=1,label="flow"),#introduced as a dead engine for the moment, see 2nd section
	   GlobalStiffnessTimeStepper(active=1,timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8),
	   #PeriTriaxController(dynCell=True,mass=0.2,maxUnbalanced=0.01,relStressTol=0.02,goal=(-1e6,-1e6,-1e6),stressMask=3,globUpdate=5,maxStrainRate=(1e10,1e10,1e10),doneHook='triaxDone()',label='compressor'),
	   #NewtonIntegrator(damping=.6), ##Triaxonly
	   NewtonIntegrator(damping=.2),
	]
	factor = .5
	O.dt = factor * PWaveTimeStep()
	flow.dead=0
	flow.defTolerance=0.3
	flow.meshUpdateInterval=200
	flow.useSolver=3
	flow.permeabilityFactor=1
	flow.viscosity=10
	flow.bndCondIsPressure=[0,0,1,1,0,0]
	flow.bndCondValue=[0,0,1,0,0,0]
	flow.boundaryUseMaxMin=[0,0,0,0,0,0]
	O.dt=0.1e-3
	O.dynDt=False
	flow.emulateAction()
	flow.saveVtk()
	#raw_input('FlowEngine saveVtk END')
	
	
	vtkExporter = export.VTKExporter('/tmp/vtkExporterTesting')
	vtkExporter.exportSpheres(what=[('dist','b.state.pos.norm()')])


from yade import pack,export,ymport
import random
random.seed(1) # to make colors always the same


minCorner = (0,0,0)
#maxCorner = (7e-7,7e-7,7e-7)
sizeMultiplier = 1.3
size = 5e-7*sizeMultiplier
maxCorner = (size,size,size)
maxDim = 0


#O.periodic=True
sp = pack.SpherePack()


# load clumps
ymport.textClumps('/tmp/clump0.txt')

c0 = pack.SpherePack()
c0.fromSimulation()

maxDim = maxSphereDim(maxDim)


O.resetThisScene()


ymport.textClumps('/tmp/clump1.txt')
#c1 = getClumpInfo()
c1 = pack.SpherePack()
c1.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump2.txt')
#c1 = getClumpInfo()
c2 = pack.SpherePack()
c2.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump3.txt')
#c1 = getClumpInfo()
c3 = pack.SpherePack()
c3.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump4.txt')
#c1 = getClumpInfo()
c4 = pack.SpherePack()
c4.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump5.txt')
#c1 = getClumpInfo()
c5 = pack.SpherePack()
c5.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump6.txt')
#c1 = getClumpInfo()
c6 = pack.SpherePack()
c6.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump7.txt')
#c1 = getClumpInfo()
c7 = pack.SpherePack()
c7.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump8.txt')
#c1 = getClumpInfo()
c8 = pack.SpherePack()
c8.fromSimulation()
O.resetThisScene()

ymport.textClumps('/tmp/clump9.txt')
#c1 = getClumpInfo()
c9 = pack.SpherePack()
c9.fromSimulation()
O.resetThisScene()



maxDim = maxSphereDim(maxDim)

print(['maxDim:', maxDim])

	
O.switchScene(); O.resetThisScene() #####!!!!!!!	


print('Creating Clump Cloud')

#test = sp.makeClumpCloud(minCorner, maxCorner, [c0,c1], periodic=True, num = -11)#, periodic=True, num=-1, seed=1)
test = sp.makeClumpCloud(minCorner, maxCorner, [c0,c1,c2,c3,c4,c5,c6,c7,c8,c9], periodic=True, num = -1)#, periodic=True, num=-1, seed=1)
#test = sp.makeClumpCloud(minCorner, maxCorner, [c0,c1,c2,c3,c4], periodic=True, num = -1)#, periodic=True, num=-1, seed=1)

#sp = pack.randomDensePack(pack.inAlignedBox(minCorner,maxCorner),radius=maxDim,returnSpherePack=True)

#test = sp.makeClumpCloud(minCorner, maxCorner, [c0,c1], periodic=True, num = 1000)#, periodic=True, num=-1, seed=1)

#print(['number of spheres:' , test])
print(len(O.bodies))
#raw_input('Pause')
#quit()



sp.toSimulation()
print(len(O.bodies))

#raw_input('ClumpCloud')

#TwoPhase=TwoPhaseFlowEngine()
#TwoPhase.initialization()
#raw_input('TwoPhase Done')
#tryvtk()

#quit()

#raw_input('Before gravity')
#gravityDeposition()
#raw_input('After gravity')
#quit()

setClumpInfo()
#print(enumerate(sp))



print('Porosity: ', porosity())

# save the result, including information of agglomerates which the particle belongs to
export.textExt('/tmp/divided.txt','x_y_z_r_attrs',attrs=['b.agglomerate'])

print('Number of particles = ' , len(O.bodies))


try:
	from yade import qt
	qt.View()
except:
	pass

raw_input('Generated packing, quiting')
quit()

