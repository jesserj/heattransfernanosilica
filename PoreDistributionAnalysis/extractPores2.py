# This code reads a VTI file to extract the pore distance field and analyze pore distributions 
# Created by Jesse Ross-Jones


from vtk.util.colors import deep_ochre


def main(parSub, path, filename):
    import vtk
    import numpy as np
    from vtk.numpy_interface import dataset_adapter as dsa
    import matplotlib.pyplot as plt
    #from matplotlib2tikz import save as tikz_save
    from tikzplotlib import save as tikz_save

    # import matplotlib as mpl
    plt.rcParams['axes.unicode_minus'] = False
    # mpl.rcParams['axes.unicode_minus'] = False

    # https://stackoverflow.com/questions/51043815/save-an-array-of-data-from-a-vtk-in-python

    # Read the file (to test that it was written correctly)
    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(path + filename)
    reader.Update()

    reader.PointArrayStatus = ['geometry', 'physBoundaryDistance', 'physPoreSizeDistribution']
    #reader.PointArrayStatus = ['geometry', 'physTauFromBoundaryDistance', 'physPoreSizeDistribution']


    usg = dsa.WrapDataObject(reader.GetOutput())
    physPoreSizeDistribution = usg.PointData['physPoreSizeDistribution']  # Assuming you know the name of the array
    #physPoreSizeDistribution = usg.PointData['physTauFromBoundaryDistance']  # Assuming you know the name of the array
    physBoundaryDistance = usg.PointData['physBoundaryDistance']  # Assuming you know the name of the array
    #physBoundaryDistance = usg.PointData['physTauFromBoundaryDistance']  # Assuming you know the name of the array
    positivePoresfilter = physBoundaryDistance[np.where(physPoreSizeDistribution > 0.00)[0]]
    poreRadius = physBoundaryDistance[np.where(physPoreSizeDistribution > 0.0)[0]]

    filter = np.where(physPoreSizeDistribution > 0.0)

    polyData = vtk.vtkPolyData()
    polyData = reader.GetOutput()

    # allCoordinatesX = []
    # allCoordinatesY = []
    # allCoordinatesZ = []
    # for i in range(polyData.GetNumberOfPoints()):
    # 	[x,y,z] = polyData.GetPoint(i)
    # 	allCoordinatesX.append(x)
    # 	allCoordinatesY.append(y)
    # 	allCoordinatesZ.append(z)
    # 	#allCoordinates.append(polyData.GetPoint(i))
    # coordFilterX = allCoordinatesX[np.where(physPoreSizeDistribution > 0.00)[0]]
    # coordFilterY = allCoordinatesY[np.where(physPoreSizeDistribution > 0.00)[0]]
    # coordFilterZ = allCoordinatesZ[np.where(physPoreSizeDistribution > 0.00)[0]]

    coordFilterX = []
    coordFilterY = []
    coordFilterZ = []
    for item in filter[0]:
        [x, y, z] = polyData.GetPoint(item)
        coordFilterX.append(x)
        coordFilterY.append(y)
        coordFilterZ.append(z)

    # locations = physBoundaryDistance = usg.PointData['geometry']
    # coords = usg.GetPoints()

    pores = []
    arrayX = []
    arrayY = []
    arrayZ = []

    # Create dimension projections
    for poreID in range(len(poreRadius) - parSub):  # if parSub > 0 crop end of list
        pores.append([poreID, coordFilterX[poreID], coordFilterY[poreID], coordFilterZ[poreID], poreRadius[poreID]])
        arrayX.append([poreID, coordFilterX[poreID] - poreRadius[poreID], 0])  # start
        arrayX.append([poreID, coordFilterX[poreID] + poreRadius[poreID], 1])  # end

        arrayY.append([poreID, coordFilterY[poreID] - poreRadius[poreID], 0])  # start
        arrayY.append([poreID, coordFilterY[poreID] + poreRadius[poreID], 1])  # end

        arrayZ.append([poreID, coordFilterZ[poreID] - poreRadius[poreID], 0])  # start
        arrayZ.append([poreID, coordFilterZ[poreID] + poreRadius[poreID], 1])  # end

    # Sort pores by size (smallest to largest??)
    pores.sort(reverse=False, key=lambda p: p[4])
    positivePoresOri = []
    pPoreVolOri = []

    numParticles = len(pores)

    f = open(path + 'orig.txt', 'w')
    f.write("#format x_y_z_r \n")
    for p in pores:
        # print(p)
        if p[4] > 0:
            f.write('\t'.join(map(str, p[1:5])))
            f.write('\n')
            positivePoresOri.append(p[4])
            pPoreVolOri.append(p[4] ** 3 * 3.14159 * 3. / 4.)
    f.close()

    poreIndex = []
    for pID, x, y, z, r in pores:
        poreIndex.append(pID)

    origArrayX = arrayX.copy()
    origArrayY = arrayY.copy()
    origArrayZ = arrayZ.copy()

    arrayX.sort(reverse=False, key=lambda p: p[1])
    arrayY.sort(reverse=False, key=lambda p: p[1])
    arrayZ.sort(reverse=False, key=lambda p: p[1])

    for pore in range(len(pores)):  # iterate from smallest radius to largest
        curRad = pores[pore][4]
        curLoc = pores[pore][1:4]
        curID = pores[pore][0]
        if curRad == 0:
            print('Uh Oh Main!')
            continue

        # xFindStart = origArrayX[pores[pore][0]*2]
        # xFindEnd = origArrayX[pores[pore][0]*2+1]
        startX = arrayX.index(origArrayX[pores[pore][0] * 2])
        endX = arrayX.index(origArrayX[pores[pore][0] * 2 + 1])

        startY = arrayY.index(origArrayY[pores[pore][0] * 2])
        endY = arrayY.index(origArrayY[pores[pore][0] * 2 + 1])

        startZ = arrayZ.index(origArrayZ[pores[pore][0] * 2])
        endZ = arrayZ.index(origArrayZ[pores[pore][0] * 2 + 1])

        # print('looking for pore:', pores[pore][0], 'found: ', arrayX[startX][0], arrayX[endX][0], arrayY[startY][0], arrayY[endY][0], arrayZ[startZ][0], arrayZ[endZ][0])
        ## TODO: make an error check to see if correct pore is found
        ## TODO can the index find be speed up with a lookup table? What happens when the radius changes?

        activeX = set()
        activeY = set()
        activeZ = set()
        for itemX in range(startX + 1, endX - 1):
            activeX.add(arrayX[itemX][0])  # TODO check if add without doubles is faster than removing doubles after
        for itemY in range(startY + 1, endY - 1):
            activeY.add(arrayY[itemY][0])  # TODO check if add without doubles is faster than removing doubles after
        for itemZ in range(startZ + 1, endZ - 1):
            activeZ.add(arrayZ[itemZ][0])  # TODO check if add without doubles is faster than removing doubles after
        repeats = set()
        repeats = activeX.intersection(activeY, activeZ)
        # print('Found probable intersection: ', curID, ':', repeats)

        # TODO create a break if the radius is 0?
        for rep in repeats:
            # find location and radius of overlapping pore
            dex = poreIndex.index(rep)
            poreloc = pores[dex][1:4]
            poreRad = pores[dex][4]
            poreDex = pores[dex][0]
            if poreRad != 0:
                # print('Uh Oh Sub!')

                dist = np.linalg.norm(np.array(curLoc) - np.array(poreloc))
                overlap = dist - poreRad - curRad
                if overlap < 0:  # pores intersect
                    if poreRad < curRad:
                        # print('Warning! shrinking larger sphere')
                        pores[dex][4] += overlap
                        if pores[dex][4] < 0: pores[dex][4] = 0
                    # print('OverlapFound ', curID , ':' , poreDex, ':' , overlap )
                    # print('Old radius:', pores[pore][4])
                    else:
                        pores[pore][4] += overlap
                        if pores[pore][4] < 0: pores[pore][4] = 0
                # print('New radius:', pores[pore][4])

    positivePoresRed = []
    positivePoresRedDia = []
    pPoreVolRed = []
    f = open(path + 'reduced.txt', 'w')
    f.write("#format x_y_z_r \n")
    for p in pores:
        # print(p)
        if p[4] > 0:
            f.write('\t'.join(map(str, p[1:5])))
            f.write('\n')
            positivePoresRed.append(p[4])
            positivePoresRedDia.append(p[4] * 2.)
            pPoreVolRed.append(p[4] ** 3 * 3.14159 * 3. / 4.)
    f.close()
    ## TODO remove pores when radius < 0
    ## TODO make a check to shrink the smaller of the two spheres

## Save diameter to csv file
    f = open(path + 'pores.csv', 'w')
    #f.write("#format x_y_z_r \n")
    for p in positivePoresRedDia:
        # print(p)
        #if p[4] > 0:
        f.write(str(p))
        f.write('\n')
        #    positivePoresRed.append(p[4])
        #    positivePoresRedDia.append(p[4] * 2.)
        #    pPoreVolRed.append(p[4] ** 3 * 3.14159 * 3. / 4.)
    f.close()

    #quit()

    plt.figure(2)
    plt.subplot(211)
    # plt.hist(positivePoresOri, bins='auto')  # arguments are passed to np.histogram
    plt.hist(positivePoresOri, bins=40, range=(0, 8e-8))  # arguments are passed to np.histogram
    # plt.title("Original Radii Histogram")
    plt.xlabel("radius [nm]")
    plt.ylabel("occurence [-]")
    # plt.show()

    plt.subplot(212)
    # plt.hist(positivePoresRed, bins='auto')  # arguments are passed to np.histogram
    plt.hist(positivePoresRed, bins=40, range=(0, 8e-8))  # arguments are passed to np.histogram
    # plt.title("Reduced Radii Histogram")
    plt.xlabel("radius [nm]")
    plt.ylabel("occurence [-]")
    # plt.show()
    plt.subplots_adjust(hspace=0.3,
                        bottom=0.1)  # https://matplotlib.org/api/_as_gen/matplotlib.pyplot.subplots_adjust.html

    plt.savefig(path + 'histogram.png')
    tikz_save(path + 'histogram.tex')

    # Save Diameter
    plt.figure(1)
    # plt.show()
    fig, ax1 = plt.subplots()
    hist, bins, _ = ax1.hist(positivePoresRedDia, bins=200, density=True, histtype='step',
                             range=(1e-9, 1.2e-7))  # arguments are passed to np.histogram
    logbins = np.logspace(np.log10(bins[0]), np.log10(bins[-1]), len(bins))
    # plt.xlim(1.2e-7, 0)
    plt.figure(4)
    plt.hist(positivePoresRedDia, bins=logbins)
    plt.xscale('log')
    plt.xlim(1.e-9, 1e-7)
    ax1.set_ylabel('occurence [-]')
    ax2 = ax1.twinx()
    ax2 = plt.hist(positivePoresRedDia, bins=200, density=True, histtype='step', cumulative=-1, label='Cumulative',
                   color='tab:red')
    # plt.title("Reduced Radii Histogram")
    plt.xlabel("diameter [nm]")
    # ax1.ylabel("occurence [-]")
    # ax2.set_ylabel('exp', color='tab:red')
    # plt.show()
    plt.subplots_adjust(hspace=0.3,
                        bottom=0.1)  # https://matplotlib.org/api/_as_gen/matplotlib.pyplot.subplots_adjust.html

    plt.savefig(path + 'histogramDia.png')
    tikz_save(path + 'histogramDia.tex')

    # return numParticles
    # Visualize Volume distribution

    ## Plot Volume Histogram

    plt.figure(3)
    plt.subplot(211)
    # plt.hist(pPoreVolOri, bins='auto')  # arguments are passed to np.histogram
    plt.hist(pPoreVolOri, bins=40, range=(0, 1.25e-21))  # arguments are passed to np.histogram
    # plt.title("Original Radii Histogram")
    plt.xlabel("volume [$nm^3$]")
    plt.ylabel("occurence [-]")
    plt.yscale('log')
    # plt.show()

    plt.subplot(212)
    # plt.hist(pPoreVolRed, bins='auto')  # arguments are passed to np.histogram
    plt.hist(pPoreVolRed, bins=40, range=(0, 1.25e-21))  # arguments are passed to np.histogram
    # plt.title("Reduced Radii Histogram")
    plt.xlabel("volume [$nm^3$]")
    plt.ylabel("occurence [-]")
    plt.yscale('log')
    # plt.show()
    plt.subplots_adjust(hspace=0.3,
                        bottom=0.1)  # https://matplotlib.org/api/_as_gen/matplotlib.pyplot.subplots_adjust.html

    plt.savefig(path + 'histogramVol.png')
    tikz_save(path + 'histogramVol.tex')

    return numParticles


# print(array1)


## structures
## [sphereID, location, radius]


# def shrinkSpheres():
## Sort pores by radii [arrayR]
# Sort spheres along three axis by AABB [arrayX] (sort and prune, sweep and prune)
# for pore in arrayR,
# 	find start and end in arrayX, Y , Z
# 	for entry in arrayX[startX:endX]: add entry to [activeInterX]
#	for entry in arrayY[startY:endY]: add entry to [activeInterY]
#	for entry in arrayZ[startZ:endZ]: add entry to [activeInterZ]
# 		##for each axis X,Y,Z for intersections (create array active intersections [activeInter] )
#		## remove doubles, make sure current sp is not included
# 	find [repeats] in activeInterX, activeInterY, activeInterZ
# 	For sphere in [repeats]:
# |		overlap =  distance(sp,sphere) – sp.radius - sphere.radius
#   	if overlap <= 0:
#       	sp.radius = sp.radius - |overlap|
#           update interval in arrayX, arrayY, arrayZ
#			resort arrayX, arrayY, arrayZ

# Create a priority list of all action points (action points are the starting or ending point of a line).
# And each item of the PQ has 3 elements (Current Point, Start or End, Comes from what line). (O(n log n) operation if we use Quick Short for sorting).

def mainIT():
    import vtk
    import numpy as np
    from vtk.numpy_interface import dataset_adapter as dsa
    import matplotlib.pyplot as plt
    from intervaltree import Interval, IntervalTree

    # https://stackoverflow.com/questions/51043815/save-an-array-of-data-from-a-vtk-in-python

    filename = 'spherePacking_iT0000000iC00000.vti'

    # Read the file (to test that it was written correctly)
    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(filename)
    reader.Update()

    reader.PointArrayStatus = ['geometry', 'physBoundaryDistance', 'physPoreSizeDistribution']

    usg = dsa.WrapDataObject(reader.GetOutput())
    physPoreSizeDistribution = usg.PointData['physPoreSizeDistribution']  # Assuming you know the name of the array
    physBoundaryDistance = usg.PointData['physBoundaryDistance']  # Assuming you know the name of the array
    positivePoresfilter = physBoundaryDistance[np.where(physPoreSizeDistribution > 0.00)[0]]
    poreRadius = physBoundaryDistance[np.where(physPoreSizeDistribution > 0.0)[0]]

    filter = np.where(physPoreSizeDistribution > 0.0)

    polyData = vtk.vtkPolyData()
    polyData = reader.GetOutput()

    # allCoordinatesX = []
    # allCoordinatesY = []
    # allCoordinatesZ = []
    # for i in range(polyData.GetNumberOfPoints()):
    # 	[x,y,z] = polyData.GetPoint(i)
    # 	allCoordinatesX.append(x)
    # 	allCoordinatesY.append(y)
    # 	allCoordinatesZ.append(z)
    # 	#allCoordinates.append(polyData.GetPoint(i))
    # coordFilterX = allCoordinatesX[np.where(physPoreSizeDistribution > 0.00)[0]]
    # coordFilterY = allCoordinatesY[np.where(physPoreSizeDistribution > 0.00)[0]]
    # coordFilterZ = allCoordinatesZ[np.where(physPoreSizeDistribution > 0.00)[0]]

    coordFilterX = []
    coordFilterY = []
    coordFilterZ = []
    for item in filter[0]:
        [x, y, z] = polyData.GetPoint(item)
        coordFilterX.append(x)
        coordFilterY.append(y)
        coordFilterZ.append(z)

    # locations = physBoundaryDistance = usg.PointData['geometry']
    # coords = usg.GetPoints()

    pores = []
    # arrayX = []
    # arrayY = []
    # arrayZ = []
    # https://pypi.org/project/intervaltree/
    interX = IntervalTree()
    interZ = IntervalTree()
    interY = IntervalTree()
    # Create projections to each dimension
    for poreID in range(len(poreRadius)):
        pores.append(
            [poreID, coordFilterX[poreID], coordFilterY[poreID], coordFilterZ[poreID], poreRadius[poreID], set()])
        # arrayX.append([poreID, coordFilterX[poreID] - poreRadius[poreID], 0]) # start
        # arrayX.append([poreID, coordFilterX[poreID] + poreRadius[poreID], 1]) # end
        interX.addi(coordFilterX[poreID] - poreRadius[poreID], coordFilterX[poreID] + poreRadius[poreID], [poreID])

        # arrayY.append([poreID, coordFilterY[poreID] - poreRadius[poreID], 0])  # start
        # arrayY.append([poreID, coordFilterY[poreID] + poreRadius[poreID], 1])  # end
        interY.addi(coordFilterY[poreID] - poreRadius[poreID], coordFilterY[poreID] + poreRadius[poreID], [poreID])

        # arrayZ.append([poreID, coordFilterZ[poreID] - poreRadius[poreID], 0])  # start
        # arrayZ.append([poreID, coordFilterZ[poreID] + poreRadius[poreID], 1])  # end
        interZ.addi(coordFilterZ[poreID] - poreRadius[poreID], coordFilterZ[poreID] + poreRadius[poreID], [poreID])

    pores.sort(reverse=False, key=lambda p: p[4])  # sort by radius
    positivePoresOri = []

    poreIndex = []
    for pID, x, y, z, r, interval in pores:
        poreIndex.append(pID)

    f = open('origIT.txt', 'w')
    f.write("#format x_y_z_r \n")
    for p in pores:
        # print(p)
        if p[4] > 0:
            f.write('\t'.join(map(str, p[1:5])))
            f.write('\n')
            positivePoresOri.append(p[4])
    f.close()

    # poreIndex = []
    # for pID, x,y,z, r, intersect in pores: #extract a list of poreIDs after sort
    for p in range(len(pores)):  # extract a list of poreIDs after sort
        xSet = set()
        ySet = set()
        zSet = set()
        for xInt in interX.search(pores[p][1]):
            xSet.add(xInt[2][0])
        for yInt in interY.search(pores[p][2]):
            ySet.add(yInt[2][0])
        for zInt in interY.search(pores[p][3]):
            zSet.add(zInt[2][0])
        pores[p][5] = xSet.intersection(ySet, zSet)
    # print(pores[p][5])
    # print(intersect)
    # print('Found probable intersection: ', pID, ':', intersect)

    # poreIndex.append(pID)

    # origArrayX = arrayX.copy()
    # origArrayY = arrayY.copy()
    # origArrayZ = arrayZ.copy()

    # arrayX.sort(reverse=False, key=lambda p: p[1]) #sort each axis
    # arrayY.sort(reverse=False, key=lambda p: p[1])
    # arrayZ.sort(reverse=False, key=lambda p: p[1])

    for pore in range(len(pores)):  # iterate from smallest radius to largest
        curRad = pores[pore][4]
        curLoc = pores[pore][1:4]
        curID = pores[pore][0]
        repeats = pores[pore][5]

        for rep in repeats:
            # find location and radius of overlapping pore
            dex = poreIndex.index(rep)
            poreloc = pores[dex][1:4]
            poreRad = pores[dex][4]
            poreDex = pores[dex][0]
            dist = np.linalg.norm(np.array(curLoc) - np.array(poreloc))
            overlap = dist - poreRad - curRad
            if overlap < 0:
                if poreRad < curRad:
                    # print('Warning! shrinking larger sphere')
                    pores[dex][4] += overlap
                    if pores[dex][4] < 0: pores[dex][4] = 0
                # print('OverlapFound ', curID , ':' , poreDex, ':' , overlap )
                # print('Old radius:', pores[pore][4])
                else:
                    pores[pore][4] += overlap
                    if pores[pore][4] < 0: pores[pore][4] = 0
    # print('New radius:', pores[pore][4])

    positivePoresRed = []
    f = open('reducedIT.txt', 'w')
    f.write("#format x_y_z_r \n")
    for p in pores:
        # print(p)
        if p[4] > 0:
            f.write('\t'.join(map(str, p[1:5])))
            f.write('\n')
            positivePoresRed.append(p[4])
    f.close()

    # xFindStart = origArrayX[pores[pore][0]*2]
    # xFindEnd = origArrayX[pores[pore][0]*2+1]
    #	startX = arrayX.index(origArrayX[pores[pore][0] * 2])
    #	endX   = arrayX.index(origArrayX[pores[pore][0] * 2 + 1])

    #	startY = arrayY.index(origArrayY[pores[pore][0] * 2])
    #	endY   = arrayY.index(origArrayY[pores[pore][0] * 2 + 1])

    #	startZ = arrayZ.index(origArrayZ[pores[pore][0] * 2])
    #	endZ   = arrayZ.index(origArrayZ[pores[pore][0] * 2 + 1])

    # print('looking for pore:', pores[pore][0], 'found: ', arrayX[startX][0], arrayX[endX][0], arrayY[startY][0], arrayY[endY][0], arrayZ[startZ][0], arrayZ[endZ][0])
    ## TODO: make an error check to see if correct pore is found
    ## TODO can the index find be speed up with a lookup table? What happens when the radius changes?

    # activeX = set()
    # activeY = set()
    # activeZ = set()
    #	for itemX in range(startX+1,endX-1):
    #		activeX.add(arrayX[itemX][0]) # TODO check if add without doubles is faster than removing doubles after
    #	for itemY in range(startY+1,endY-1):
    #		activeY.add(arrayY[itemY][0]) # TODO check if add without doubles is faster than removing doubles after
    #	for itemZ in range(startZ+1,endZ-1):
    #		activeZ.add(arrayZ[itemZ][0]) # TODO check if add without doubles is faster than removing doubles after
    #	repeats = set()
    #	repeats = activeX.intersection(activeY,activeZ)
    #	print('Found probable intersection: ',curID,':', repeats)

    ## TODO remove pores when radius < 0
    ## TODO make a check to shrink the smaller of the two spheres

    plt.hist(positivePores, bins='auto')  # arguments are passed to np.histogram
    plt.title("Histogram with 'auto' bins")
    plt.show()

    plt.hist(positivePoresfilter, bins='auto')  # arguments are passed to np.histogram
    plt.title("Histogram with 'auto' bins")
    plt.show()


# print(array1)

def mainBF(parSub, path, filename):
    import vtk
    import numpy as np
    from vtk.numpy_interface import dataset_adapter as dsa
    import matplotlib.pyplot as plt

    # https://stackoverflow.com/questions/51043815/save-an-array-of-data-from-a-vtk-in-python

    # filename = 'spherePacking_iT0000000iC00000.vti'

    # Read the file (to test that it was written correctly)
    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(path + filename)
    reader.Update()

    reader.PointArrayStatus = ['geometry', 'physBoundaryDistance', 'physPoreSizeDistribution']

    usg = dsa.WrapDataObject(reader.GetOutput())
    physPoreSizeDistribution = usg.PointData['physPoreSizeDistribution']  # Assuming you know the name of the array
    physBoundaryDistance = usg.PointData['physBoundaryDistance']  # Assuming you know the name of the array
    positivePoresfilter = physBoundaryDistance[np.where(physPoreSizeDistribution > 0.00)[0]]
    poreRadius = physBoundaryDistance[np.where(physPoreSizeDistribution > 0.0)[0]]

    filter = np.where(physPoreSizeDistribution > 0.0)

    polyData = vtk.vtkPolyData()
    polyData = reader.GetOutput()

    # allCoordinatesX = []
    # allCoordinatesY = []
    # allCoordinatesZ = []
    # for i in range(polyData.GetNumberOfPoints()):
    # 	[x,y,z] = polyData.GetPoint(i)
    # 	allCoordinatesX.append(x)
    # 	allCoordinatesY.append(y)
    # 	allCoordinatesZ.append(z)
    # 	#allCoordinates.append(polyData.GetPoint(i))
    # coordFilterX = allCoordinatesX[np.where(physPoreSizeDistribution > 0.00)[0]]
    # coordFilterY = allCoordinatesY[np.where(physPoreSizeDistribution > 0.00)[0]]
    # coordFilterZ = allCoordinatesZ[np.where(physPoreSizeDistribution > 0.00)[0]]

    coordFilterX = []
    coordFilterY = []
    coordFilterZ = []
    for item in filter[0]:
        [x, y, z] = polyData.GetPoint(item)
        coordFilterX.append(x)
        coordFilterY.append(y)
        coordFilterZ.append(z)

    # locations = physBoundaryDistance = usg.PointData['geometry']
    # coords = usg.GetPoints()

    pores = []
    arrayX = []
    arrayY = []
    arrayZ = []

    # Create dimension projections
    for poreID in range(len(poreRadius) - parSub):
        pores.append([poreID, coordFilterX[poreID], coordFilterY[poreID], coordFilterZ[poreID], poreRadius[poreID]])
    # 	arrayX.append([poreID, coordFilterX[poreID] - poreRadius[poreID], 0])  # start
    # 	arrayX.append([poreID, coordFilterX[poreID] + poreRadius[poreID], 1])  # end
    #
    # 	arrayY.append([poreID, coordFilterY[poreID] - poreRadius[poreID], 0])  # start
    # 	arrayY.append([poreID, coordFilterY[poreID] + poreRadius[poreID], 1])  # end
    #
    # 	arrayZ.append([poreID, coordFilterZ[poreID] - poreRadius[poreID], 0])  # start
    # 	arrayZ.append([poreID, coordFilterZ[poreID] + poreRadius[poreID], 1])  # end

    numParticles = len(pores)

    pores.sort(reverse=False, key=lambda p: p[4])
    positivePoresOri = []

    f = open('origBF.txt', 'w')
    f.write("#format x_y_z_r \n")
    for p in pores:
        # print(p)
        if p[4] > 0:
            f.write('\t'.join(map(str, p[1:5])))
            f.write('\n')
            positivePoresOri.append(p[4])
    f.close()

    poreIndex = []
    for pID, x, y, z, r in pores:
        poreIndex.append(pID)

    # origArrayX = arrayX.copy()
    # origArrayY = arrayY.copy()
    # origArrayZ = arrayZ.copy()
    #
    # arrayX.sort(reverse=False, key=lambda p: p[1])
    # arrayY.sort(reverse=False, key=lambda p: p[1])
    # arrayZ.sort(reverse=False, key=lambda p: p[1])

    for pore in range(len(pores) - 1):  # iterate from smallest radius to largest
        curRad = pores[pore][4]
        curLoc = pores[pore][1:4]
        curID = pores[pore][0]

        # xFindStart = origArrayX[pores[pore][0]*2]
        # xFindEnd = origArrayX[pores[pore][0]*2+1]
        # startX = arrayX.index(origArrayX[pores[pore][0] * 2])
        # endX = arrayX.index(origArrayX[pores[pore][0] * 2 + 1])
        #
        # startY = arrayY.index(origArrayY[pores[pore][0] * 2])
        # endY = arrayY.index(origArrayY[pores[pore][0] * 2 + 1])
        #
        # startZ = arrayZ.index(origArrayZ[pores[pore][0] * 2])
        # endZ = arrayZ.index(origArrayZ[pores[pore][0] * 2 + 1])
        #
        # # print('looking for pore:', pores[pore][0], 'found: ', arrayX[startX][0], arrayX[endX][0], arrayY[startY][0], arrayY[endY][0], arrayZ[startZ][0], arrayZ[endZ][0])
        # ## TODO: make an error check to see if correct pore is found
        # ## TODO can the index find be speed up with a lookup table? What happens when the radius changes?
        #
        # activeX = set()
        # activeY = set()
        # activeZ = set()
        # for itemX in range(startX + 1, endX - 1):
        # 	activeX.add(arrayX[itemX][0])  # TODO check if add without doubles is faster than removing doubles after
        # for itemY in range(startY + 1, endY - 1):
        # 	activeY.add(arrayY[itemY][0])  # TODO check if add without doubles is faster than removing doubles after
        # for itemZ in range(startZ + 1, endZ - 1):
        # 	activeZ.add(arrayZ[itemZ][0])  # TODO check if add without doubles is faster than removing doubles after
        # repeats = set()
        # repeats = activeX.intersection(activeY, activeZ)
        # print('Found probable intersection: ', curID, ':', repeats)

        for p2 in range(pore + 1, len(pores)):
            # find location and radius of overlapping pore
            dex = p2
            poreloc = pores[dex][1:4]
            poreRad = pores[dex][4]
            poreDex = pores[dex][0]
            dist = np.linalg.norm(np.array(curLoc) - np.array(poreloc))
            overlap = dist - poreRad - curRad
            if overlap < 0:
                if poreRad < curRad:
                    # print('Warning! shrinking larger sphere')
                    pores[dex][4] += overlap
                    if pores[dex][4] < 0: pores[dex][4] = 0
                # print('OverlapFound ', curID , ':' , poreDex, ':' , overlap )
                # print('Old radius:', pores[pore][4])
                else:
                    pores[pore][4] += overlap
                    if pores[pore][4] < 0: pores[pore][4] = 0
    # print('New radius:', pores[pore][4])

    positivePoresRed = []
    f = open(path + 'reducedBF.txt', 'w')
    f.write("#format x_y_z_r \n")
    for p in pores:
        # print(p)
        if p[4] > 0:
            f.write('\t'.join(map(str, p[1:5])))
            f.write('\n')
            positivePoresRed.append(p[4])
    f.close()
    ## TODO remove pores when radius < 0
    ## TODO make a check to shrink the smaller of the two spheres

    return numParticles

    plt.hist(positivePoresOri, bins='auto')  # arguments are passed to np.histogram
    plt.title("Original Radii Histogram with 'auto' bins")
    plt.show()

    plt.hist(positivePoresRed, bins='auto')  # arguments are passed to np.histogram
    plt.title("Reduced Radii Histogram with 'auto' bins")
    plt.show()

    return numParticles


def main1():
    # from vmtk import vtk
    import vtk

    # filename = "spherePacking_iT0000000iC00000.vti"

    # Read the file (to test that it was written correctly)
    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(filename)
    reader.Update()

    reader.PointArrayStatus = ['geometry', 'physBoundaryDistance', 'physPoreSizeDistribution']

    pa = vtk.vtkPassArrays()
    pa.SetInputConnection(reader.GetOutputPort())
    pa.AddArray(1, 'physPoreSizeDistribution')  # 0 for PointData, 1 for CellData, 2 for FieldData
    writer = vtk.vtkDataSetWriter()
    writer.SetFileName('test.vtk')
    writer.SetInputConnection(pa.GetOutputPort())
    writer.Update()
    writer.Write()

    # return
    # Convert the image to a polydata
    imageDataGeometryFilter = vtk.vtkImageDataGeometryFilter()
    imageDataGeometryFilter.SetInputConnection(reader.GetOutputPort())
    imageDataGeometryFilter.Update()

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(imageDataGeometryFilter.GetOutputPort())

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetPointSize(3)

    # Setup rendering
    renderer = vtk.vtkRenderer()
    renderer.AddActor(actor)
    renderer.SetBackground(1, 1, 1)
    renderer.ResetCamera()

    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    renderWindowInteractor = vtk.vtkRenderWindowInteractor()

    renderWindowInteractor.SetRenderWindow(renderWindow)
    renderWindowInteractor.Initialize()
    renderWindowInteractor.Start()

    print('END PROGRAM')

    return


def paraview():
    #### import the simple module from the paraview
    # from paraview.simple import *
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # create a new 'XML Image Data Reader'
    spherePacking_iT0000000iC00000vti = XMLImageDataReader(FileName=['data\\spherePacking_iT0000000iC00000.vti'])
    spherePacking_iT0000000iC00000vti.PointArrayStatus = ['geometry', 'physBoundaryDistance',
                                                          'physPoreSizeDistribution']

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')
    # uncomment following to set a specific view size
    # renderView1.ViewSize = [1532, 804]

    # show data in view
    spherePacking_iT0000000iC00000vtiDisplay = Show(spherePacking_iT0000000iC00000vti, renderView1)
    # trace defaults for the display properties.
    spherePacking_iT0000000iC00000vtiDisplay.Representation = 'Outline'
    spherePacking_iT0000000iC00000vtiDisplay.ColorArrayName = [None, '']
    spherePacking_iT0000000iC00000vtiDisplay.OSPRayScaleArray = 'geometry'
    spherePacking_iT0000000iC00000vtiDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
    spherePacking_iT0000000iC00000vtiDisplay.SelectOrientationVectors = 'None'
    spherePacking_iT0000000iC00000vtiDisplay.ScaleFactor = 0.36514306
    spherePacking_iT0000000iC00000vtiDisplay.SelectScaleArray = 'None'
    spherePacking_iT0000000iC00000vtiDisplay.GlyphType = 'Arrow'
    spherePacking_iT0000000iC00000vtiDisplay.GlyphTableIndexArray = 'None'
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid = 'GridAxesRepresentation'
    spherePacking_iT0000000iC00000vtiDisplay.PolarAxes = 'PolarAxesRepresentation'
    spherePacking_iT0000000iC00000vtiDisplay.ScalarOpacityUnitDistance = 0.04468526370945028
    spherePacking_iT0000000iC00000vtiDisplay.Slice = 62

    # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid.XTitleColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid.YTitleColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid.ZTitleColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid.XLabelColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid.YLabelColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.DataAxesGrid.ZLabelColor = [0.3333333333333333, 0.0, 1.0]

    # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    spherePacking_iT0000000iC00000vtiDisplay.PolarAxes.PolarAxisTitleColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.PolarAxes.PolarAxisLabelColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.PolarAxes.LastRadialAxisTextColor = [0.3333333333333333, 0.0, 1.0]
    spherePacking_iT0000000iC00000vtiDisplay.PolarAxes.SecondaryRadialAxesTextColor = [0.3333333333333333, 0.0, 1.0]

    # reset view to fit data
    renderView1.ResetCamera()

    # update the view to ensure updated data information
    renderView1.Update()

    # create a new 'Glyph'
    glyph1 = Glyph(Input=spherePacking_iT0000000iC00000vti,
                   GlyphType='Arrow')
    glyph1.Scalars = ['POINTS', 'None']
    glyph1.Vectors = ['POINTS', 'None']
    glyph1.ScaleFactor = 0.36514306
    glyph1.GlyphTransform = 'Transform2'

    # Properties modified on glyph1
    glyph1.GlyphType = 'Sphere'
    glyph1.Scalars = ['POINTS', 'physPoreSizeDistribution']

    # get color transfer function/color map for 'GlyphScale'
    glyphScaleLUT = GetColorTransferFunction('GlyphScale')

    # show data in view
    glyph1Display = Show(glyph1, renderView1)
    # trace defaults for the display properties.
    glyph1Display.Representation = 'Surface'
    glyph1Display.ColorArrayName = ['POINTS', 'GlyphScale']
    glyph1Display.LookupTable = glyphScaleLUT
    glyph1Display.OSPRayScaleArray = 'GlyphScale'
    glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
    glyph1Display.SelectOrientationVectors = 'GlyphScale'
    glyph1Display.ScaleFactor = 0.4007418751716614
    glyph1Display.SelectScaleArray = 'GlyphScale'
    glyph1Display.GlyphType = 'Arrow'
    glyph1Display.GlyphTableIndexArray = 'GlyphScale'
    glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
    glyph1Display.PolarAxes = 'PolarAxesRepresentation'
    glyph1Display.GaussianRadius = 0.2003709375858307
    glyph1Display.SetScaleArray = ['POINTS', 'GlyphScale']
    glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
    glyph1Display.OpacityArray = ['POINTS', 'GlyphScale']
    glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'

    # init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
    glyph1Display.DataAxesGrid.XTitleColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.DataAxesGrid.YTitleColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.DataAxesGrid.ZTitleColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.DataAxesGrid.XLabelColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.DataAxesGrid.YLabelColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.DataAxesGrid.ZLabelColor = [0.3333333333333333, 0.0, 1.0]

    # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    glyph1Display.PolarAxes.PolarAxisTitleColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.PolarAxes.PolarAxisLabelColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.PolarAxes.LastRadialAxisTextColor = [0.3333333333333333, 0.0, 1.0]
    glyph1Display.PolarAxes.SecondaryRadialAxesTextColor = [0.3333333333333333, 0.0, 1.0]

    # show color bar/color legend
    glyph1Display.SetScalarBarVisibility(renderView1, True)

    # update the view to ensure updated data information
    renderView1.Update()

    # set active source
    SetActiveSource(spherePacking_iT0000000iC00000vti)

    # create a new 'Extract Selection'
    extractSelection1 = ExtractSelection(Input=spherePacking_iT0000000iC00000vti,
                                         Selection=None)

    # set active source
    SetActiveSource(glyph1)

    # Properties modified on glyph1
    glyph1.Input = extractSelection1

    # set active source
    SetActiveSource(glyph1)


def mainTime():
    filename = 'BigO.txt'

    # f.write("#format x_y_z_r \n")
    for p in range(0, 800, 10):
        # print(p)

        print(p)

        start = time.time()
        numParticles = main(p)
        end = time.time()
        timeMain = end - start

        f = open(filename, 'a')
        # f.write('\t'.join(map(str, timeMain)))
        f.write('\t'.join(map(str, ['Time Main', timeMain, 'num', numParticles])))
        # f.write('\n')
        f.close()

        start = time.time()
        numParticles = mainBF(p)
        end = time.time()
        timeBF = end - start
        f = open(filename, 'a')
        # f.write('\t'.join(map(str, timeMain)))
        f.write('\t'.join(map(str, ['\tTime MainBF', timeBF, 'num', numParticles])))
        f.write('\n')
        f.close()


if __name__ == "__main__":
    import time

    path = '.\\'
    filename = 'spherePacking_iT0000000iC00000.vti'

    start = time.time()
    # mainTime()
    main(0, path, filename)
    end = time.time()
    print(end - start)

    start = time.time()
    # mainBF(0, path, filename)
    end = time.time()
    print(end - start)

