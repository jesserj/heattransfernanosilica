Run extractPores2.py to run the pore overlap reduction algorithm
Obtain spherePacking_iT0000000iC00000.vti from OpenLB app conjugateHeat3d
This generates:
A list of original pore locations and radii: 			orig.txt
A list of reduced pore locations and radii: 			reduced.txt
A sorted list of reduced pore radii:				pores.csv
A histogram comparing pore radii before and after reduction:	histogram.png and histogram.tex
A histogram comparing pore volumes before and after reduction:	histogram.png and histogram.tex


Run plotDist.py to plot the cumulative density distributions 
obtain pores.csv for various samples from extractPores2.py
This generates:
A plot of the CFD for the samples:				tikzPoreDist.png and tikzPoreDist.tex

