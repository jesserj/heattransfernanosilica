/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2019 Sam Avis
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/* channelFlow3d.cpp
 * In this example a Janus droplet passes through a pressure-driven
 * channel and past the outlet. This demonstrates using three fluid
 * components with the free energy model, as well as wall boundaries
 * and density inlet / outlet boundary conditions.
 */

#include "olb3D.h"
#include "olb3D.hh"   // use only generic version!
#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace std;

typedef double T;
#define DESCRIPTOR FreeEnergyD3Q19Descriptor
std::string gnuplotFilename = "gnuplot.dat";

// Parameters for the simulation setup
const int N  = 60;
const T nx = 150.;
const T channelRadius = 30.;
const T dropletX = 60.;
const T dropletRadius = 20.;
const T inletDensity = 1.0003; // [lattice units]
const T outletDensity = 1.;    // [lattice units]
const T gama = 1.;             // [lattice units]
const T alpha = 1.5;           // [lattice units]
const T kappa1 = 0.0132;       // [lattice units]
const T kappa2 = 0.0012;       // [lattice units]
const T kappa3 = 0.0032;       // [lattice units]
const T h1 = 0.;               // [lattice units]
const T h2 = 0.;               // [lattice units]
const T h3 = 0.;               // [lattice units]

const int maxIter  = 200000;
const int vtkIter  = 500;
const int statIter = 1000;


void prepareGeometry( SuperGeometry3D<T>& superGeometry ) {
  OstreamManager clout( std::cout,"prepareGeometry" );
  clout << "Prepare Geometry ..." << std::endl;

  T deltaX = channelRadius * 2. / N;
  T c0[3] = {0., channelRadius, channelRadius};
  T c1[3] = {nx, channelRadius, channelRadius};
  IndicatorCylinder3D<T> channel(c0, c1, channelRadius);

  superGeometry.rename( 0,2, channel );
  superGeometry.rename( 2,1,1,1,1 );

  // Inlet material number
  Vector<T,3> origin( -deltaX, 0., 0. );
  Vector<T,3> extend( 2.*deltaX, 2.*channelRadius, 2.*channelRadius );
  IndicatorCuboid3D<T> inlet( extend, origin );
  superGeometry.rename( 2,3,1,inlet );
  
  // Outlet material number
  origin[0] = nx - deltaX;
  IndicatorCuboid3D<T> outlet( extend, origin );
  superGeometry.rename( 2,4,1,outlet );

  superGeometry.clean();
  superGeometry.innerClean();
  superGeometry.checkForErrors();
  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;
}


void prepareLattice( SuperLattice3D<T, DESCRIPTOR>& sLattice1,
                     SuperLattice3D<T, DESCRIPTOR>& sLattice2,
                     SuperLattice3D<T, DESCRIPTOR>& sLattice3,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics1,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics2,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics3,
                     sOnLatticeBoundaryCondition3D<T,DESCRIPTOR>& sOnBC1,
                     sOnLatticeBoundaryCondition3D<T,DESCRIPTOR>& sOnBC2,
                     sOnLatticeBoundaryCondition3D<T,DESCRIPTOR>& sOnBC3,
                     UnitConverter<T, DESCRIPTOR>& converter,
                     SuperGeometry3D<T>& superGeometry ) {

  OstreamManager clout( std::cout,"prepareLattice" );
  clout << "Prepare Lattice ..." << std::endl;
 
  // define lattice dynamics
  sLattice1.defineDynamics( superGeometry, 0, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice2.defineDynamics( superGeometry, 0, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice3.defineDynamics( superGeometry, 0, &instances::getNoDynamics<T, DESCRIPTOR>() );

  sLattice1.defineDynamics( superGeometry, 1, &bulkDynamics1 );
  sLattice2.defineDynamics( superGeometry, 1, &bulkDynamics2 );
  sLattice3.defineDynamics( superGeometry, 1, &bulkDynamics3 );

  sLattice1.defineDynamics( superGeometry, 2, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice2.defineDynamics( superGeometry, 2, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice3.defineDynamics( superGeometry, 2, &instances::getNoDynamics<T, DESCRIPTOR>() );

  sLattice1.defineDynamics( superGeometry, 3, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice2.defineDynamics( superGeometry, 3, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice3.defineDynamics( superGeometry, 3, &instances::getNoDynamics<T, DESCRIPTOR>() );

  sLattice1.defineDynamics( superGeometry, 4, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice2.defineDynamics( superGeometry, 4, &instances::getNoDynamics<T, DESCRIPTOR>() );
  sLattice3.defineDynamics( superGeometry, 4, &instances::getNoDynamics<T, DESCRIPTOR>() );

  // add wall boundary conditions
  sOnBC1.addFreeEnergyWallBoundary( superGeometry, 2, alpha, kappa1, kappa2, kappa3, h1, h2, h3, 1 );
  sOnBC2.addFreeEnergyWallBoundary( superGeometry, 2, alpha, kappa1, kappa2, kappa3, h1, h2, h3, 2 );
  sOnBC3.addFreeEnergyWallBoundary( superGeometry, 2, alpha, kappa1, kappa2, kappa3, h1, h2, h3, 3 );

  // add inlet boundary conditions
  T omega = converter.getLatticeRelaxationFrequency();
  auto inletIndicator = superGeometry.getMaterialIndicator(3);
  sOnBC1.addFreeEnergyInletBoundary( inletIndicator, omega, "density", 1 );
  sOnBC2.addFreeEnergyInletBoundary( inletIndicator, omega, "density", 2 );
  sOnBC3.addFreeEnergyInletBoundary( inletIndicator, omega, "density", 3 );

  // add outlet boundary conditions
  auto outletIndicator = superGeometry.getMaterialIndicator(4);
  sOnBC1.addFreeEnergyOutletBoundary( outletIndicator, omega, "density", 1 );
  sOnBC2.addFreeEnergyOutletBoundary( outletIndicator, omega, "density", 2 );
  sOnBC3.addFreeEnergyOutletBoundary( outletIndicator, omega, "density", 3 );

  // bulk initial conditions
  std::vector<T> v( 3,T() );
  AnalyticalConst3D<T,T> zeroVelocity( v );

  T center[3] = {dropletX, channelRadius, channelRadius};
  T centerL[3] = {dropletX-dropletRadius*2., channelRadius, channelRadius};
  T centerR[3] = {dropletX+dropletRadius*2., channelRadius, channelRadius};
  SmoothIndicatorSphere3D<T,T> sphere( center, dropletRadius, 10.*alpha );
  SmoothIndicatorCylinder3D<T,T> left ( center, centerL, channelRadius, 0. );
  SmoothIndicatorCylinder3D<T,T> right( center, centerR, channelRadius, 0. );
  AnalyticalConst3D<T,T> one ( 1. );

  AnalyticalIdentity3D<T,T> c1Bulk( sphere*left );
  AnalyticalIdentity3D<T,T> c2Bulk( sphere*right );
  AnalyticalIdentity3D<T,T> rhoBulk( one );
  AnalyticalIdentity3D<T,T> phiBulk( c1Bulk - c2Bulk );
  AnalyticalIdentity3D<T,T> psiBulk( rhoBulk - c1Bulk - c2Bulk );
  
  auto allIndicator = superGeometry.getMaterialIndicator({1, 2, 3, 4});
  sLattice1.iniEquilibrium( allIndicator, rhoBulk, zeroVelocity );
  sLattice2.iniEquilibrium( allIndicator, phiBulk, zeroVelocity );
  sLattice3.iniEquilibrium( allIndicator, psiBulk, zeroVelocity );

  // inlet boundary conditions
  AnalyticalConst3D<T,T> rhoInlet( inletDensity );
  AnalyticalIdentity3D<T,T> phiInlet( c1Bulk - c2Bulk );
  AnalyticalIdentity3D<T,T> psiInlet( rhoInlet - c1Bulk - c2Bulk );
  sLattice1.defineRho( inletIndicator, rhoInlet );
  sLattice2.defineRho( inletIndicator, phiInlet );
  sLattice3.defineRho( inletIndicator, psiInlet );

  // outlet initial / boundary conditions
  AnalyticalConst3D<T,T> rhoOutlet( outletDensity );
  AnalyticalIdentity3D<T,T> phiOutlet( c1Bulk - c2Bulk );
  AnalyticalIdentity3D<T,T> psiOutlet( rhoOutlet - c1Bulk - c2Bulk );
  sLattice1.defineRho( outletIndicator, rhoOutlet );
  sLattice2.defineRho( outletIndicator, phiOutlet );
  sLattice3.defineRho( outletIndicator, psiOutlet );

  // initialise lattices
  sLattice1.initialize();
  sLattice2.initialize();
  sLattice3.initialize();

  sLattice1.communicate();
  sLattice2.communicate();
  sLattice3.communicate();

  clout << "Prepare Lattice ... OK" << std::endl;
}


void prepareCoupling(SuperLattice3D<T, DESCRIPTOR>& sLattice1,
                     SuperLattice3D<T, DESCRIPTOR>& sLattice2,
                     SuperLattice3D<T, DESCRIPTOR>& sLattice3,
                     SuperGeometry3D<T>& superGeometry) {
  OstreamManager clout( std::cout,"prepareCoupling" );
  clout << "Add lattice coupling" << endl;

  // The DensityOutlet coupling must be added to the first lattice and come before the ChemicalPotential coupling
  // The InletOutlet couplings must be added to the second lattice and come after the Force coupling.
  FreeEnergyDensityOutletGenerator3D<T,DESCRIPTOR> coupling1( outletDensity );
  FreeEnergyChemicalPotentialGenerator3D<T,DESCRIPTOR> coupling2( alpha, kappa1, kappa2, kappa3 );
  FreeEnergyForceGenerator3D<T,DESCRIPTOR> coupling3;
  FreeEnergyInletOutletGenerator3D<T,DESCRIPTOR> coupling4;

  // Suppress compiler warnings
  coupling1.shift(0, 0, 0);
  coupling2.shift(0, 0, 0);
  coupling3.shift(0, 0, 0);
  coupling4.shift(0, 0, 0);

  sLattice1.addLatticeCoupling<DESCRIPTOR>( superGeometry, 4, coupling1, {&sLattice2, &sLattice3} );
  sLattice1.addLatticeCoupling<DESCRIPTOR>( superGeometry, 1, coupling2, {&sLattice2, &sLattice3} );
  sLattice2.addLatticeCoupling<DESCRIPTOR>( superGeometry, 1, coupling3, {&sLattice1, &sLattice3} );
  sLattice2.addLatticeCoupling<DESCRIPTOR>( superGeometry, 3, coupling4, {&sLattice1, &sLattice3} );
  sLattice2.addLatticeCoupling<DESCRIPTOR>( superGeometry, 4, coupling4, {&sLattice1, &sLattice3} );

  clout << "Add lattice coupling ... OK!" << endl;
}


void getResults( SuperLattice3D<T, DESCRIPTOR>& sLattice1,
                 SuperLattice3D<T, DESCRIPTOR>& sLattice2,
                 SuperLattice3D<T, DESCRIPTOR>& sLattice3, int iT,
                 SuperGeometry3D<T>& superGeometry, Timer<T>& timer,
                 UnitConverter<T, DESCRIPTOR> converter) {

  OstreamManager clout( std::cout,"getResults" );
  SuperVTMwriter3D<T> vtmWriter( "channelFlow3d" );

  if ( iT==0 ) {
    // Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeGeometry3D<T, DESCRIPTOR> geometry( sLattice1, superGeometry );
    SuperLatticeCuboid3D<T, DESCRIPTOR> cuboid( sLattice1 );
    SuperLatticeRank3D<T, DESCRIPTOR> rank( sLattice1 );
    vtmWriter.write( geometry );
    vtmWriter.write( cuboid );
    vtmWriter.write( rank );
    vtmWriter.createMasterFile();
  }

  // Get statistics
  if ( iT%statIter==0 ) {
    // Timer console output
    timer.update( iT );
    timer.printStep();
    sLattice1.getStatistics().print( iT, converter.getPhysTime(iT) );
    sLattice2.getStatistics().print( iT, converter.getPhysTime(iT) );
    sLattice3.getStatistics().print( iT, converter.getPhysTime(iT) );
  }

  // Writes the VTK files
  if ( iT%vtkIter==0 ) {
    SuperLatticeVelocity3D<T, DESCRIPTOR> velocity( sLattice1 );
    SuperLatticeDensity3D<T, DESCRIPTOR> density1( sLattice1 );
    density1.getName() = "rho";
    SuperLatticeDensity3D<T, DESCRIPTOR> density2( sLattice2 );
    density2.getName() = "phi";
    SuperLatticeDensity3D<T, DESCRIPTOR> density3( sLattice3 );
    density3.getName() = "density-fluid-3";
    
    AnalyticalConst3D<T,T> half_( 0.5 );
    SuperLatticeFfromAnalyticalF3D<T, DESCRIPTOR> half(half_, sLattice1);

    SuperIdentity3D<T,T> c1 (half*(density1+density2-density3));
    c1.getName() = "density-fluid-1";
    SuperIdentity3D<T,T> c2 (half*(density1-density2-density3));
    c2.getName() = "density-fluid-2";

    vtmWriter.addFunctor( velocity );
    vtmWriter.addFunctor( density1 );
    vtmWriter.addFunctor( density2 );
    vtmWriter.addFunctor( density3 );
    vtmWriter.addFunctor( c1 );
    vtmWriter.addFunctor( c2 );
    vtmWriter.write( iT );
  }
}


int main( int argc, char *argv[] ) {

  // === 1st Step: Initialization ===

  olbInit( &argc, &argv );
  singleton::directories().setOutputDir( "./tmp/" );
  OstreamManager clout( std::cout,"main" );

  UnitConverterFromResolutionAndRelaxationTime<T,DESCRIPTOR> converter(
    (T)   N, // resolution
    (T)   1., // lattice relaxation time (tau)
    (T)   2. * channelRadius, // charPhysLength: reference length of simulation geometry
    (T)   1.e-6, // charPhysVelocity: maximal/highest expected velocity during simulation in __m / s__
    (T)   0.1, // physViscosity: physical kinematic viscosity in __m^2 / s__
    (T)   1. // physDensity: physical density in __kg / m^3__
  );

  // Prints the converter log as console output
  converter.print();

  // === 2nd Step: Prepare Geometry ===
  std::vector<T> extend = { nx, 2.*channelRadius, 2*channelRadius };
  std::vector<T> origin = { 0, 0, 0 };
  IndicatorCuboid3D<T> cuboid(extend,origin);
#ifdef PARALLEL_MODE_MPI
  CuboidGeometry3D<T> cGeometry( cuboid, converter.getPhysDeltaX(), singleton::mpi().getSize() );
#else
  CuboidGeometry3D<T> cGeometry( cuboid, converter.getPhysDeltaX() );
#endif

  // set periodic boundaries to the domain
  cGeometry.setPeriodicity( false, false, false );
  cGeometry.print();

  // Instantiation of loadbalancer
  HeuristicLoadBalancer<T> loadBalancer( cGeometry );
  loadBalancer.print();

  // Instantiation of superGeometry
  SuperGeometry3D<T> superGeometry( cGeometry,loadBalancer );

  prepareGeometry( superGeometry );

  // === 3rd Step: Prepare Lattice ===
  SuperLattice3D<T, DESCRIPTOR> sLattice1( superGeometry );
  SuperLattice3D<T, DESCRIPTOR> sLattice2( superGeometry );
  SuperLattice3D<T, DESCRIPTOR> sLattice3( superGeometry );

  ForcedBGKdynamics<T, DESCRIPTOR> bulkDynamics1 (
    converter.getLatticeRelaxationFrequency(),
    instances::getBulkMomenta<T,DESCRIPTOR>() );

  FreeEnergyBGKdynamics<T, DESCRIPTOR> bulkDynamics23 (
    converter.getLatticeRelaxationFrequency(), gama,
    instances::getBulkMomenta<T,DESCRIPTOR>() );

  sOnLatticeBoundaryCondition3D<T, DESCRIPTOR> sOnBC1( sLattice1 );
  sOnLatticeBoundaryCondition3D<T, DESCRIPTOR> sOnBC2( sLattice2 );
  sOnLatticeBoundaryCondition3D<T, DESCRIPTOR> sOnBC3( sLattice3 );
  createLocalBoundaryCondition3D<T, DESCRIPTOR> (sOnBC1);
  createLocalBoundaryCondition3D<T, DESCRIPTOR> (sOnBC2);
  createLocalBoundaryCondition3D<T, DESCRIPTOR> (sOnBC3);

  prepareLattice( sLattice1, sLattice2, sLattice3, bulkDynamics1, bulkDynamics23,
                  bulkDynamics23, sOnBC1, sOnBC2, sOnBC3, converter, superGeometry );

  prepareCoupling( sLattice1, sLattice2, sLattice3, superGeometry );

  SuperExternal3D<T, DESCRIPTOR> sExternal1 (superGeometry, sLattice1, 0, 2, sLattice1.getOverlap() );
  SuperExternal3D<T, DESCRIPTOR> sExternal2 (superGeometry, sLattice2, 0, 2, sLattice2.getOverlap() );
  SuperExternal3D<T, DESCRIPTOR> sExternal3 (superGeometry, sLattice3, 0, 2, sLattice3.getOverlap() );

  // === 4th Step: Main Loop with Timer ===
  int iT = 0;
  clout << "starting simulation..." << endl;
  clout << superGeometry.getStatistics().getNvoxel() << endl;
  Timer<T> timer( maxIter, superGeometry.getStatistics().getNvoxel() );
  timer.start();
  
  for ( iT=0; iT<maxIter; ++iT ) {
    // Computation and output of the results
    getResults( sLattice1, sLattice2, sLattice3, iT, superGeometry, timer, converter );

    // Collide and stream execution
    sLattice1.collideAndStream();
    sLattice2.collideAndStream();
    sLattice3.collideAndStream();

    // MPI communication for lattice data
    sLattice1.communicate();
    sLattice2.communicate();
    sLattice3.communicate();
 
    // Execute coupling between the two lattices
    sLattice1.executeCoupling();
    sExternal1.communicate();
    sExternal2.communicate();
    sExternal3.communicate();
    sLattice2.executeCoupling();
  }

  timer.stop();
  timer.printSummary();

}
