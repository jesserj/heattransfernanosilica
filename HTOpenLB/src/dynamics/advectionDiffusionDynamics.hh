/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas, Andrea Parmigiani
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a Cell object
 * can be instantiated -- generic implementation.
 */
#ifndef ADVECTION_DIFFUSION_DYNAMICS_HH
#define ADVECTION_DIFFUSION_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "advectionDiffusionDynamics.h"
#include "dynamics/lbHelpers.h"

namespace olb {


////////////////////// Class AdvectionDiffusionRLBdynamics //////////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
//==================================================================//
//============= Regularized Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
AdvectionDiffusionRLBdynamics<T, DESCRIPTOR>::AdvectionDiffusionRLBdynamics (
  T omega_, Momenta<T, DESCRIPTOR>& momenta_ )
  : BasicDynamics<T, DESCRIPTOR>( momenta_ ),
    omega( omega_ )
{ }

template<typename T, template<typename U> class DESCRIPTOR>
T AdvectionDiffusionRLBdynamics<T, DESCRIPTOR>::computeEquilibrium( int iPop, T rho,
    const T u[DESCRIPTOR<T>::d], T uSqr ) const
{
  return lbHelpers<T, DESCRIPTOR>::equilibriumFirstOrder( iPop, rho, u );
}


template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionRLBdynamics<T, DESCRIPTOR>::collide( Cell<T, DESCRIPTOR>& cell,
    LatticeStatistics<T>& statistics )
{
  T temperature = this->_momenta.computeRho( cell );

  const T* u = cell.getExternal( DESCRIPTOR<T>::ExternalField::velocityBeginsAt );

  T uSqr = lbHelpers<T, DESCRIPTOR>::
           rlbCollision( cell, temperature, u, omega );

  statistics.incrementStats( temperature, uSqr );
}

template<typename T, template<typename U> class DESCRIPTOR>
T AdvectionDiffusionRLBdynamics<T, DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionRLBdynamics<T, DESCRIPTOR>::setOmega( T omega_ )
{
  omega = omega_;
}


////////////////////// Class CombinedAdvectionDiffusionRLBdynamics /////////////////////////

template<typename T, template<typename U> class DESCRIPTOR, typename Dynamics>
CombinedAdvectionDiffusionRLBdynamics<T,DESCRIPTOR,Dynamics>::CombinedAdvectionDiffusionRLBdynamics (
  T omega, Momenta<T,DESCRIPTOR>& momenta )
  : BasicDynamics<T,DESCRIPTOR>(momenta),
    _boundaryDynamics(omega, momenta)
{ }

template<typename T, template<typename U> class DESCRIPTOR, typename Dynamics>
T CombinedAdvectionDiffusionRLBdynamics<T,DESCRIPTOR,Dynamics>::
computeEquilibrium(int iPop, T rho, const T u[DESCRIPTOR<T>::d], T uSqr) const
{
  return lbHelpers<T, DESCRIPTOR>::equilibriumFirstOrder( iPop, rho, u );
}

template<typename T, template<typename U> class DESCRIPTOR, typename Dynamics>
void CombinedAdvectionDiffusionRLBdynamics<T,DESCRIPTOR,Dynamics>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef DESCRIPTOR<T> L;

  T temperature = this->_momenta.computeRho( cell );
  const T* u = cell.getExternal( DESCRIPTOR<T>::ExternalField::velocityBeginsAt );

  T jNeq[DESCRIPTOR<T>::d];
  // this->_momenta.computeJ( cell, jNeq );
  dynamic_cast<AdvectionDiffusionBoundaryMomenta<T,DESCRIPTOR>&>(this->_momenta).computeJneq( cell, jNeq );
  // cout << jNeq[0] << " " << jNeq[1] << " " << u[0] << " " << u[1] << endl;
  // stripe of equilibrium part u * T
  // for ( int iD = 0; iD < DESCRIPTOR<T>::d; ++iD ) {
  //   jNeq[iD] -= u[iD] * temperature;
  // }
  // cout << jNeq[0] << " " << jNeq[1] << " " << u[0] << " " << u[1] << endl;

  for (int iPop = 0; iPop < L::q; ++iPop) {
    cell[iPop] = lbHelpers<T, DESCRIPTOR>::equilibriumFirstOrder( iPop, temperature, u )
                 + firstOrderLbHelpers<T,DESCRIPTOR>::fromJneqToFneq(iPop, jNeq);
    // cout << firstOrderLbHelpers<T,DESCRIPTOR>::fromJneqToFneq(iPop,jNeq) << " ";
  }
  // cout << endl;
  // lbHelpers<T,DESCRIPTOR>::computeJ(cell, jNeq);
  // cout << jNeq[0] << " " << jNeq[1] << " " << u[0] << " " << u[1] << endl;

  _boundaryDynamics.collide(cell, statistics);
  // lbHelpers<T,DESCRIPTOR>::computeJ(cell, jNeq);
  // cout << jNeq[0] << " " << jNeq[1] << " " << u[0] << " " << u[1] << endl;
}

template<typename T, template<typename U> class DESCRIPTOR, typename Dynamics>
T CombinedAdvectionDiffusionRLBdynamics<T,DESCRIPTOR,Dynamics>::getOmega() const
{
  return _boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class DESCRIPTOR, typename Dynamics>
void CombinedAdvectionDiffusionRLBdynamics<T,DESCRIPTOR,Dynamics>::setOmega(T omega)
{
  _boundaryDynamics.setOmega(omega);
}

//==================================================================//
//============= BGK Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::AdvectionDiffusionBGKdynamics (
  T omega, Momenta<T, DESCRIPTOR>& momenta )
  : BasicDynamics<T, DESCRIPTOR>( momenta ),
    _omega(omega)
{ }

template<typename T, template<typename U> class DESCRIPTOR>
AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::AdvectionDiffusionBGKdynamics (
  const UnitConverter<T,DESCRIPTOR>& converter, Momenta<T, DESCRIPTOR>& momenta )
  : BasicDynamics<T, DESCRIPTOR>( momenta ),
    _omega(converter.getLatticeRelaxationFrequency())
{ }

template<typename T, template<typename U> class DESCRIPTOR>
T AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::computeEquilibrium( int iPop, T rho,
    const T u[DESCRIPTOR<T>::d], T uSqr ) const
{
  return lbHelpers<T, DESCRIPTOR>::equilibriumFirstOrder( iPop, rho, u );
}


template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::collide( Cell<T, DESCRIPTOR>& cell,
    LatticeStatistics<T>& statistics )
{
  T temperature = this->_momenta.computeRho( cell );
  const T* u = cell.getExternal(DESCRIPTOR<T>::ExternalField::velocityBeginsAt);

  T uSqr = lbHelpers<T, DESCRIPTOR>::
           bgkCollision( cell, temperature, u, _omega );

  statistics.incrementStats( temperature, uSqr );
}

template<typename T, template<typename U> class DESCRIPTOR>
T AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::setOmega( T omega )
{
  _omega = omega;
}


//==================================================================//
//============= TRT Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
AdvectionDiffusionTRTdynamics<T, DESCRIPTOR>::AdvectionDiffusionTRTdynamics (
  T omega, Momenta<T, DESCRIPTOR>& momenta, T magicParameter )
  : AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>( omega, momenta ),
    _omega2(1/(magicParameter/(1/omega-0.5)+0.5)), _magicParameter(magicParameter)
{ }

template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionTRTdynamics<T, DESCRIPTOR>::collide( Cell<T, DESCRIPTOR>& cell,
    LatticeStatistics<T>& statistics )
{
  T temperature = this->_momenta.computeRho( cell );
  const T* u = cell.getExternal(DESCRIPTOR<T>::ExternalField::velocityBeginsAt);

  T fPlus[DESCRIPTOR<T>::q], fMinus[DESCRIPTOR<T>::q];
  T fEq[DESCRIPTOR<T>::q], fEqPlus[DESCRIPTOR<T>::q], fEqMinus[DESCRIPTOR<T>::q];

  for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
    fPlus[iPop] = 0.5 * ( cell[iPop] + cell[DESCRIPTOR<T>::opposite[iPop]] );
    fMinus[iPop] = 0.5 * ( cell[iPop] - cell[DESCRIPTOR<T>::opposite[iPop]] );
    fEq[iPop] = lbHelpers<T, DESCRIPTOR>::equilibriumFirstOrder(iPop, temperature, u);
  }
  for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
    fEqPlus[iPop] = 0.5 * ( fEq[iPop] + fEq[DESCRIPTOR<T>::opposite[iPop]] );
    fEqMinus[iPop] = 0.5 * ( fEq[iPop] - fEq[DESCRIPTOR<T>::opposite[iPop]] );
  }
  for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
    cell[iPop] -= _omega2 * (fPlus[iPop] - fEqPlus[iPop]) + this->_omega * (fMinus[iPop] - fEqMinus[iPop]);
  }

  statistics.incrementStats( temperature, 0. );
}



//==================================================================//
//============= BGK Model for Advection diffusion with source ===========//
//==================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
SourcedAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::SourcedAdvectionDiffusionBGKdynamics (
  T omega, Momenta<T, DESCRIPTOR>& momenta )
  : AdvectionDiffusionBGKdynamics<T, DESCRIPTOR>( omega, momenta ), _omegaMod(1. - 0.5 * omega)
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size
  OLB_PRECONDITION( DESCRIPTOR<T>::ExternalField::sizeOfSource == 1 );
}


template<typename T, template<typename U> class DESCRIPTOR>
void SourcedAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::collide( Cell<T, DESCRIPTOR>& cell,
    LatticeStatistics<T>& statistics )
{
  const T* u = cell.getExternal(DESCRIPTOR<T>::ExternalField::velocityBeginsAt);
  const T* source = cell.getExternal(DESCRIPTOR<T>::ExternalField::sourceBeginsAt);
  const T temperature = this->_momenta.computeRho( cell ) + 0.5 * source[0];

  T uSqr = lbHelpers<T, DESCRIPTOR>::
           bgkCollision( cell, temperature, u, this->_omega );

  // Q / t_i = ( 1 - 0.5*_omega) * q
  const T sourceMod = source[0] * _omegaMod;
  for ( int iPop = 0; iPop < DESCRIPTOR<T>::q; iPop++ ) {
    cell[iPop] += sourceMod * DESCRIPTOR<T>::t[iPop];
  }

  statistics.incrementStats( temperature, uSqr );
}

template<typename T, template<typename U> class DESCRIPTOR>
T SourcedAdvectionDiffusionBGKdynamics<T,DESCRIPTOR>::computeRho(Cell<T,DESCRIPTOR> const& cell) const
{
  const T* source = cell.getExternal(DESCRIPTOR<T>::ExternalField::sourceBeginsAt);
  return this->_momenta.computeRho( cell ) + 0.5 * source[0];
}

template<typename T, template<typename U> class DESCRIPTOR>
void SourcedAdvectionDiffusionBGKdynamics<T,DESCRIPTOR>::computeRhoU (
  Cell<T,DESCRIPTOR> const& cell,
  T& rho, T u[DESCRIPTOR<T>::d]) const
{
  this->_momenta.computeRhoU( cell, rho, u );
  const T* source = cell.getExternal(DESCRIPTOR<T>::ExternalField::sourceBeginsAt);
  rho += 0.5 * source[0] * _omegaMod;
}


//==================================================================================//
//=========== BGK Model for Advection diffusion with Stokes drag and Smagorinsky====//
//==================================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
SmagorinskyParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::SmagorinskyParticleAdvectionDiffusionBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_, T smagoConst_, T dx_, T dt_)
  : AdvectionDiffusionBGKdynamics<T,DESCRIPTOR>(omega_,momenta_), smagoConst(smagoConst_), preFactor(computePreFactor(omega_,smagoConst_, dx_, dt_) )
{ }

template<typename T, template<typename U> class DESCRIPTOR>
void SmagorinskyParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::collide(Cell<T,DESCRIPTOR>& cell, LatticeStatistics<T>& statistics )
{
  T temperature, uad[DESCRIPTOR<T>::d], pi[util::TensorVal<DESCRIPTOR<T> >::n];
  this->_momenta.computeAllMomenta(cell, temperature, uad, pi);
  int offset = (statistics.getTime() % 2 == 0) ? DESCRIPTOR<T>::ExternalField::velocityBeginsAt : DESCRIPTOR<T>::ExternalField::velocity2BeginsAt;
  const T* u = cell.getExternal(offset);
  T newOmega = computeOmega(this->getOmega(), preFactor, temperature, pi);
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, temperature, u, newOmega);
  statistics.incrementStats(temperature, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T SmagorinskyParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::getSmagorinskyOmega(Cell<T,DESCRIPTOR>& cell)
{
  T temperature, uTemp[DESCRIPTOR<T>::d], pi[util::TensorVal<DESCRIPTOR<T> >::n];
  this->_momenta.computeAllMomenta(cell, temperature, uTemp, pi);
  T newOmega = computeOmega(this->getOmega(), preFactor, temperature, pi);
  return newOmega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void SmagorinskyParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::setOmega(T omega_)
{
  preFactor = computePreFactor(omega_, smagoConst, dx, dt);
}

template<typename T, template<typename U> class DESCRIPTOR>
T SmagorinskyParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::computePreFactor(T omega_, T smagoConst_, T dx_, T dt_)
{
  return (T)(smagoConst_*smagoConst_*dx_*dx_)*DESCRIPTOR<T>::invCs2/dt_*4*sqrt(2);
}

template<typename T, template<typename U> class DESCRIPTOR>
T SmagorinskyParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::computeOmega(T omega0, T preFactor_, T rho, T pi[util::TensorVal<DESCRIPTOR<T> >::n] )
{
  T PiNeqNormSqr = pi[0]*pi[0] + 2.0*pi[1]*pi[1] + pi[2]*pi[2];
  if (util::TensorVal<DESCRIPTOR<T> >::n == 6) {
    PiNeqNormSqr += pi[2]*pi[2] + pi[3]*pi[3] + 2*pi[4]*pi[4] +pi[5]*pi[5];
  }
  T PiNeqNorm    = sqrt(PiNeqNormSqr);
  /// Molecular realaxation time
  T tau_mol = 1. /omega0;
  /// Turbulent realaxation time
  T tau_turb = 0.5*(sqrt(tau_mol*tau_mol+(preFactor_*tau_eff*PiNeqNorm))-tau_mol);
  /// Effective realaxation time
  tau_eff = tau_mol+tau_turb;
  T omega_new= 1./tau_eff;
  return omega_new;
}

//==================================================================//
//=========== BGK Model for Advection diffusion with Stokes Drag ====//
//==================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
ParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::ParticleAdvectionDiffusionBGKdynamics (
  T omega_, Momenta<T, DESCRIPTOR>& momenta_ )
  : AdvectionDiffusionBGKdynamics<T,DESCRIPTOR>(omega_,momenta_), omega( omega_ )
{ }

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleAdvectionDiffusionBGKdynamics<T, DESCRIPTOR>::collide( Cell<T, DESCRIPTOR>& cell,
    LatticeStatistics<T>& statistics )
{
  T temperature = this->_momenta.computeRho( cell );
  int offset = (statistics.getTime() % 2 == 0) ? DESCRIPTOR<T>::ExternalField::velocityBeginsAt : DESCRIPTOR<T>::ExternalField::velocity2BeginsAt;
  const T* u = cell.getExternal(offset);
  T uSqr = lbHelpers<T, DESCRIPTOR>::
           bgkCollision( cell, temperature, u, omega );
  statistics.incrementStats( temperature, uSqr );
}


//==================================================================//
//================= MRT Model for Advection diffusion ==============//
//==================================================================//

template<typename T, template<typename U> class DESCRIPTOR>
AdvectionDiffusionMRTdynamics<T, DESCRIPTOR>::AdvectionDiffusionMRTdynamics(
  T omega, Momenta<T, DESCRIPTOR>& momenta) :
  BasicDynamics<T, DESCRIPTOR>(momenta), _omega(omega)
{
  T rt[DESCRIPTOR<T>::q]; // relaxation times vector.
  for (int iPop = 0; iPop < DESCRIPTOR<T>::q; ++iPop) {
    rt[iPop] = DESCRIPTOR<T>::S[iPop];
  }
  for (int iPop = 0; iPop < DESCRIPTOR<T>::shearIndexes; ++iPop) {
    rt[DESCRIPTOR<T>::shearViscIndexes[iPop]] = omega;
  }
  for (int iPop = 0; iPop < DESCRIPTOR<T>::q; ++iPop) {
    for (int jPop = 0; jPop < DESCRIPTOR<T>::q; ++jPop) {
      invM_S[iPop][jPop] = T();
      for (int kPop = 0; kPop < DESCRIPTOR<T>::q; ++kPop) {
        if (kPop == jPop) {
          invM_S[iPop][jPop] += DESCRIPTOR<T>::invM[iPop][kPop] * rt[kPop];
        }
      }
    }
  }

}

template<typename T, template<typename U> class DESCRIPTOR>
T AdvectionDiffusionMRTdynamics<T, DESCRIPTOR>::computeEquilibrium(int iPop, T rho,
    const T u[DESCRIPTOR<T>::d], T uSqr) const
{
  return lbHelpers<T, DESCRIPTOR>::equilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionMRTdynamics<T, DESCRIPTOR>::collide(Cell<T, DESCRIPTOR>& cell,
    LatticeStatistics<T>& statistics)
{
  T temperature = this->_momenta.computeRho(cell);
  const T* u = cell.getExternal(DESCRIPTOR<T>::ExternalField::velocityBeginsAt);

  T uSqr = lbHelpers<T, DESCRIPTOR>::mrtCollision(cell, temperature, u, invM_S);

  statistics.incrementStats(temperature, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T AdvectionDiffusionMRTdynamics<T, DESCRIPTOR>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void AdvectionDiffusionMRTdynamics<T, DESCRIPTOR>::setOmega(T omega)
{
  _omega = omega;
}



} // namespace olb



#endif
