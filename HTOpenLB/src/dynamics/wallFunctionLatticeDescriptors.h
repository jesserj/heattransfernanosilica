/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Marc Haussmann
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef WALLFUNCTION_LATTICE_DESCRIPTOR_H
#define WALLFUNCTION_LATTICE_DESCRIPTOR_H

#include "dynamics/latticeDescriptors.h"


namespace olb {

namespace descriptors {

/// 3D Descriptors for the forced wall function approach
struct WallFunctionForced3dDescriptor {
  static const int numScalars = 18;
  static const int numSpecies = 5;
  static const int avShearIsAt = 0;
  static const int sizeOfAvShear = 1;
  static const int tauWIsAt   = 1;
  static const int sizeOfTauW   = 1;
  static const int tauEffIsAt   = 2;
  static const int sizeOfTauEff   = 1;
  static const int forceBeginsAt    = 3;
  static const int sizeOfForce      = 3;
  static const int vBeginsAt = 6;
  static const int sizeOfV   = 12;
};

struct WallFunctionForced3dDescriptorBase {
  typedef WallFunctionForced3dDescriptor ExternalField;
};

template <typename T> struct WallFunctionForcedD3Q19Descriptor
  : public D3Q19DescriptorBase<T>, public WallFunctionForced3dDescriptorBase {
};

template <typename T> struct WallFunctionForcedD3Q27Descriptor
  : public D3Q27DescriptorBase<T>, public WallFunctionForced3dDescriptorBase {
};

} // namespace descriptors

} // namespace olb

#endif
