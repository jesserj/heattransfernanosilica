/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Thomas Henn, Mathias J. Krause, Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * BGK Dynamics for porous -- generic implementation.
 */
#ifndef POROUS_BGK_DYNAMICS_HH
#define POROUS_BGK_DYNAMICS_HH

#include "porousBGKdynamics.h"
#include "core/cell.h"
#include "dynamics.h"
#include "core/util.h"
#include "lbHelpers.h"
#include "math.h"

namespace olb {

////////////////////// Class PorousBGKdynamics //////////////////////////

template<typename T, template<typename U> class DESCRIPTOR>
PorousBGKdynamics<T,DESCRIPTOR>::PorousBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_)
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class DESCRIPTOR>
void PorousBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* porosity = cell.getExternal(porosityIsAt);
  for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
    u[i] *= porosity[0];
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T PorousBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void PorousBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_)
{
  omega = omega_;
}


//////////////////// Class ExtendedPorousBGKdynamics ////////////////////

template<typename T, template<typename U> class DESCRIPTOR>
ExtendedPorousBGKdynamics<T,DESCRIPTOR>::ExtendedPorousBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_)
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_),
    omega(omega_)
{
}

template<typename T, template<typename U> class DESCRIPTOR>
void ExtendedPorousBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* porosity = cell.getExternal(porosityIsAt);
  T* localVelocity = cell.getExternal(localDragBeginsAt);

  cell.defineExternalField(localDragBeginsAt,DESCRIPTOR<T>::d, u);

  for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
    u[i] *= porosity[0];
    u[i] += (1.-porosity[0]) * localVelocity[i];
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T ExtendedPorousBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void ExtendedPorousBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_)
{
  omega = omega_;
}

//////////////////// Class SubgridParticleBGKdynamics ////////////////////

template<typename T, template<typename U> class DESCRIPTOR>
SubgridParticleBGKdynamics<T,DESCRIPTOR>::SubgridParticleBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_)
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_),
    omega(omega_)
{
  _fieldTmp[0] = T();
  _fieldTmp[1] = T();
  _fieldTmp[2] = T();
  _fieldTmp[3] = T();
}

template<typename T, template<typename U> class DESCRIPTOR>
void SubgridParticleBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* porosity = cell.getExternal(porosityIsAt);
  T* extVelocity = cell.getExternal(localDragBeginsAt);
//  if (porosity[0] != 0) {
//    cout << "extVelocity: " << extVelocity[0] << " " <<  extVelocity[1] << " " <<  extVelocity[2] << " " << std::endl;
//    cout << "porosity: " << porosity[0] << std::endl;
//  }
  for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
    u[i] *= (1.-porosity[0]);
    u[i] += extVelocity[i];
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);

  statistics.incrementStats(rho, uSqr);
  for (int i=0; i < 4; ++i) {
    cell.getExternal(0)[i] = 0; //_fieldTmp[i];
  }
}

template<typename T, template<typename U> class DESCRIPTOR>
T SubgridParticleBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void SubgridParticleBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_)
{
  omega = omega_;
}

//////////////////// Class PorousParticleBGKdynamics ////////////////////

template<typename T, template<typename U> class DESCRIPTOR, bool isStatic>
PorousParticleBGKdynamics<T,DESCRIPTOR,isStatic>::PorousParticleBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_)
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_),
    omega(omega_)
{}

template<typename T, template<typename U> class DESCRIPTOR, bool isStatic>
void PorousParticleBGKdynamics<T,DESCRIPTOR,isStatic>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* external = cell.getExternal(0);
  T tmpMomentumLoss[DESCRIPTOR<T>::d] = { };

  if (external[velDenominator] > std::numeric_limits<T>::epsilon()) {
    effectiveVelocity<isStatic>::calculate(external, u);
    const T tmp_uSqr = util::normSqr<T,DESCRIPTOR<T>::d>(u);
    for(int tmp_i=0; tmp_i<DESCRIPTOR<T>::d; tmp_i++) {
      for(int tmp_iPop=0; tmp_iPop<DESCRIPTOR<T>::q; tmp_iPop++) {
        tmpMomentumLoss[tmp_i] -= DESCRIPTOR<T>::c[tmp_iPop][tmp_i] * omega * (lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(tmp_iPop, rho, u, tmp_uSqr)-cell[tmp_iPop]);
      }
    }
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
  statistics.incrementStats(rho, uSqr);
  for(int i_dim=0; i_dim<DESCRIPTOR<T>::d; i_dim++)
    external[velNumerator+i_dim] = tmpMomentumLoss[i_dim];

}

template<typename T, template<typename U> class DESCRIPTOR, bool isStatic>
template<bool dummy>
struct PorousParticleBGKdynamics<T,DESCRIPTOR,isStatic>::effectiveVelocity<false, dummy> {
  static void calculate(T* pExternal, T* pVelocity) {
    for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
      pVelocity[i] += (1.-pExternal[DESCRIPTOR<T>::ExternalField::porosityIsAt]) 
          * (pExternal[DESCRIPTOR<T>::ExternalField::velNumerator+i] / pExternal[DESCRIPTOR<T>::ExternalField::velDenominator] - pVelocity[i]);
    }
    pExternal[DESCRIPTOR<T>::ExternalField::porosityIsAt] = 1.;
    pExternal[DESCRIPTOR<T>::ExternalField::velDenominator] = 0.;
  }
};

template<typename T, template<typename U> class Lattice, bool isStatic>
template<bool dummy>
struct PorousParticleBGKdynamics<T,Lattice,isStatic>::effectiveVelocity<true, dummy> {
  static void calculate(T* pExternal, T* pVelocity) {
    for (int i=0; i<Lattice<T>::d; i++)  {
      pVelocity[i] -= (1.-pExternal[Lattice<T>::ExternalField::porosityIsAt]) * pVelocity[i];
    }
  }
};

template<typename T, template<typename U> class DESCRIPTOR, bool isStatic>
T PorousParticleBGKdynamics<T,DESCRIPTOR,isStatic>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR, bool isStatic>
void PorousParticleBGKdynamics<T,DESCRIPTOR,isStatic>::setOmega(T omega_)
{
  omega = omega_;
}

//////////////////// Class KrauseHBGKdynamics ////////////////////

template<typename T, template<typename U> class DESCRIPTOR>
KrauseHBGKdynamics<T,DESCRIPTOR>::KrauseHBGKdynamics (T omega_,
    Momenta<T,DESCRIPTOR>& momenta_, T smagoConst_, T dx_, T dt_ )
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_), omega(omega_), smagoConst(smagoConst_),
    preFactor(computePreFactor(omega_,smagoConst_) )
{
  _fieldTmp[0] = T(1);
  _fieldTmp[1] = T();
  _fieldTmp[2] = T();
  _fieldTmp[3] = T();
}

template<typename T, template<typename U> class DESCRIPTOR>
void KrauseHBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  T newOmega[DESCRIPTOR<T>::q];
  this->_momenta.computeRhoU(cell, rho, u);
  computeOmega(this->getOmega(), cell, preFactor, rho, u, newOmega);

  T vel_denom = *cell.getExternal(velDenominator);
  if (vel_denom > std::numeric_limits<T>::epsilon()) {
    T porosity = *cell.getExternal(porosityIsAt); // prod(1-smoothInd)
    T* vel_num = cell.getExternal(velNumerator);
    porosity = 1.-porosity; // 1-prod(1-smoothInd)
    for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
      u[i] += porosity * (vel_num[i] / vel_denom - u[i]);
    }
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, newOmega);
  statistics.incrementStats(rho, uSqr);

  for (int i=0; i < 4; ++i) {
    cell.getExternal(0)[i] = _fieldTmp[i];
  }
}

template<typename T, template<typename U> class DESCRIPTOR>
T KrauseHBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void KrauseHBGKdynamics<T,DESCRIPTOR>::setOmega(T omega)
{
  this->setOmega(omega);
  preFactor = computePreFactor(omega, smagoConst);
}

template<typename T, template<typename U> class DESCRIPTOR>
T KrauseHBGKdynamics<T,DESCRIPTOR>::computePreFactor(T omega, T smagoConst)
{
  return (T)smagoConst*smagoConst*DESCRIPTOR<T>::invCs2*DESCRIPTOR<T>::invCs2*2*sqrt(2);
}


template<typename T, template<typename U> class DESCRIPTOR>
void KrauseHBGKdynamics<T,DESCRIPTOR>::computeOmega(T omega0, Cell<T,DESCRIPTOR>& cell, T preFactor, T rho,
    T u[DESCRIPTOR<T>::d], T newOmega[DESCRIPTOR<T>::q])
{
  T uSqr = u[0]*u[0];
  for (int iDim=0; iDim<DESCRIPTOR<T>::d; iDim++) {
    uSqr += u[iDim]*u[iDim];
  }
  /// Molecular realaxation time
  T tau_mol = 1./omega0;

  for (int iPop=0; iPop<DESCRIPTOR<T>::q; iPop++) {
    T fNeq = std::fabs(cell[iPop] - lbHelpers<T,DESCRIPTOR>::equilibrium(iPop, rho, u, uSqr));
    /// Turbulent realaxation time
    T tau_turb = 0.5*(sqrt(tau_mol*tau_mol+(preFactor*fNeq))-tau_mol);
    /// Effective realaxation time
    tau_eff = tau_mol + tau_turb;
    newOmega[iPop] = 1./tau_eff;
  }
}


//////////////////// Class ParticlePorousBGKdynamics ////////////////////
/*
template<typename T, template<typename U> class DESCRIPTOR>
ParticlePorousBGKdynamics<T,DESCRIPTOR>::ParticlePorousBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_)
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class DESCRIPTOR>
void ParticlePorousBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* porosity = cell.getExternal(porosityIsAt);
  T* localVelocity = cell.getExternal(localDragBeginsAt);
  for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
    u[i] *= porosity[0];
    u[i] += localVelocity[i];
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
  statistics.incrementStats(rho, uSqr);

//  *cell.getExternal(porosityIsAt) = 1;
//  *cell.getExternal(localDragBeginsAt) = 0.;
//  *(cell.getExternal(localDragBeginsAt)+1) = 0.;
}

template<typename T, template<typename U> class DESCRIPTOR>
T ParticlePorousBGKdynamics<T,DESCRIPTOR>::getOmega() const {
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticlePorousBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_) {
  omega = omega_;
}
*/

//////////////////// Class SmallParticleBGKdynamics ////////////////////

template<typename T, template<typename U> class DESCRIPTOR>
SmallParticleBGKdynamics<T,DESCRIPTOR>::SmallParticleBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_)
  : BGKdynamics<T,DESCRIPTOR>(omega_,momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class DESCRIPTOR>
void SmallParticleBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* porosity = cell.getExternal(porosityIsAt);
  T* localVelocity = cell.getExternal(localDragBeginsAt);

  //cout << porosity[0]  << " " <<   localVelocity[0]<< " " <<   localVelocity[1]<< " " <<   localVelocity[2]<<std::endl;
  for (int i=0; i<DESCRIPTOR<T>::d; i++)  {
    u[i] *= porosity[0];
    u[i] += localVelocity[i];
  }
  T uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T SmallParticleBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void SmallParticleBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_)
{
  omega = omega_;
}

////////////////////// Class PSMBGKdynamics //////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class DESCRIPTOR>
PSMBGKdynamics<T,DESCRIPTOR>::PSMBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_, int mode_ )
  : BGKdynamics<T,DESCRIPTOR>(omega_, momenta_),
    omega(omega_), paramA(1. / omega_ - 0.5)
{
  mode = (Mode) mode_;
}

template<typename T, template<typename U> class DESCRIPTOR>
void PSMBGKdynamics<T,DESCRIPTOR>::computeU (Cell<T,DESCRIPTOR> const& cell, T u[DESCRIPTOR<T>::d] ) const
{
  T rho;
  this->_momenta.computeRhoU(cell, rho, u);
//  T epsilon = 1. - *(cell.getExternal(DESCRIPTOR<T>::ExternalField::porosityIsAt));
//  // speed up paramB
//  T paramB = (epsilon * paramA) / ((1. - epsilon) + paramA);
//  // speed up paramC
//  T paramC = (1. - paramB);
//  for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
//    u[iVel] = paramC * u[iVel] +
//              paramB * cell.getExternal(DESCRIPTOR<T>::ExternalField::velocitySolidIsAt)[iVel];
//  }
}

template<typename T, template<typename U> class DESCRIPTOR>
void PSMBGKdynamics<T,DESCRIPTOR>::computeRhoU (Cell<T,DESCRIPTOR> const& cell, T& rho, T u[DESCRIPTOR<T>::d] ) const
{
    this->_momenta.computeRhoU(cell, rho, u);
//  T epsilon = 1. - *(cell.getExternal(DESCRIPTOR<T>::ExternalField::porosityIsAt));
//  // speed up paramB
//  T paramB = (epsilon * paramA) / ((1. - epsilon) + paramA);
//  // speed up paramC
//  T paramC = (1. - paramB);
//  for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
//    u[iVel] = paramC * u[iVel] +
//              paramB * cell.getExternal(DESCRIPTOR<T>::ExternalField::velocitySolidIsAt)[iVel];
//  }
}

template<typename T, template<typename U> class DESCRIPTOR>
void PSMBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d], uSqr;
  T epsilon = 1. - *(cell.getExternal(DESCRIPTOR<T>::ExternalField::porosityIsAt));

  this->_momenta.computeRhoU(cell, rho, u);
  // velocity at the boundary
  T u_s[DESCRIPTOR<T>::d];
  for (int i = 0; i < DESCRIPTOR<T>::d; i++) {
    u_s[i] = (cell.getExternal(DESCRIPTOR<T>::ExternalField::velocitySolidIsAt))[i];
  }

  if (epsilon < 1e-5) {
    uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
  } else {
    // speed up paramB
    T paramB = (epsilon * paramA) / ((1. - epsilon) + paramA);
    // speed up paramC
    T paramC = (1. - paramB);

    T omega_s[DESCRIPTOR<T>::q];
    T cell_tmp[DESCRIPTOR<T>::q];

    const T uSqr_s = util::normSqr<T,DESCRIPTOR<T>::d>(u_s);

    uSqr = util::normSqr<T,DESCRIPTOR<T>::d>(u);
    for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
      cell_tmp[iPop] = cell[iPop];
      switch(mode){
        case M2: omega_s[iPop] = (lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(iPop, rho, u_s, uSqr_s ) - cell[iPop])
                         + (1 - omega) * (cell[iPop] - lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(iPop, rho, u, uSqr )); break;
        case M3: omega_s[iPop] = (cell[DESCRIPTOR<T>::opposite[iPop]] - lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(DESCRIPTOR<T>::opposite[iPop], rho, u_s, uSqr_s ))
               - (cell[iPop] - lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(iPop, rho, u_s, uSqr_s ));
      }

    }

    uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);

    for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
      cell[iPop] = cell_tmp[iPop] + paramC * (cell[iPop] - cell_tmp[iPop]);
      cell[iPop] += paramB * omega_s[iPop];
    }
    for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
      u[iVel] = paramC * u[iVel] + paramB * u_s[iVel];
    }
    uSqr = util::normSqr<T,DESCRIPTOR<T>::d>(u);
  }
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T PSMBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void PSMBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_)
{
  paramA = (1. / omega_ - 0.5);
  omega = omega_;

}

////////////////////// Class ForcedPSMBGKdynamics //////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class DESCRIPTOR>
ForcedPSMBGKdynamics<T,DESCRIPTOR>::ForcedPSMBGKdynamics (
  T omega_, Momenta<T,DESCRIPTOR>& momenta_, int mode_ )
  : ForcedBGKdynamics<T,DESCRIPTOR>(omega_, momenta_),
    omega(omega_), paramA(1. / omega_ - 0.5)
{
  mode = (Mode) mode_;
}


template<typename T, template<typename U> class DESCRIPTOR>
void ForcedPSMBGKdynamics<T,DESCRIPTOR>::computeU (Cell<T,DESCRIPTOR> const& cell, T u[DESCRIPTOR<T>::d] ) const
{
  T rho;
  this->_momenta.computeRhoU(cell, rho, u);
  T epsilon = 1. - *(cell.getExternal(DESCRIPTOR<T>::ExternalField::porosityIsAt));
  // speed up paramB
  T paramB = (epsilon * paramA) / ((1. - epsilon) + paramA);
  // speed up paramC
  T paramC = (1. - paramB);
  for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
    u[iVel] = paramC * (u[iVel] + cell.getExternal(this->forceBeginsAt)[iVel] / (T)2.) +
              paramB * cell.getExternal(DESCRIPTOR<T>::ExternalField::velocitySolidIsAt)[iVel];
  }
}

template<typename T, template<typename U> class DESCRIPTOR>
void ForcedPSMBGKdynamics<T,DESCRIPTOR>::computeRhoU (Cell<T,DESCRIPTOR> const& cell, T& rho, T u[DESCRIPTOR<T>::d] ) const
{
  this->_momenta.computeRhoU(cell, rho, u);
  T epsilon = 1. - *(cell.getExternal(DESCRIPTOR<T>::ExternalField::porosityIsAt));
  // speed up paramB
  T paramB = (epsilon * paramA) / ((1. - epsilon) + paramA);
  // speed up paramC
  T paramC = (1. - paramB);
  for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
    u[iVel] = paramC * (u[iVel] + cell.getExternal(this->forceBeginsAt)[iVel] / (T)2.) +
              paramB * cell.getExternal(DESCRIPTOR<T>::ExternalField::velocitySolidIsAt)[iVel];
  }
}

template<typename T, template<typename U> class DESCRIPTOR>
void ForcedPSMBGKdynamics<T,DESCRIPTOR>::collide (
  Cell<T,DESCRIPTOR>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[DESCRIPTOR<T>::d], uSqr;
  T epsilon = 1. - *(cell.getExternal(DESCRIPTOR<T>::ExternalField::porosityIsAt));

  this->_momenta.computeRhoU(cell, rho, u);

  T* force = cell.getExternal(this->forceBeginsAt);
  for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
    u[iVel] += force[iVel] / (T)2.;
  }
  // velocity at the boundary
  T u_s[DESCRIPTOR<T>::d];
  for (int i = 0; i < DESCRIPTOR<T>::d; i++) {
    u_s[i] = (cell.getExternal(DESCRIPTOR<T>::ExternalField::velocitySolidIsAt))[i];
  }

  if (epsilon < 1e-5) {
    uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
    lbHelpers<T,DESCRIPTOR>::addExternalForce(cell, u, omega, rho);
  } else {
    // speed up paramB
    T paramB = (epsilon * paramA) / ((1. - epsilon) + paramA);
    // speed up paramC
    T paramC = (1. - paramB);

    T omega_s[DESCRIPTOR<T>::q];
    T cell_tmp[DESCRIPTOR<T>::q];

    const T uSqr_s = util::normSqr<T,DESCRIPTOR<T>::d>(u_s);

    for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
      cell_tmp[iPop] = cell[iPop];
      switch(mode){
        case M2: omega_s[iPop] = (lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(iPop, rho, u_s, uSqr_s ) - cell[iPop])
                         + (1 - omega) * (cell[iPop] - lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(iPop, rho, u, uSqr )); break;
        case M3: omega_s[iPop] = (cell[DESCRIPTOR<T>::opposite[iPop]] - lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(DESCRIPTOR<T>::opposite[iPop], rho, u_s, uSqr_s ))
               - (cell[iPop] - lbDynamicsHelpers<T,DESCRIPTOR<T>>::equilibrium(iPop, rho, u_s, uSqr_s ));
      }
    }

    uSqr = lbHelpers<T,DESCRIPTOR>::bgkCollision(cell, rho, u, omega);
    lbHelpers<T,DESCRIPTOR>::addExternalForce(cell, u, omega, rho);

    for (int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop) {
      cell[iPop] = cell_tmp[iPop] + paramC * (cell[iPop] - cell_tmp[iPop]);
      cell[iPop] += paramB * omega_s[iPop];
    }
    for (int iVel=0; iVel<DESCRIPTOR<T>::d; ++iVel) {
      u[iVel] = paramC * u[iVel] + paramB * u_s[iVel];
    }
    uSqr = util::normSqr<T,DESCRIPTOR<T>::d>(u);
  }
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class DESCRIPTOR>
T ForcedPSMBGKdynamics<T,DESCRIPTOR>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class DESCRIPTOR>
void ForcedPSMBGKdynamics<T,DESCRIPTOR>::setOmega(T omega_)
{
  paramA = (1. / omega_ - 0.5);
  omega = omega_;

}


} // olb

#endif
