/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014-2016 Cyril Masquelier, Mathias J. Krause, Albert Mink
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef SMOOTH_INDICATOR_F_3D_HH
#define SMOOTH_INDICATOR_F_3D_HH

#include <vector>
#include <cmath>
#include <sstream>

#include "smoothIndicatorF3D.h"
#include "smoothIndicatorBaseF3D.h"
#include "smoothIndicatorCalcF3D.h"
#include "utilities/vectorHelpers.h"
#include "functors/analytical/interpolationF3D.h"
#include "functors/lattice/reductionF3D.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#ifndef M_PI2
#define M_PI2 1.57079632679489661923
#endif

namespace olb {

template <typename T, typename S, bool HLBM>
SmoothIndicatorCuboid3D<T,S,HLBM>::SmoothIndicatorCuboid3D(Vector<S,3> center, S xLength, S yLength, S zLength, S density, S epsilon, Vector<S,3> theta, Vector<S,3> vel)
  : _xLength(xLength),_yLength(yLength),_zLength(zLength)
{
  this->_pos = center;
  this->_circumRadius = .5*(sqrt(xLength*xLength+yLength*yLength+zLength*zLength))+0.5*epsilon;
  this->_myMin = {
    center[0] - this->getCircumRadius(), 
    center[1] - this->getCircumRadius(),
    center[2] - this->getCircumRadius()
  };
  this->_myMax = {
    center[0] + this->getCircumRadius(),
    center[1] + this->getCircumRadius(),
    center[2] + this->getCircumRadius()
  };
  this->_epsilon = epsilon;
  this->_theta = theta;
  
  T mass = xLength*yLength*zLength*density;
  T xLength2 = xLength*xLength;
  T yLength2 = yLength*yLength;
  T zLength2 = zLength*zLength;
  Vector<S,3> mofi;
  mofi[0] = 1./12.*mass*(yLength2+zLength2);
  mofi[1] = 1./12.*mass*(xLength2+zLength2);
  mofi[2] = 1./12.*mass*(yLength2+xLength2);
  this->init(theta, vel, mass, mofi);
}

template <typename T, typename S, bool HLBM>
bool SmoothIndicatorCuboid3D<T,S,HLBM>::operator()(T output[], const S input[])
{
  T xDist = input[0] - this->getCenter()[0];
  T yDist = input[1] - this->getCenter()[1];
  T zDist = input[2] - this->getCenter()[2];

  // counter-clockwise rotation by _theta=-theta around center
  T x = this->getCenter()[0] + this->getRotationMatrix()[0]*xDist + this->getRotationMatrix()[3]*yDist + this->getRotationMatrix()[6]*zDist;
  T y = this->getCenter()[1] + this->getRotationMatrix()[1]*xDist + this->getRotationMatrix()[4]*yDist + this->getRotationMatrix()[7]*zDist;
  T z = this->getCenter()[2] + this->getRotationMatrix()[2]*xDist + this->getRotationMatrix()[5]*yDist + this->getRotationMatrix()[8]*zDist;

  xDist = fabs(x-this->getCenter()[0]) - 0.5*(_xLength-this->getEpsilon());
  yDist = fabs(y-this->getCenter()[1]) - 0.5*(_yLength-this->getEpsilon());
  zDist = fabs(z-this->getCenter()[2]) - 0.5*(_zLength-this->getEpsilon());

  if (xDist <= 0 && yDist <= 0 && zDist <= 0) {
    output[0] = 1.;
    return true;
  }
  if (xDist >= this->getEpsilon() || yDist >= this->getEpsilon() || zDist >= this->getEpsilon()) {
    output[0] = 0.;
    return false;
  }
  // treating edges and corners as "rounded"
  T count = 0.;
  T dist2 = 0.;
  if (xDist < this->getEpsilon() && xDist > 0) {
    count += 1;
    dist2 += xDist*xDist;
  }
  if (yDist < this->getEpsilon() && yDist > 0) {
    count += 1;
    dist2 += yDist*yDist;
  }
  if (zDist < this->getEpsilon() && zDist > 0) {
    count += 1;
    dist2 += zDist*zDist;
  }
  if (count > 0) {
    output[0] = T(cos(M_PI2*sqrt(dist2)/this->getEpsilon())*cos(M_PI2*sqrt(dist2)/this->getEpsilon()));
    return true;
  }
  output[0] = 0.;
  return false;
}

template <typename T, typename S, bool HLBM>
SmoothIndicatorSphere3D<T,S,HLBM>::SmoothIndicatorSphere3D(Vector<S,3> center,
    S radius, S epsilon, S density, Vector<S,3> vel)
  : _radius(radius)
{
  this->_pos = center;
  this->_circumRadius = radius + 0.5*epsilon;
  this->_myMin = {
    center[0] - this->getCircumRadius(), 
    center[1] - this->getCircumRadius(),
    center[2] - this->getCircumRadius()
  };
  this->_myMax = {
    center[0] + this->getCircumRadius(),
    center[1] + this->getCircumRadius(),
    center[2] + this->getCircumRadius()
  };
  this->_epsilon = epsilon;

  T mass = 4./3.*M_PI*std::pow(radius, 3.)*density;
  T radius2 = radius * radius;
  Vector<S,3> mofi;
  mofi[0] = 2./5.*mass*radius2;
  mofi[1] = 2./5.*mass*radius2;
  mofi[2] = 2./5.*mass*radius2;
  Vector<S,3> theta(0.,0.,0.);
  this->init(theta, vel, mass, mofi);
}

// returns true if x is inside the sphere
template <typename T, typename S, bool HLBM>
bool SmoothIndicatorSphere3D<T, S, HLBM>::operator()(T output[], const S input[])
{
  T distToCenter2 = (this->getCenter()[0]-input[0])*(this->getCenter()[0]-input[0]) +
                         (this->getCenter()[1]-input[1])*(this->getCenter()[1]-input[1]) +
                         (this->getCenter()[2]-input[2])*(this->getCenter()[2]-input[2]); 
  T circumRadius2 = this->getCircumRadius()*this->getCircumRadius();
  if ( distToCenter2 >= circumRadius2 ) {
    output[0] = 0.;
    return false;
  } else if ( distToCenter2 <= std::pow(this->getCircumRadius()-this->getEpsilon(), 2) ) {
    output[0] = 1.;
    return true;
  } else {
    // d is between 0 and epsilon
    T d = std::sqrt(distToCenter2) - this->getCircumRadius();
    output[0] = T(cos(M_PI*d/this->getEpsilon())*cos(M_PI*d/this->getEpsilon()));
    return true;
  }
  return false;
}

template <typename T, typename S, bool HLBM>
SmoothIndicatorCylinder3D<T,S,HLBM>::SmoothIndicatorCylinder3D(Vector<S,3> center, S length, S radius, S density, S epsilon, Vector<S,3> theta, Vector<S,3> vel)
 : _length(length), _radius(radius)
{
  this->_pos = center;
  this->_circumRadius = std::sqrt(radius*radius+(0.5*length)*(0.5*length))+0.5*this->getEpsilon();
  this->_myMin = {
    center[0] - this->getCircumRadius(),
    center[1] - this->getCircumRadius(),
    center[2] - this->getCircumRadius()
  };
  this->_myMax = {
    center[0] + this->getCircumRadius(),
    center[1] + this->getCircumRadius(),
    center[2] + this->getCircumRadius()
  };
  this->_epsilon = epsilon;
  this->_theta = theta; 
 
  T mass = M_PI*std::pow(radius, 3.)*density;
  T radius2 = radius * radius;
  Vector<S,3> mofi;
  mofi[0] = 0.5*mass*radius2;
  mofi[1] = 1/12.*mass*(length*length+3.*radius2);
  mofi[2] = 1/12.*mass*(length*length+3.*radius2);
  this->init(theta, vel, mass, mofi);
}

template <typename T, typename S, bool HLBM>
SmoothIndicatorCylinder3D<T,S,HLBM>::SmoothIndicatorCylinder3D(Vector<S,3> center1, Vector<S,3> center2, S radius, S epsilon, Vector<S,3> theta, Vector<S,3> vel)
 : _radius(radius)
{
  T dx = center1[0]-center2[0];
  T dy = center1[1]-center2[1];
  T dz = center1[2]-center2[2];
  _length = sqrt( dx*dx + dy*dy + dz*dz );
  this->_pos[0] = 0.5*(center1[0]+center2[0]);
  this->_pos[1] = 0.5*(center1[1]+center2[1]);
  this->_pos[2] = 0.5*(center1[2]+center2[2]);
  this->_circumRadius = std::sqrt(radius*radius+(0.5*_length)*(0.5*_length))+0.5*this->getEpsilon();
  this->_myMin = {
    this->_pos[0] - this->getCircumRadius(),
    this->_pos[1] - this->getCircumRadius(),
    this->_pos[2] - this->getCircumRadius()
  };
  this->_myMax = {
    this->_pos[0] + this->getCircumRadius(),
    this->_pos[1] + this->getCircumRadius(),
    this->_pos[2] + this->getCircumRadius()
  };
  this->_epsilon = epsilon;
  this->_theta = theta;

  T mass = M_PI*std::pow(radius, 3.)*1.; // density set to one here (needs work)
  T radius2 = radius * radius;
  Vector<S,3> mofi;
  mofi[0] = 0.5*mass*radius2;
  mofi[1] = 1/12.*mass*(_length*_length+3.*radius2);
  mofi[2] = 1/12.*mass*(_length*_length+3.*radius2);
  this->init(theta, vel, mass, mofi);
}

template <typename T, typename S, bool HLBM>
bool SmoothIndicatorCylinder3D<T,S,HLBM>::operator()(T output[],const S input[])
{
  T xDist = input[0] - this->getCenter()[0];
  T yDist = input[1] - this->getCenter()[1];
  T zDist = input[2] - this->getCenter()[2];

  // counter-clockwise rotation by _theta=-theta around center
  T x= this->getCenter()[0] + this->getRotationMatrix()[0]*xDist + this->getRotationMatrix()[3]*yDist + this->getRotationMatrix()[6]*zDist;
  T y= this->getCenter()[1] + this->getRotationMatrix()[1]*xDist + this->getRotationMatrix()[4]*yDist + this->getRotationMatrix()[7]*zDist;
  T z= this->getCenter()[2] + this->getRotationMatrix()[2]*xDist + this->getRotationMatrix()[5]*yDist + this->getRotationMatrix()[8]*zDist;

  xDist = fabs(x -this-> getCenter()[0]);
  yDist = fabs(y -this-> getCenter()[1]);
  zDist = fabs(z -this-> getCenter()[2]);
  T tmp = std::sqrt((yDist*yDist+xDist*xDist));

  if ( zDist <= (_length-this->getEpsilon())*0.5 && tmp <= _radius-0.5*this->getEpsilon()) {
    output[0] = 1.;
    return true;
  } else if (zDist <= (_length-this->getEpsilon())*0.5 && tmp <= _radius+0.5*this->getEpsilon()) {
    output[0] = T( std::pow(cos(0.5*M_PI*(tmp - _radius)/this->_epsilon), 2));
    return true;
  } else if (zDist <= (_length+this->_epsilon)*0.5 && tmp <= _radius-0.5*this->getEpsilon()) {
    output[0] = T( std::pow(cos(0.5*M_PI*(zDist - 0.5*_length)/this->_epsilon), 2));
    return true;
  } else if (zDist <= (_length+this->_epsilon)*0.5 && tmp <= _radius+0.5*this->getEpsilon()) {
    output[0] = T( std::pow(cos(0.5*M_PI*((tmp-_radius)+(zDist-0.5*_length))/(2.*this->_epsilon)), 2    ));
    return true;
  } else {
    output[0] = 0.;
    return false;
  }
  output[0] = 0.;
  return false;
}

//TODO: TO Be Repaired
//TODO: Check for consitency
//template <typename T, typename S, template<typename U> class DESCRIPTOR, bool HLBM>
//SmoothIndicatorCustom3D<T,S,DESCRIPTOR,HLBM>::SmoothIndicatorCustom3D(UnitConverter<T,DESCRIPTOR> const& converter,
//    IndicatorF3D<T>& ind,
//    Vector<T,3> center,
//    T rhoP,
//    T epsilon,
//    Vector<T,3> theta,
//    Vector<T,3> vel )
//  : _converter(converter)
//{
//  OstreamManager clout(std::cout,"createIndicatorCustom3D");
//  this->_pos = center;
//  this->_vel = vel;
//  this->_epsilon = epsilon;
//  this->_theta = theta;
//
//  // initialize rotation matrix
//  Vector<int,3> ct;
//  Vector<int,3> st;
//  ct[0] = std::cos(this->_theta[0]);
//  ct[1] = std::cos(this->_theta[1]);
//  ct[2] = std::cos(this->_theta[2]);
//  st[0] = std::sin(this->_theta[0]);
//  st[1] = std::sin(this->_theta[1]);
//  st[2] = std::sin(this->_theta[2]);
//
//  this->_rotMat[0] = ct[1]*ct[2];
//  this->_rotMat[1] = st[0]*st[1]*ct[2] - ct[0]*st[2];
//  this->_rotMat[2] = ct[0]*st[1]*ct[2] + st[0]*st[2];
//  this->_rotMat[3] = ct[1]*st[2];
//  this->_rotMat[4] = st[0]*st[1]*st[2] + ct[0]*ct[2];
//  this->_rotMat[5] = ct[0]*st[1]*st[2] - st[0]*ct[2];
//  this->_rotMat[6] = -st[1];
//  this->_rotMat[7] = st[0]*ct[1];
//  this->_rotMat[8] = ct[0]*ct[1];
//
//  // initialize temporary values
//  SmoothBlockIndicator3D<T,olb::descriptors::D3Q19Descriptor> smoothBlock(ind, _converter.getConversionFactorLength(), this->_epsilon/_converter.getConversionFactorLength());
//  int _nX = smoothBlock.getBlockData().getNx();
//  int _nY = smoothBlock.getBlockData().getNy();
//  int _nZ = smoothBlock.getBlockData().getNz();
//  T tmpNcells = 0.0;
//
//  // create smoothed blockData
//  BlockData3D<T,BaseType> block_tmp(_nX, _nY, _nZ);
//  for (int iX=0; iX < _nX; iX++) {
//    for (int iY=0; iY < _nY; iY++) {
//      for (int iZ=0; iZ < _nZ; iZ++) {
//        block_tmp.get(iX, iY, iZ) = smoothBlock.getBlockData().get(iX, iY, iZ);
//        // check if above 0.499 since real boundary is at 0.5 due to smoothing
//        if (block_tmp.get(iX, iY, iZ) > 0.499) {
//          tmpNcells += block_tmp.get(iX, iY, iZ);
//        }
//      }
//    }
//  }
//  this->_blockData = block_tmp;
//  T invNcells = 1./tmpNcells;
//
//  // calculate mass and centerpoint for rotation
//  // TODO check again for correctness of center due to smooth boundary and coordinate system
//  this->_mass = rhoP * tmpNcells * pow(_converter.getConversionFactorLength(),3);
//  this->_center[0] = 0.0;
//  this->_center[1] = 0.0;
//  this->_center[2] = 0.0;
//  Vector<T,3> tmpCenter;
//  tmpCenter[0] = 0.0 + _converter.getPhysLength(_nX)/2.;
//  tmpCenter[1] = 0.0 + _converter.getPhysLength(_nY)/2.;
//  tmpCenter[2] = 0.0 + _converter.getPhysLength(_nZ)/2.;
//  //this->_latticeCenter[0] = 0;
//  //this->_latticeCenter[1] = 0;
//  //this->_latticeCenter[2] = 0;
//  for (int iX= 0; iX < _nX; iX++) {
//    for (int iY = 0; iY < _nY; iY++) {
//      for (int iZ = 0; iZ < _nZ; iZ++) {
//        if (this->_blockData.get(iX,iY,iZ) > 0.5-std::numeric_limits<T>::epsilon()) {
//          this->_center[0] += (_converter.getPhysLength(iX)+0.5*_converter.getConversionFactorLength()-tmpCenter[0]) * this->_blockData.get(iX, iY, iZ);
//          this->_center[1] += (_converter.getPhysLength(iY)+0.5*_converter.getConversionFactorLength()-tmpCenter[1]) * this->_blockData.get(iX, iY, iZ);
//          this->_center[2] += (_converter.getPhysLength(iZ)+0.5*_converter.getConversionFactorLength()-tmpCenter[2]) * this->_blockData.get(iX, iY, iZ);
//        }
//      }
//    }
//  }
//  this->_center[0] = this->_center[0] * invNcells + tmpCenter[0];
//  this->_center[1] = this->_center[1] * invNcells + tmpCenter[1];
//  this->_center[2] = this->_center[2] * invNcells + tmpCenter[2];
//  this->_latticeCenter[0] = this->_converter.getLatticeLength(this->_center[0]);  // TODO converter MG
//  this->_latticeCenter[1] = this->_converter.getLatticeLength(this->_center[1]);
//  this->_latticeCenter[2] = this->_converter.getLatticeLength(this->_center[2]);
//
//  // calculate moment of inertia
//  // TODO - calculation
//  T cuboidMofi = pow(_converter.getPhysLength(1.), 2)/ 6.0; // Single cuboid mofi at center of gravity
//  T cuboidMass = this->_mass*invNcells;
//  this->_mofi[0] = 0; // x
//  this->_mofi[1] = 0; // y
//  this->_mofi[2] = 0; // z
//  T halfLattice = 0.5*_converter.getPhysLength(1.);
//  T dx, dz, dy;
//  for (int iX = 0; iX < _nX; iX++) {
//    dx = std::abs(_converter.getPhysLength(iX) - this->_center[0] + halfLattice);
//    for (int iY = 0; iY < _nY; iY++) {
//      dy = std::abs(_converter.getPhysLength(iY) - this->_center[1] + halfLattice);
//      for (int iZ = 0; iZ < _nZ; iZ++) {
//        if (this->_blockData.get(iX,iY,iZ) > 0.5-std::numeric_limits<T>::epsilon()) {
//          dz = std::abs(_converter.getPhysLength(iZ) - this->_center[2] + halfLattice);
//          this->_mofi[0] += (dy*dy+dz*dz+cuboidMofi)*this->_blockData.get(iX,iY,iZ);
//          this->_mofi[1] += (dx*dx+dz*dz+cuboidMofi)*this->_blockData.get(iX,iY,iZ);
//          this->_mofi[2] += (dx*dx+dy*dy+cuboidMofi)*this->_blockData.get(iX,iY,iZ);
//        }
//      }
//    }
//  }
//  this->_mofi[0] *= cuboidMass;
//  this->_mofi[1] *= cuboidMass;
//  this->_mofi[2] *= cuboidMass;
//
//  // calculate min and max from circumRadius
//  T distance = 0.;
//  for (int iX = 0; iX < _nX; iX++) {
//    T x = _converter.getPhysLength(iX);
//    for (int iY = 0; iY < _nY; iY++) {
//      T y = _converter.getPhysLength(iY);
//      for (int iZ = 0; iZ < _nZ; iZ++) {
//        T z = _converter.getPhysLength(iZ);
//        if (this->_blockData.get(iX,iY,iZ) > std::numeric_limits<T>::epsilon()) {
//          T tmpDist = sqrt(pow(this->_center[0]-x,2)+pow(this->_center[1]-y,2)+pow(this->_center[2]-z,2));
//          if (tmpDist > distance) {
//            distance = tmpDist;
//          }
//        }
//      }
//    }
//  }
//  this->_myMin[0] = -this->_epsilon - distance;
//  this->_myMin[1] = -this->_epsilon - distance;
//  this->_myMin[2] = -this->_epsilon - distance;
//  this->_myMax[0] = this->_epsilon + distance;
//  this->_myMax[1] = this->_epsilon + distance;
//  this->_myMax[2] = this->_epsilon + distance;
//  this->_name = "custom3D";
//
//  clout << "----->>>>> Cell Number: " << _nX << " // " << _nY << " // " << _nZ << std::endl;
//  clout << "----->>>>> Local Center: " << this->_center[0] << " // " << this->_center[1] << " // " << this->_center[2] << std::endl;
//  clout << "----->>>>> Lattice Center: " << this->_latticeCenter[0] << " // " << this->_latticeCenter[1] << " // " << this->_latticeCenter[2] << std::endl;
//  clout << "----->>>>> Mofi: " << this->_mofi[0] << " // " << this->_mofi[1] << " // " << this->_mofi[2] << std::endl;
//  clout << "----->>>>> Mass: " << this->_mass << std::endl;
//  clout << "----->>>>> Lenght: " << this->_converter.getPhysLength(_nX) << " // " << this->_converter.getPhysLength(_nY) << " // " << this->_converter.getPhysLength(_nZ) << std::endl;
//}
//
//template <typename T, typename S, template<typename U> class DESCRIPTOR, bool HLBM>
//bool SmoothIndicatorCustom3D<T,S,DESCRIPTOR,HLBM>::operator() (T output[], const S input[])
//{
//  // Translation
//  T xDist = input[0] - this->getPos()[0];
//  T yDist = input[1] - this->getPos()[1];
//  T zDist = input[2] - this->getPos()[2];
//
//  // counter-clockwise rotation by _theta=-theta around (0/0) and movement from rotation center to local center
//  int x= this->_latticeCenter[0] + _converter.getLatticeLength(this->_rotMat[0]*xDist + this->_rotMat[3]*yDist + this->_rotMat[6]*zDist);
//  int y= this->_latticeCenter[1] + _converter.getLatticeLength(this->_rotMat[1]*xDist + this->_rotMat[4]*yDist + this->_rotMat[7]*zDist);
//  int z= this->_latticeCenter[2] + _converter.getLatticeLength(this->_rotMat[2]*xDist + this->_rotMat[5]*yDist + this->_rotMat[8]*zDist);
//
//  if (x >= 0 && x < _blockData.getNx() && y >= 0 && y < _blockData.getNy() && z >= 0 && z < _blockData.getNz()) {
//    if (this->_blockData.get(x, y, z) > std::numeric_limits<T>::epsilon()) {
//      output[0] = T(this->_blockData.get(x, y, z));
//      return true;
//    }
//  }
//  output[0] = T(0);
//
//  return false;
//}


} // namespace olb

#endif
