/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014-2016 Cyril Masquelier, Mathias J. Krause, Albert Mink
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef SMOOTH_INDICATOR_F_3D_H
#define SMOOTH_INDICATOR_F_3D_H

#include <vector>

#include "smoothIndicatorBaseF3D.h"
#include "io/xmlReader.h"

#include "core/blockData3D.h"
#include "core/unitConverter.h"
#include "functors/analytical/indicator/indicatorBaseF2D.h"
#include "functors/analytical/indicator/indicatorBaseF3D.h"

namespace olb {


///////////////////////////SmoothIndicatorF/////////////////////////////////////

/// implements a smooth particle cuboid in 3D with an _epsilon sector. 
template <typename T, typename S, bool HLBM=false>
class SmoothIndicatorCuboid3D final: public SmoothIndicatorF3D<T, S, HLBM> {
private:
  S _xLength;
  S _yLength;
  S _zLength;
public:
  SmoothIndicatorCuboid3D(Vector<S,3> center, S xLength, S yLength, S zLength, S density, S epsilon, Vector<S,3> theta, Vector<S,3> vel = Vector<S,3> (0.,0.,0.));
  bool operator()(T output[],const S input[]) override;
};

/// implements a smooth sphere in 3D with an _epsilon sector 
template <typename T, typename S, bool HLBM=false>
class SmoothIndicatorSphere3D final: public SmoothIndicatorF3D<T, S, HLBM> {
private:
  S _radius;
public:
  SmoothIndicatorSphere3D(Vector<S, 3> center, S radius, S epsilon, S density=0, Vector<S,3> vel = Vector<S,3> (0.,0.,0.));
  bool operator()(T output[], const S input[]) override;
};

/// implements a smooth particle cylinder in 3D with an _epsilon sector.
template <typename T, typename S, bool HLBM=false>
class SmoothIndicatorCylinder3D final: public SmoothIndicatorF3D<T, S, HLBM> {
private:
  S _length;
  S _radius;
public:
  SmoothIndicatorCylinder3D(Vector<S,3> center, S length, S radius, S density, S epsilon, Vector<S,3> theta = Vector<S,3> (0.,0.,0.), Vector<S,3> vel = Vector<S,3> (0.,0.,0.));
  // TODO: second interface for conformity to units tests, needs to be reworked
  // Problem 1: this interface only works for radius around z-axis (therefore one unit test currently commented out)
  // Problem 2: For use with HLBM==true density has to be added
  SmoothIndicatorCylinder3D(Vector<S,3> center1, Vector<S,3> center2, S radius, S epsilon, Vector<S,3> theta = Vector<S,3> (0.,0.,0.), Vector<S,3> vel = Vector<S,3> (0.,0.,0.));
  bool operator()(T output[], const S input[]) override;
};

//implements a custom shaped smooth particle //TODO: Check for consistency
//ALSO: adap .hh
template <typename T, typename S, template<typename U> class DESCRIPTOR, bool HLBM=false>
class SmoothIndicatorCustom3D final: public SmoothIndicatorF3D<T, S, HLBM> {
private:
  // _center is the local center, _startPos the center at the start
  Vector<T,3> _center;
  // _latticeCenter gives the center in local lattice coordinates
  Vector<int,3> _latticeCenter;
  BlockData3D<T, T> _blockData;
  UnitConverter<T,DESCRIPTOR> const& _converter;

public:
  // for now epsilon has to be chosen to be latticeL
  SmoothIndicatorCustom3D(UnitConverter<T,DESCRIPTOR> const& converter, IndicatorF3D<T>& ind, Vector<T,3> center, T density, T epsilon, Vector<T,3> theta, Vector<T,3> vel = Vector<T,3> (0.,0.,0.));
  bool operator() (T output[], const S input[]);
};


}

#endif

