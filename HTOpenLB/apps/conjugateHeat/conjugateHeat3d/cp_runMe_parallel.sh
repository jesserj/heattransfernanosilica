
echo "Starting Simulation\n"

#Parameter overview
#  if(argc>=2) N = atof(argv[1]); 					//resolution 
#  if(argc>=3) sphereDiameter = atof(argv[2])*2.; 			//particle diameter
#  if(argc>=4) sysPressure = atof(argv[3]);				//pressure
#  if(argc>=5) filenameYadeSpheres = argv[4];				//particle packing
#  if(argc>=6) filenameBounding = argv[5];				//packing bounding box
#  //if(argc>=7) L = atof(argv[5]);					//no longer used
#  if(argc>=7) oFileName = argv[6];					//filename to save results


filepath="./"
#echo $filepath

filePacking="$filepath/cp_spheresXML_N5311_C1_r0.xml" 			#Without SCALED SPHERES
radius="7.79e-09"

#filePacking="$filepath/cp_spheresXML_N5311_C1_r1.9475e-09.xml" 	#With SCALED SPHERES
#radius="9.7375e-09"

fileCuboid="$filepath/cp_cuboidXML_5311_C1.xml"
fileSave="$filepath/cp_results_parallel.txt"

res="6"
#echo $filePacking
#file $filePacking

make clean && make

mpirun -np 4 conjugateHeat3d $res $radius 1e5 $filePacking $fileCuboid $fileSave
mpirun -np 4 conjugateHeat3d $res $radius 5e4 $filePacking $fileCuboid $fileSave
mpirun -np 4 conjugateHeat3d $res $radius 1e4 $filePacking $fileCuboid $fileSave
mpirun -np 4 conjugateHeat3d $res $radius 5e3 $filePacking $fileCuboid $fileSave
mpirun -np 4 conjugateHeat3d $res $radius 1e3 $filePacking $fileCuboid $fileSave
mpirun -np 4 conjugateHeat3d $res $radius 5e2 $filePacking $fileCuboid $fileSave
mpirun -np 4 conjugateHeat3d $res $radius 1e2 $filePacking $fileCuboid $fileSave
