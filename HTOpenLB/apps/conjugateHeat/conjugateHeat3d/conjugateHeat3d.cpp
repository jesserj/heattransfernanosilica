/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2011-2013 Mathias J. Krause, Thomas Henn, Tim Dornieden
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/* conjugateHead3d.cpp:

 * The authors use the primary particle size, primary pore size, aggregate
 * diameter, shape and standard deviation, as well as overall packing porosity
 * to define a mesoscopic procedural generation of nano-porous geometry.
 * Moreover, the local pore size is evaluated in order to simulate pore scale
 * heat transfer. A distinct advantage over similar methods is the holistic
 * approach presented provides direct tuning and control over particle packing
 * characteristics to study their influence on conduction and radiation
 * independently.

 * The inputs to the application are:
 N // resolution  // number of grid cells to resolve the diameter of a particle
 sphereDiameter // particle diameter
 sysPressure 		// system pressure
 filenameYadeSpheres	// XML particle filename containing particle list
 filenameBounding     // XML bounding box filename
 oFileName            // Output filename to save effective heat transfer result
*/


#include "olb3D.h"
#include "olb3D.hh"   // include full template code
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

inline bool exists_test3 (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

typedef double T;

#define DESCRIPTOR SmagorinskyAdvectionDiffusionD3Q7Descriptor
#define RADDESCRIPTOR D3Q7DescriptorRTLBM



// Parameters for the simulation setup
int N = 3;        // resolution of the model
const int maxTimeSteps = numeric_limits<int>::max();
const T epsilon = 1.e-3;   // precision of the convergence (residuum)

const T Tcold = 305.15;//273.15;
const T Thot = 306.15;
const T Tavg = (Tcold + Thot)/2; //K       //average system temperature

//const T p_min = 100.;               // [Pa]    minimum pressure experienced
T sysPressure = 10.e2;      //Pa
const T T_avg = (Tcold + Thot) / 2.;
const T c_p_air = 1.007e3;    // [J/kg*K] VDIe.V.VDI-Wärmeatlas.SpringerBerlinHeidelberg,2013. DOI:10.1007/9783-642-19981-3.
const T beta_air = 1.63;      // Jochen Fricke et al. “Vakuum-Isolations-Paneelefür Gebäude”. In: ZAE Bayern (2007).

//Fluid Parameters
const T lambda_0_air = 26.606e-3;   //[ (W / m * K) ] VDIe.V.VDI-Wärmeatlas.SpringerBerlinHeidelberg,2013. DOI:10.1007/9783-642-19981-3.

const T sigmaAir = 3.7e-10;         // [m]      %% Molecular diameter air // S Post. Applied and Computational Fluid Mechanics. Jones & Bartlett Learning, 2011.
const T p_0 = 1.e5;	                // [Pa]     %% ambient pressure
const T n_0 = 2.69e25;              // [m^-3]   %% number of particles per volume ambient 2.69e19 cm^-3 // DJJacob. Introduction to Atmospheric Chemistry.Princeton University Press,1999.


//Silica Parameters
//const T c_p_silica = 985.8;                 // [J/kg*K]
const T c_p_silica = 1052;                    // [J/kg*K]  //Quarzglas Technisches Datenblat. Tech. rep. Final Advanced Materials.
//const T rho_silica = 300;                   // [kg/m^3]  //this is compacted bulk density
const T rho_silica = 2201;                    // [kg/m^3]  //Quarzglas Technisches Datenblat. Tech. rep. Final Advanced Materials.
const T lambda_silica = 1.38;                 // [W / m * K] //Quarzglas Technisches Datenblat. Tech. rep. Final Advanced Materials.
const T alpha_solid = lambda_silica / rho_silica / c_p_silica;

const T stephan_boltzmann = 5.670367e-8;
const T stephan_boltzmann_over_pi = stephan_boltzmann / M_PI;

T absorption = 0.48;                          // VDI Wärmeatlas 2013 K3

T computeLambda( SuperLattice3D<T,DESCRIPTOR> &ADlattice,
                 SuperLattice3D<T,RADDESCRIPTOR> &RADlattice,
                 SuperGeometry3D<T>& superGeometry,
                 SuperIndicatorF3D<T>& spheresIndicator,
                 ThermalUnitConverter<T, DESCRIPTOR, DESCRIPTOR> const& converter, T charLength,
                 T &lambda_eff_air, T &lambda_eff_solid, T &lambda_eff_rad)
{

  OstreamManager clout( std::cout,"computeLambda" );

  int voxel = 0;
  int voxel_air = 0;
  int voxel_solid = 0;

  T q_z = 0;
  T flux_air = 0;
  T flux_solid = 0;
  T flux_rad = 0;

  const T physRmin= superGeometry.getStatistics().getMinPhysR(1)[2];

  for (int iC = 0; iC < ADlattice.getLoadBalancer().size(); iC++) {
    int nx = ADlattice.getBlockLattice(iC).getNx();
    int ny = ADlattice.getBlockLattice(iC).getNy();


    int iZ = 1;
    //int iZ = supergeometry.getStatistics().getMaxPhysR()[2]/2.-supergeometry.getStatistics().getMinPhysR()[2];

    for (int iX = 0; iX < nx; ++iX) {
      for (int iY = 0; iY < ny; ++iY) {
        T physR[3];
        superGeometry.getBlockGeometry(iC).getPhysR(physR, iX, iY, iZ);
        // cout << iCglob << ADlattice.getLoadBalancer().isLocal(iCglob) << " " << physR[2] << " " << superGeometry.getStatistics().getMinPhysR(1)[2] <<  " " << (physR[2] == superGeometry.getStatistics().getMinPhysR(1)[2]) << endl;
        if ( physR[2] != physRmin ) {
          // cout << "skip" << endl;
          continue;
        }

        bool tmpOut[1] = {false};
        int tmpIn[4] = {iC, iX, iY, iZ};
        spheresIndicator(tmpOut, tmpIn);

            T flux_z = 0;

            if (!tmpOut[0]) {

            T j[3];
            ADlattice.getBlockLattice(iC).get(iX,iY,iZ).computeJ(j);

            T temperature = converter.getPhysTemperature(ADlattice.getBlockLattice(iC).get(iX,iY,iZ).computeRho()); // in K
            T intensity = RADlattice.getBlockLattice(iC).get(iX,iY,iZ).computeRho() * pow(Thot, 4) * stephan_boltzmann_over_pi; // in W/m^2

              T latticeCp = converter.getLatticeSpecificHeatCapacity(converter.getPhysSpecificHeatCapacity());
              T tau = ADlattice.getBlockLattice(iC).get(iX,iY,iZ).getExternal(DESCRIPTOR<T>::ExternalField::tauEffIsAt)[0];
              T rho = 1.;

              // cout << material << "\t" << latticeCp * rho << "\t";

              flux_z = rho * latticeCp * j[2] * (tau - 0.5) / tau;

              flux_z = flux_z * converter.getConversionFactorMass() / pow(converter.getConversionFactorTime(), 3); // W / m^2

              flux_rad += ( 4.0 * stephan_boltzmann * pow(temperature, 4) - intensity ) * absorption;

              // clout << "Flux Conduction / Radiation " << flux_z << "\t" << flux_rad << endl;

              flux_air += flux_z;
              voxel_air++;

            } else if (tmpOut[0]) {

            T j[3];
            ADlattice.getBlockLattice(iC).get(iX,iY,iZ).computeJ(j);

              T latticeCp = converter.getLatticeSpecificHeatCapacity(lambda_silica);
              T tau = (DESCRIPTOR<T>::invCs2 * alpha_solid / converter.getConversionFactorViscosity()) + 0.5;
              T rho = rho_silica / converter.getPhysDensity();

              // cout << material << "\t" << latticeCp * rho << "\t";

              flux_z = rho * latticeCp * j[2] * (tau - 0.5) / tau;

              flux_z = flux_z * converter.getConversionFactorMass() / pow(converter.getConversionFactorTime(), 3);

              flux_solid += flux_z;
              voxel_solid++;
            } else {
              cout << "Error: material number not 1, 5 or 6!" << endl;
              //cin_ignore();
            }

            q_z += flux_z;

            voxel++;
      }
    }
  }

  // cout << endl;

  #ifdef PARALLEL_MODE_MPI
    singleton::mpi().reduceAndBcast(q_z, MPI_SUM);
    singleton::mpi().reduceAndBcast(voxel, MPI_SUM);
    singleton::mpi().reduceAndBcast(flux_air, MPI_SUM);
    singleton::mpi().reduceAndBcast(flux_solid, MPI_SUM);
    singleton::mpi().reduceAndBcast(flux_rad, MPI_SUM);
    singleton::mpi().reduceAndBcast(voxel_air, MPI_SUM);
    singleton::mpi().reduceAndBcast(voxel_solid, MPI_SUM);
  #endif

  q_z = q_z / (T)voxel;

  flux_air /= (T)voxel_air;
  flux_solid /= (T)voxel_solid;
  flux_rad /= (T)voxel_air;



  T lambda_eff = (flux_air + flux_solid + flux_rad) / converter.getCharPhysTemperatureDifference() * charLength * 1e3; // mW / m K

              //  flux_z = flux_z * converter.physMass() / pow(converter.physTime(), 3) / converter.getCharDeltaT() * charLength;

  clout << "voxel_air=" << voxel_air << "\t voxel_solid=" << voxel_solid << endl;


  lambda_eff_air   = (flux_air ) / converter.getCharPhysTemperatureDifference() * charLength * 1e3; // mW / m K
  lambda_eff_solid = (flux_solid) / converter.getCharPhysTemperatureDifference() * charLength * 1e3; // mW / m K
  lambda_eff_rad   = (flux_rad) / converter.getCharPhysTemperatureDifference() * charLength * 1e3; // mW / m K

  clout << "flux_air="<< flux_air << "\t flux_solid=" << flux_solid << "\t flux_rad=" << flux_rad << endl;
  clout << "lambda_eff_air="<< lambda_eff_air << "\t lambda_eff_solid=" << lambda_eff_solid << "\t lambda_eff_rad=" << lambda_eff_rad << endl;

  clout << "lambda_eff="<< lambda_eff << " mW / m K" << endl;

  return lambda_eff; // mW / m K
}

void prepareGeometry( ThermalUnitConverter<T, DESCRIPTOR, DESCRIPTOR> const& converter, IndicatorF3D<T>& cuboid, IndicatorF3D<T>& indicator,
                      SuperGeometry3D<T>& superGeometry )
{

  OstreamManager clout( std::cout,"prepareGeometry" );
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0,2);
  superGeometry.rename(2,1,0,0,1);

  // superGeometry.clean();

  std::vector<T> origin=superGeometry.getStatistics().getMinPhysR( 2 );
  origin[2] -= converter.getPhysDeltaX()/2.;
  std::vector<T> extend = superGeometry.getStatistics().getPhysExtend( 2 );
  extend[2] = 2.*converter.getPhysDeltaX();

  /// Set material number for inflow
  IndicatorCuboid3D<T> inflow( extend,origin );
  superGeometry.rename( 2,3, inflow );

  /// Set material number for outflow
  origin[2] = superGeometry.getStatistics().getMaxPhysR( 2 )[2]-converter.getPhysDeltaX();
  IndicatorCuboid3D<T> outflow( extend,origin );
  superGeometry.rename( 2,4, outflow );

  // superGeometry.rename(1,5,indicator);
  // superGeometry.rename(2,5,indicator);
  // superGeometry.rename(5,6,1,1,1);

  /// Removes all not needed boundary voxels outside the surface
  // superGeometry.clean();
  superGeometry.checkForErrors();

  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;
}

int main( int argc, char* argv[] )
{

  /// === 1st Step: Initialization ===
  olbInit( &argc, &argv );
  singleton::directories().setOutputDir( "./tmp/" );
  OstreamManager clout( std::cout,"main" );

  // default filenames, replaced by user inputs
  std::string filenameYadeSpheres = "spheresXML_N3346_C3_r1.9475e-09.xml";
  std::string filenameBounding = "cuboidXML_C3.xml";
  std::string oFileName = "results_PvsLambda.txt";

  T sphereDiameter = 1.9475e-09 * 2.0 * 5; //Input Parameter

  //T sysPressure = 10.e2;      //Pa

  //T L = 500e-9;     // [m]      %% Characteristic length [TODO] define by YADE
  T L = 1;            // [m]      %% Characteristic length [TODO] define by YADE

  /// === 2nd Step: Prepare Geometry ===
  if(argc>=2) N = atof(argv[1]); 					//resolution
  if(argc>=3) sphereDiameter = atof(argv[2])*2.; 	//particle diameter
  if(argc>=4) sysPressure = atof(argv[3]);			//pressure
  if(argc>=5) filenameYadeSpheres = argv[4];		//particle packing
  if(argc>=6) filenameBounding = argv[5];			//packing bounding box
  //if(argc>=7) L = atof(argv[5]);					//no longer used
  if(argc>=7) oFileName = argv[6];					//filename to save results

  clout << "Saving results in: " << oFileName << std::endl;

  clout << "Particle Size: " << sphereDiameter << std::endl;

  clout << "Geometry file: " << filenameYadeSpheres << std::endl;
  //cin.ignore();
  XMLreader xmlFile(filenameYadeSpheres);
  std::shared_ptr<olb::IndicatorF3D<T>> fibers = createIndicatorF3D<T>(xmlFile);
  clout << fibers->getMin()[0] <<"/"<< fibers->getMax()[0] << std::endl;
  clout << fibers->getMin()[1] <<"/"<< fibers->getMax()[1] << std::endl;
  clout << fibers->getMin()[2] <<"/"<< fibers->getMax()[2] << std::endl;

  T fiberExtend[3] = {
    fibers->getMax()[0] - fibers->getMin()[0],
    fibers->getMax()[1] - fibers->getMin()[1],
    fibers->getMax()[2] - fibers->getMin()[2]
  };

  T fiberExtendMax = fiberExtend[0];
  if (fiberExtend[1] > fiberExtendMax) fiberExtendMax = fiberExtend[1];
  if (fiberExtend[2] > fiberExtendMax) fiberExtendMax = fiberExtend[2];

  std::vector<T> extend = {fiberExtend[0], fiberExtend[1], fiberExtend[2]};
  std::vector<T> origin = {fibers->getMin()[0], fibers->getMin()[1], fibers->getMin()[2]};

  clout << origin[0] <<"/"<< extend[0] << std::endl;
  clout << origin[1] <<"/"<< extend[1] << std::endl;
  clout << origin[2] <<"/"<< extend[2] << std::endl;

  XMLreader xmlCuboidFile(filenameBounding);
  std::shared_ptr<olb::IndicatorF3D<T>> cuboid = createIndicatorF3D<T>(xmlCuboidFile);

  const T r_specific = 287.058;    //J/(kg*K)//specific gas constant dry air //  Klaus Langeheinecke et al. Thermodynamik für Ingenieure. Springer Fachmedien Wiesbaden, 2013. DOI: 10.1007/978-3-658-03169-5.
  //const T cpAir = 1.007e3;         //J/kg*K  //heat capacity of air : VDI-Waermeatlas
  const T physDeltaX = sphereDiameter / (T)N;
  const T _tmp1 = (lambda_0_air * r_specific * T_avg / sysPressure / c_p_air);
  const T _tmp2 = (2. * beta_air / (sqrt(2.) * M_PI * sigmaAir * sigmaAir * sysPressure * n_0 / p_0));
  const T alpha_min = min(_tmp1 / ( 1. + _tmp2 / physDeltaX ), alpha_solid);
  const T tau_min = 0.51;

  const T n1 = sysPressure * n_0 / p_0; // [m^-3]   %%number of particles per volume vacuum

  const T lf = 1./(sqrt(2.)*M_PI*sigmaAir*sigmaAir*n1); // [m] %% mean free path of a molecule
  const T Kn = lf / L; // [-]
  const T lambda_p_air = lambda_0_air / (1. + 2. * beta_air * Kn);  // [W / (m * K)]

  const T rho_air = sysPressure / (r_specific * Tavg); // kg/m^3
  const T alpha_p_air = lambda_p_air / rho_air / c_p_air;
  clout << "rho_air = " << rho_air << endl;
  clout << "alpha_p_air = " << alpha_p_air << endl;


  const ThermalUnitConverter<T, DESCRIPTOR, DESCRIPTOR> converter(
    (T) physDeltaX, // physDeltaX
    (T) physDeltaX * physDeltaX / alpha_min * (tau_min - 0.5) / DESCRIPTOR<T>::invCs2, // physDeltaT
    (T) sphereDiameter, // charPhysLength
    (T) 1.0, // charPhysVelocity
    (T) alpha_min, // physViscosity
    (T) rho_air, // physDensity
    (T) lambda_p_air, // physThermalConductivity
    (T) c_p_air, // physSpecificHeatCapacity
    (T) beta_air, // physThermalExpansionCoefficient
    (T) Tcold, // charPhysLowTemperature
    (T) Thot, // charPhysHighTemperature
    (T) sysPressure // charPhysPressure
  );
  converter.print();

  absorption = 9.2693e-3*pow(log(sysPressure), 2) + 1.3437e-1 * log(sysPressure) + 4.7933e-1; //VDI Waermeatlas K3 1120???????
  RadiativeUnitConverter<T, RADDESCRIPTOR> radConverter( N, 1., absorption, 0.1); //n, tau, absorption, scattering

  //const T tauT_solid = (DESCRIPTOR<T>::invCs2 * 1.38 / 300. / 985.8 / converter.getConversionFactorViscosity()) + 0.5; // TODO: check alpha_solid = (lambda / rho / c_p)_solid
  const T tauT_solid = (DESCRIPTOR<T>::invCs2 * alpha_solid / converter.getConversionFactorViscosity()) + 0.5; // TODO: check alpha_solid = (lambda / rho / c_p)_solid
  const T tauT_air_constant = (DESCRIPTOR<T>::invCs2 * alpha_p_air / converter.getConversionFactorViscosity()) + 0.5; // TODO: check


  /// Instantiation of a cuboidGeometry with weights
#ifdef PARALLEL_MODE_MPI
  const int noOfCuboids = singleton::mpi().getSize();
#else
  const int noOfCuboids = 1;
#endif

  CuboidGeometry3D<T> cuboidGeometry( cuboid, converter.getPhysDeltaX(), noOfCuboids );

  cuboidGeometry.setPeriodicity(true, true, false);

  HeuristicLoadBalancer<T> loadBalancer( cuboidGeometry );

  /// Instantiation of a superGeometry
  SuperGeometry3D<T> superGeometry( cuboidGeometry, loadBalancer, 2 );

  prepareGeometry( converter, *cuboid, *fibers, superGeometry );

  /// === 3rd Step: Prepare Lattice ===
  SuperLattice3D<T, DESCRIPTOR> sLattice( superGeometry );
  SuperLattice3D<T, RADDESCRIPTOR> radLattice( superGeometry );

  sOnLatticeBoundaryCondition3D<T,DESCRIPTOR> TboundaryCondition(sLattice);
  createAdvectionDiffusionBoundaryCondition3D<T,DESCRIPTOR>(TboundaryCondition);
  sOnLatticeBoundaryCondition3D<T,RADDESCRIPTOR> radBoundaryCondition(radLattice);
  createAdvectionDiffusionBoundaryCondition3D<T,RADDESCRIPTOR, AdvectionDiffusionBGKdynamics<T,RADDESCRIPTOR> >( radBoundaryCondition );

  ExternalTauEffLESBGKdynamics<T, DESCRIPTOR> bulkDynamics (
    converter.getLatticeThermalRelaxationFrequency(),
    instances::getAdvectionDiffusionBulkMomenta<T,DESCRIPTOR>()
  );

  sLattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, DESCRIPTOR>());
  sLattice.defineDynamics(superGeometry.getMaterialIndicator({1,2,3,4}), &bulkDynamics);
  TboundaryCondition.addTemperatureBoundary(superGeometry.getMaterialIndicator({3,4}), converter.getLatticeRelaxationFrequency());

  const T latticeSink = 3.*radConverter.getLatticeAbsorption()*(radConverter.getLatticeAbsorption()+radConverter.getLatticeScattering()) / 8.;
  PoissonDynamics<T,RADDESCRIPTOR> radDynamics( radConverter.getLatticeRelaxationFrequency(), instances::getBulkMomenta<T,RADDESCRIPTOR>(), latticeSink );

  radLattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, RADDESCRIPTOR>());
  radLattice.defineDynamics(superGeometry.getMaterialIndicator({1,2,3,4}), &radDynamics);
  radBoundaryCondition.addTemperatureBoundary(superGeometry.getMaterialIndicator({3,4}), radConverter.getLatticeRelaxationFrequency());

  SuperVTMwriter3D<T> vtkWriter( "spherePacking" );
  vtkWriter.createMasterFile();
  SuperLatticeGeometry3D<T, DESCRIPTOR> geometry( sLattice, superGeometry );
  SuperLatticePhysTemperature3D<T, DESCRIPTOR, DESCRIPTOR> temperature( sLattice, converter );
  SuperLatticePhysHeatFlux3D<T, DESCRIPTOR, DESCRIPTOR> heatflux( sLattice, converter );
  // SuperLatticePhysBoundaryDistance3D<T, DESCRIPTOR> distance( sLattice, superGeometry, xmlFile );
  // SuperLatticePhysPoreSizeDistribution3D<T, DESCRIPTOR> poreSize( sLattice, superGeometry, 1, xmlFile );
  SuperLatticePhysTauFromBoundaryDistance3D<T, DESCRIPTOR, DESCRIPTOR> modTau( sLattice, superGeometry, xmlFile, converter, sysPressure, T_avg, c_p_air, beta_air, lambda_0_air, sigmaAir, p_0, n_0);
  SuperLatticeExternalField3D<T, DESCRIPTOR> tauEff( sLattice, DESCRIPTOR<T>::ExternalField::tauEffIsAt, 1);

  SuperIndicatorFfromIndicatorF3D<T> spheresIndicator(*fibers, superGeometry);
  AnalyticalFfromIndicatorF3D<T,T> analyticalSpheres(*fibers);
  SuperLatticeFfromAnalyticalF3D<T, DESCRIPTOR> superSpheres(analyticalSpheres, sLattice);

  vtkWriter.addFunctor( geometry );
  vtkWriter.addFunctor( temperature );
  // vtkWriter.addFunctor( distance );
  // vtkWriter.addFunctor( poreSize );
  vtkWriter.addFunctor( modTau );
  vtkWriter.addFunctor( tauEff );
  vtkWriter.addFunctor( superSpheres );
  vtkWriter.addFunctor( heatflux );


  SuperMin3D<T, T> minModTau(modTau, superGeometry, 1);
  SuperMax3D<T, T> maxModTau(modTau, superGeometry, 1);
  //SuperAverage3D<T, T> avModTau(modTau, superGeometry, 1);
  int tmpInput[4];
  T tmpOutputMin[1], tmpOutputMax[1];
  minModTau(tmpOutputMin, tmpInput);
  maxModTau(tmpOutputMax, tmpInput);
  clout << "min / max tau air: " << tmpOutputMin[0] << " / " << tmpOutputMax[0] << " -- tau solid: " << tauT_solid << endl;

  const T alpha_air_max = (tmpOutputMax[0] - 0.5)/DESCRIPTOR<T>::invCs2 * converter.getConversionFactorViscosity(); // TODO: check
  clout << "tau air constant: " << tauT_air_constant << endl;
  clout << "alpha p air constant: " << alpha_p_air << endl;
  clout << "alpha p air max: " << alpha_air_max << endl;
  clout << "alpha factor: " << alpha_p_air/alpha_air_max << endl;
  clout << "Geometry Dimensions: " << sLattice.getBlockLattice(0).getNx() << "x" << sLattice.getBlockLattice(0).getNy() << "x" << sLattice.getBlockLattice(0).getNz() << endl;

  AnalyticalConst3D<T,T> Hot(converter.getLatticeTemperature(Thot));
  AnalyticalConst3D<T,T> Cold(converter.getLatticeTemperature(Tcold));
  AnalyticalLinear3D<T,T> linTemperature(0., 0., -1.0/(superGeometry.getStatistics().getMaxPhysR()[2]-superGeometry.getStatistics().getMinPhysR()[2]), converter.getLatticeTemperature(Thot));
  sLattice.defineRho(superGeometry, 1, linTemperature);
  sLattice.defineRho(superGeometry, 3, Hot);
  sLattice.defineRho(superGeometry, 4, Cold);
    AnalyticalConst3D<T,T> I_hot(1);
    AnalyticalConst3D<T,T> I_cold(pow(Tcold/Thot, 4));
  radLattice.defineRho(superGeometry, 1, Cold);
  radLattice.defineRho(superGeometry, 3, I_hot);
  radLattice.defineRho(superGeometry, 4, I_cold);

  AnalyticalConst3D<T,T> tauSolid(tauT_solid);
  AnalyticalConst3D<T,T> tauAir(tauT_air_constant);
  sLattice.defineExternalField(superGeometry, 1, DESCRIPTOR<T>::ExternalField::tauEffIsAt, 1, modTau);
  // sLattice.defineExternalField(superGeometry, 1, DESCRIPTOR<T>::ExternalField::tauEffIsAt, 1, tauAir); // TODO: uncomment for calculation with constant tau
  sLattice.defineExternalField(spheresIndicator, DESCRIPTOR<T>::ExternalField::tauEffIsAt, 1, tauSolid);
  sLattice.defineExternalField(superGeometry.getMaterialIndicator({3,4}), DESCRIPTOR<T>::ExternalField::tauEffIsAt, 1, tauSolid);
  sLattice.initialize();
  radLattice.initialize();

  sLattice.communicate();

  vtkWriter.write( 0 );

  Timer<T> timer(maxTimeSteps, superGeometry.getStatistics().getNvoxel() );
  ValueTracer<T> converge(3,epsilon);
  timer.start();

  clout << "Start time loop ..." << endl;
  for (int iT = 0; iT < maxTimeSteps; ++iT) {

    T lambda_eff_air = 0;
    T lambda_eff_solid = 0;
    T lambda_eff_rad = 0;

    if (converge.hasConverged()) {
      clout << "Simulation converged at iT = " << iT << endl;
      T finalLambda = computeLambda(sLattice, radLattice, superGeometry, spheresIndicator, converter, fiberExtend[2], lambda_eff_air, lambda_eff_solid, lambda_eff_rad);


      vtkWriter.write( iT );

      if(singleton::mpi().isMainProcessor()) {
        std::ofstream results_file;
        bool savedAlready = exists_test3(oFileName);
        results_file.open(oFileName, std::ios_base::app);

        if(!savedAlready){
          results_file << "filename; " << "N;" << "pressure(Pa);" << "lambda(mW/mK);" << "lambda_eff_air;" << "lambda_eff_solid;" << "lambda_eff_rad;" << "min_tau_air;" << "max_tau_air;" << " tau_solid;" << std::endl;
          cout << "Creating file: "  << oFileName <<  std::endl;
        }

        results_file << filenameYadeSpheres <<";"<< N<<";" << sysPressure<<";" << finalLambda<<";" <<lambda_eff_air<<";" <<lambda_eff_solid<<";" <<lambda_eff_rad<<";" << tmpOutputMin[0] << ";" << tmpOutputMax[0] << ";" << tauT_solid << std::endl;
        cout << "Saving Results to: "  << oFileName <<  std::endl;

        results_file.close();
      }

      singleton::mpi().barrier();
      break;
    }

    sLattice.collideAndStream();
    radLattice.collideAndStream();

    if (iT%1000 == 0) {
      timer.update(iT);
      timer.printStep();
      converge.takeValue(computeLambda(sLattice, radLattice, superGeometry, spheresIndicator, converter, fiberExtend[2], lambda_eff_air, lambda_eff_solid, lambda_eff_rad));
      T average = converge.computeAverage();
      T stdDev = converge.computeStdDev(average);
      clout << "average=" << average << "; stdDev/average=" << stdDev/average << std::endl;
    }
  }

  timer.stop();
  timer.printSummary();

}
