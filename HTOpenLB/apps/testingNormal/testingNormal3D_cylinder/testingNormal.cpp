/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2007, 2012 Jonas Latt, Mathias J. Krause
 *  Vojtech Cvrcek, Peter Weisbrod, Jesse Ross-Jones
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/* poiseuilleSlip_inclined2d.cpp:
 * In this implementation of a 2D cylinder flow.
 * The flow is driven by a fixed
 * velocity at the inlet and outlet
 * It illustrates the use of slip boundaries,
 * and computation of error norms.
 */

/* TODO

* find out why *uSol doesnt work in error
* add option to select solver
* fit geometry to that used in the paper
* calculate poiseuille flow at an angle
* double check initial conditions

*/


#include "olb3D.h"
#include "olb3D.hh"   // use only generic version!
#include <vector>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace std;

typedef double T;
#define DESCRIPTOR D3Q19<>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// Parameters for the simulation setup

//const int N = 100;       // resolution of the model
//int N = 100;       // resolution of the model
const T Re = 10.;       // Reynolds number
const T maxPhysT = 20.; // max. simulation time in s, SI unit

//const Vector<T,3> center(0.,0.,0.); // center of circle
//const Vector<T,3> center1(0.,0.,0.); // center1 of cylinder
//const Vector<T,3> center2(0.,0.,50.); // center of cylinder
//const T radius = 50.; // radius of cylinder

int N = 61;
const Vector<T,3> center1(-0.0163934*61,0.5,0.5); // center1 of cylinder
const Vector<T,3> center2(2.+0.0163934*61,0.5,0.5); // center of cylinder
const T radius = 0.5+0.9*0.0163934; // radius of cylinder



// Create a circle
// calculate distance
// calculate normal

int main(int argc, char* argv[])
{


  /// === 1st Step: Initialization ===
  olbInit(&argc, &argv);
  singleton::directories().setOutputDir("./tmp/");
  OstreamManager clout(std::cout,"main");
  // display messages from every single mpi process
  //clout.setMultiOutput(true);

  T distance;
  Vector<S,3> origin;
  Vector<S,3> direction;
  Vector<S,3> normal;
  Vector<S,3> normalCirc;

  if (argc == 1) {
    clout << "Usage: testingNormal originX originY originZ directionX directionY directionZ" << endl;
  }

  if (argc > 1) { //Set originX
    origin[0] = atof(argv[1]);
    //clout << "originX = " << origin[0] << endl;
  } else {
    origin[0] = 0;
  }
  if (argc > 2) { //Set originY
    origin[1] = atof(argv[2]);
  } else {
    origin[1] = 0;
    clout << "origin = [" << origin[0] << "," << origin[1] << "]" << endl;
  }
  if (argc > 3) { //Set originZ
    origin[2] = atof(argv[3]);
    clout << "origin = [" << origin[0] << "," << origin[1]  << "," << origin[2] << "]" << endl;
  } else {
    origin[2] = 0;
    clout << "origin = [" << origin[0] << "," << origin[1]  << "," << origin[2] << "]" << endl;
  }


  if (argc > 4) { //Set directionX
    direction[0] = atof(argv[4]);
  } else {
    direction[0] = 0;
  }
  if (argc > 5) { //Set directionY
    direction[1] = atof(argv[5]);
  } else {
    direction[1] = 0;
  }
  if (argc > 6) { //Set directionZ
    direction[2] = atof(argv[6]);
    clout << "direction = [" << direction[0] << "," << direction[1] << "," << direction[2] << "]" << endl;
  } else {
    direction[2] = 1;
    clout << "direction = [" << direction[0] << "," << direction[1] << "," << direction[2] << "]" << endl;
  }
  //std::cin.ignore();

  std::string normalResults;
  //errorString.append("more stuff");
  std::ofstream normalResults_file;


/*
  LBconverter<T> converter(
    (int) 2,                               // dim
    1./N,                                  // latticeL_
    1./N,                                  // latticeU_
    (T)   1./Re,                           // charNu_
    (T)   1.                               // charL_ = 1,
  );
  //converter.print();
  */

  /// === 2nd Step: Prepare Geometry ===

  //clout << "Indicator" << endl;
  //const Vector<T,3> indNormal(0.,0.,1.); // center of circle
  
  //IndicatorSphere3D<T> sphere(center, radius);
  IndicatorCylinder3D<T> cylinder(center1, center2, radius);
  


#ifdef PARALLEL_MODE_MPI
  const int noOfCuboids = singleton::mpi().getSize();
#else
  const int noOfCuboids = 7;
#endif
  /*
  clout << "cuboidGeometry" << endl;
  CuboidGeometry3D<T> cuboidGeometry(sphere, converter.getLatticeL(), noOfCuboids);

  /// Instantiation of a loadBalancer
  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);

  /// Instantiation of a superGeometry
  SuperGeometry3D<T> superGeometry(cuboidGeometry, loadBalancer, 2);

  //superGeometry::distance(S& distance, const Vector<S,2>& origin, const Vector<S,2>& direction, int iC)
   *    */

  //clout << "Distance" << endl;
  cylinder.distance(distance, origin, direction, 1);
  clout << "distance = [" << distance << "]"  << endl;
  //clout << "Normal" << endl;
  cylinder.normal(normal, origin, direction, 1);
  
  //Vector<S,3> VecNorm (sqrt(normal[0]);
  //normalize(normal);

  //circle.normalCirc(normalCirc, origin, direction, 1);
  //clout << "normal = [" << normal[0] << "," << normal[1] << "," << normal[2] << "]" << endl;
  //clout << "normalCirc = [" << normalCirc[0] << "," << normalCirc[1] << "]" << endl;
  //superGeometry.normal(normal, origin, direction);


  // Save results to file

  ifstream f("normalTest.txt");

  if (!f.good()) {
    //clout << "file does not exist " << f.good() << endl;

    normalResults_file.open("normalTest.txt", std::ios_base::app);

    normalResults_file << "center1X;" << "center1Y;" << "center1Z;" ;
    normalResults_file << "center2X;" << "center2Y;" << "center2Z;" ;
    normalResults_file << "radius;" ;
    normalResults_file << "originX;" << "originY;" << "originZ;" << "directionX;" << "directionY;" << "directionZ;"  ;
    normalResults_file << "distance;" << "normalX;" << "normalY;" << "normalZ;" << std::endl;

    normalResults_file.close();
  }

  normalResults_file.open("normalTest.txt", std::ios_base::app);
  normalResults_file << center1[0] << ";" << center1[1] << ";" << center1[2] << ";" ;
  normalResults_file << center2[0] << ";" << center2[1] << ";" << center2[2] << ";" ;
  normalResults_file << radius << ";" ;
  normalResults_file << origin[0] << ";" << origin[1] << ";" << origin[2] << ";" << direction[0] << ";" << direction[1] << ";" << direction[2] << ";";
  normalResults_file << distance << ";" << normal[0] << ";" << normal[1] << ";" << normal[2] << ";" << std::endl;

  normalResults_file.close();




  // Check error (2 cases, top/bottom or side surface)
  
  if(std::abs(normal[0]) <= 1e-06 && std::abs(normal[1]) <= 1e-06){
    clout << "Top/bottom surface: " << endl;
      clout << "normal = [" << normal[0] << "," << normal[1] << "," << normal[2] << "]" << endl;
  }
  else{
      clout << "Side surface: " << endl;
      clout << "normal = [" << normal[0] << "," << normal[1] << "," << normal[2] << "]" << endl;
      
      Vector<S,3> POS; //Point on Surface
      POS[0] = origin[0] + direction[0]*distance/direction.norm();
      POS[1] = origin[1] + direction[1]*distance/direction.norm();
      POS[2] = origin[2] + direction[2]*distance/direction.norm();
      //clout << "POS = " << POS[0] << "," << POS[1] << endl;
      T normDir = sqrt(direction[0]*direction[0] + direction[1]*direction[1] + direction[2]*direction[2]);
      //direction.norm();
      //clout << "normDir = " << normDir << ", direction.norm() = " << direction.norm() << endl;

      Vector<S,3> normalVec;
      T norm = radius/normal.norm();
      normalVec[0] = POS[0]+normal[0]*norm;
      normalVec[1] = POS[1]+normal[1]*norm;
      normalVec[2] = POS[2]+normal[2]*norm;
      
      Vector<S,3> newCenter;
      newCenter[0] = center1[0];
      newCenter[1] = center1[1];
      newCenter[2] = center1[2]+POS[2];
      
      clout << "POS = " << POS[0] << "," << POS[1] << "," << POS[2] << endl;
      clout << "center point cylinder  = [" << newCenter[0] << "," << newCenter[1] << "," << newCenter[2] << "]" << endl;
      clout << "predicted center point = [" << normalVec[0] << "," << normalVec[1] << "," << normalVec[2] << "]" << endl;

      //clout << "norm = " << norm << endl;
      //clout << "normal = " << normal[0] << endl;
      //clout << "POS = " << POS[0] << endl;
      //clout << "normalVec = " << normalVec[0] << "=" << POS[0] << "+" << normal[0] << "*" << norm << endl;
      clout << "Error = " << (normalVec - newCenter).norm()/radius << endl;
      //clout << "Error = " << normalVec[0] << "," << normalVec[1] << "," << normalVec[2] << endl;

  }

  


  return 0;


}

